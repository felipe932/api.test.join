<?php
/**
 * Created by PhpStorm.
 * User: eduardo
 * Date: 01/08/18
 * Time: 15:17
 */

namespace App\Utils;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ResponseUtils
{
    /**
     * @param string $message
     * @param mixed  $data
     *
     * @return array
     */
    public static function makeResponse($message, $data)
    {
        return [
            'success' => true,
            'data'    => $data,
            'message' => $message,
        ];
    }

    /**
     * @param string $message
     * @param array  $data
     *
     * @return array
     */
}
