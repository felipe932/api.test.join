<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class registrationConfirmation extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var array
     */
    private $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Array $email)
    {
        //
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->with($this->email['meta'])
            ->view('emails.users.registrationConfirmationCompleted');
    }
}
