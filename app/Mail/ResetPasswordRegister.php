<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPasswordRegister extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var array
     */
    private $data;

    /**
     * Create a new message instance.
     *
     * @param array $email
     */
    public function __construct(Array $email)
    {
        $this->data = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->with($this->data['meta'])
            ->view('emails.users.resetPasswordCompleted');
    }
}
