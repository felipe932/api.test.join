<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\TrimStrings as Middleware;

class TrimStrings extends Middleware
{
    /**
     * The names of the attributes that should not be trimmed.
     *
     * @var array
     */
    protected $except = [
        'password',
        'password_confirmation',
    ];

    /**
     * Routes that will not apply the trim function
     *
     * @var array
     */
    protected $except_urls = [
        'description/standardization/algorithm',
        'taxonomy_rules/general/',
        'fixed_texts/general/'
    ];

    protected function transform($key, $value)
    {
        $regex = '#' . implode('|', $this->except_urls) . '#';
        if ( preg_match($regex, request()->path()) > 0 ) {
            return $value;
        }

        if (in_array($key, $this->except, true)) {
            return $value;
        }

        return is_string($value) ? trim($value) : $value;
    }
}
