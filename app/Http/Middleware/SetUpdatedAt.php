<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;

class SetUpdatedAt
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->isMethod('post') || $request->isMethod('patch')) {
            $request->request->add(['updated_at' => Carbon::now()]);
        }
        return $next($request);
    }
}
