<?php

namespace App\Http\Controllers;

use App\Services\RegisterService;
use App\Services\TrialService;
use App\Services\ForgotPasswordService;
use App\Services\ResetPasswordService;
use Carbon\Carbon;
use Dotenv\Exception\ValidationException;
use Illuminate\Http\Request;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Repositories\UsersRepository;
use App\Validators\UsersValidator;
use App\Exceptions\ExceptionGeneral;
use Illuminate\Support\MessageBag;
use App\Events\UsersCreatedOrUpdated;

/**
 * Class UsersController.
 *
 * @package namespace App\Http\Controllers;
 */
class UsersController extends Controller
{
    /**
     * @var UsersRepository
     */
    protected $repository;

    /**
     * @var UsersValidator
     */
    protected $validator;
    /**
     * @var Request
     */
    private $request;

    protected $data = [];

    /**
     * UsersController constructor.
     *
     * @param Request $request
     * @param UsersRepository $repository
     * @param UsersValidator $validator
     */
    public function __construct(Request $request, UsersRepository $repository, UsersValidator $validator)
    {
        $this->request = $request;
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));

        if ( $this->request->has('paginate')) {
            $this->data = $this->repository->paginate($this->request->input('paginate'));
        }

        if ( ! $this->request->has('paginate')) {
            $this->data = $this->repository->all();
        }

        return $this->setData($this->data)->setMessage('Users listed with success!')->success();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     *
     * @throws ValidatorException
     */
    public function store()
    {
        $this->beginTransaction();

        $input = $this->request->all();
        
        if( trim($input['password']) !== trim($input['confirmPassword']) ) {
            throw new \Exception('Confirmação de senha não confere.');
        }

        $this->validator->with($input)->passesOrFail(ValidatorInterface::RULE_CREATE);
        $this->data = $this->repository->create($input);

        event( new UsersCreatedOrUpdated($input, $this->data->id) );

        $this->commit();

        return $this->setData($this->data)->setMessage('User created with success!')->success();

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function show()
    {
        $id = $this->request->route('id');
        $haveUserLoggedMessage = '';

        if ( $id == 'logged' ) {
            $id = $this->request->user()->id;
            $token = request()->header('Authorization', '');
            $token = explode('Bearer ',$token);
            $token = array_key_exists(1,$token) ? $token[1] : null;
            $this->repository->update(['token_access' => $token],request()->user()->id);
        }

        $data = $this->repository->find($id);

        if (empty($data)) {
            throw new \Exception('User not found');
        }

        return $this->setData($data)->setMessage('User show with success! show')->success();
    }

    /**
     * Update the specified resource in storage.
     *
     * @return Response
     *
     * @throws ValidatorException
     */
    public function update()
    {
        $id = $this->request->route('id');

        $this->beginTransaction();

        unset($this->request->email);

        $input = $this->request->all();
        
        $this->validator->with($input)->passesOrFail(ValidatorInterface::RULE_UPDATE);
        $this->data = $this->repository->update($input, $id);

        event( new UsersCreatedOrUpdated($input, $id) );

        $this->commit();

        return $this->setData($this->data)->setMessage('User edited with success!')->success();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $id = $this->request->route('id');

        if( request()->user()->id === $id ){
            throw new \Exception('Nao é possível excluir usuário.');
        }

        $this->beginTransaction();

        $this->data = $this->repository->delete($id);

        $this->commit();

        return $this->setData(['deleted' => $this->data,'id' => $id])->setMessage('User deleted with success!')->success();
    }

}
