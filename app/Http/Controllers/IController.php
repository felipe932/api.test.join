<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

interface IController
{
    public function index();
    public function store();
    public function show();
    public function update();
    public function destroy();
}