<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Prettus\Validator\Contracts\ValidatorInterface;
use Prettus\Validator\Exceptions\ValidatorException;
use App\Http\Requests\CategoriesCreateRequest;
use App\Http\Requests\CategoriesUpdateRequest;
use App\Repositories\CategoriesRepository;
use App\Validators\CategoriesValidator;

/**
 * Class CategoriesController.
 *
 * @package namespace App\Http\Controllers;
 */
class CategoriesController extends Controller
{
    /**
     * @var CategoriesRepository
     */
    protected $repository;

    /**
     * @var CategoriesValidator
     */
    protected $validator;

     /**
     * @var Request
     */
    private $request;

    /**
     * CategoriesController constructor.
     *
     * @param CategoriesRepository $repository
     * @param CategoriesValidator $validator
     * @param Request $request
     */
    public function __construct(Request $request,CategoriesRepository $repository, CategoriesValidator $validator)
    {
        $this->request = $request;
        $this->repository = $repository;
        $this->validator  = $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));

        if ( $this->request->has('paginate')) {
            $this->data = $this->repository->paginate($this->request->input('paginate'));
        }
        if ( ! $this->request->has('paginate')) {
            $this->data = $this->repository->all();
        }
        
        return $this->setData($this->data)->setMessage('Categories listed with success!')->success();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     *
     * @throws ValidatorException
     */
    public function store()
    {
        $this->beginTransaction();

        $input = $this->request->all();
        $input = array_merge($input, ['users_id' => request()->user()->id ]);

        $this->validator->with($input)->passesOrFail(ValidatorInterface::RULE_CREATE);
        $this->data = $this->repository->create($input);

        $this->commit();

        return $this->setData($this->data)->setMessage('Categories created with success!')->success();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function show()
    {
        $id = $this->request->route('id');

        if ( $id == 'logged' ) {
            $id = $this->request->user()->id;
        }

        $data = $this->repository->find($id);

        if (empty($data)) {
            throw new \Exception('Categories not found');
        }

        return $this->setData($data)->setMessage('Categories show with success! show')->success();
    }

    /**
     * Update the specified resource in storage.
     *
     * @return Response
     *
     * @throws ValidatorException
     */
    public function update()
    {
        $id = $this->request->route('id');

        $this->beginTransaction();

        $input = $this->request->all();
        $input = array_merge($input, ['users_id' => request()->user()->id ]);

        $this->validator->with($input)->passesOrFail(ValidatorInterface::RULE_UPDATE);

        $this->data = $this->repository->update($input, $id);

        $this->commit();

        return $this->setData($this->data)->setMessage('Categories edited with success!')->success();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        $id = $this->request->route('id');

        $this->beginTransaction();

        $this->data = $this->repository->delete($id);

        $this->commit();

        return $this->setData(['deleted' => $this->data])->setMessage('Categories deleted with success!')->success();
    }
}
