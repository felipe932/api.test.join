<?php


namespace App\HealthChecks;

use UKFast\HealthCheck\HealthCheck;

class ElasticsearchHealthCheck extends HealthCheck
{
    protected $name = 'elasticsearch';

    public function status()
    {
        try {
            \Elasticsearch::ping();
        } catch ( \Exception $e ) {
            return $this->problem('Failed to connect to elasticsearch', [
                'exception' => $this->exceptionContext($e),
            ]);
        }

        return $this->okay();
    }

}
