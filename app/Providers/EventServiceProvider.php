<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\AttributeValueCreatedOrUpdated' => [
            'App\Listeners\Attributes\Values\CreateValuesDescriptions',
        ],
        'App\Events\AttributeValueDeleted' => [
            'App\Listeners\Attributes\Values\DeleteDependencies'
        ],
        'App\Events\AdditionalFieldsValueCreatedOrUpdated' => [
            'App\Listeners\AdditionalFields\Values\CreateValuesDescriptions'
        ],
        'App\Events\AdditionalFieldsValueDeleted' => [
            'App\Listeners\AdditionalFields\Values\DeleteDependencies'
        ],
        'App\Events\ItemAttachmentsGeneralsCreateOrUpdate' => [
            'App\Listeners\ItemAttachments\CreateAttachmentsDescriptions',
            'App\Listeners\ItemAttachments\ManagementFiles'
        ],
        'App\Events\ItemGeneralsCreateOrUpdate' => [
            'App\Listeners\Item\CreateDescriptions',
            'App\Listeners\Item\CreateItemTecCategory',
            'App\Listeners\Item\CreateItemAttributesValues',
            'App\Listeners\Item\CreateCategoryHierarchies',
            'App\Listeners\Item\CreateItemBarcode',
            'App\Listeners\Item\CreateItemPartnumber',
            'App\Listeners\Item\CreateItemUom',
            'App\Listeners\Item\CreateItemBrands',
            'App\Listeners\Item\CreateItemAttachments',
            'App\Listeners\Item\CreateComponents',
            'App\Listeners\Item\CreateQueueElasticSearch',
        ],
        'App\Events\ItemGeneralsCreateOrUpdateElastic' => [
            'App\Listeners\Elastic\CreateOrUpdateIndex',
        ],
        'App\Events\AttributesCreateOrUpdate' => [
            'App\Listeners\Attributes\CreateDescriptions',
            'App\Listeners\Attributes\CreateAttributesValues',
        ],
        'App\Events\CategoriesCreateOrUpdate' => [
            'App\Listeners\Categories\CreateDescriptions',
            'App\Listeners\Categories\CreateHierarchies',
            'App\Listeners\Categories\CreateCategoriesAttributes',
            'App\Listeners\Categories\StoreCategoriesImages',
        ],
        'App\Events\CompaniesCreateOrUpdate' => [
            'App\Listeners\Companies\CreateSegments'
        ],
        'App\Events\UsersProfileCreated' => [
            'App\Listeners\Users\Profiles\CreateUserIfNotExists',
            'App\Listeners\Users\Profiles\CreateUserEmail',
            'App\Listeners\Users\Profiles\StoreUserImagesCreated'
        ],
        'App\Events\UsersProfileUpdated' => [
            'App\Listeners\Users\Profiles\UpdateUser',
            'App\Listeners\Users\Profiles\UpdatedUserEmail',
            'App\Listeners\Users\Profiles\StoreUserImagesUpdated'
        ],
        'App\Events\UsersCreatedOrUpdated' => [
            'App\Listeners\Users\StoreUserImages'
        ],
        'App\Events\UnitsOfMesuaresCreatedOrUpdated' => [
            'App\Listeners\UnitsOfMesuares\CreateDescriptions'
        ],
        'App\Events\VendorsCreateOrUpdate' => [
            'App\Listeners\Vendors\CreateSuperiorVendors',
            'App\Listeners\Vendors\CreateVendorsBrands',
            'App\Listeners\Vendors\StoreCategoriesImages',
        ],
        'App\Events\AbbreviationsCreatedOrUpdated' => [
            'App\Listeners\Abbreviations\CreateDescriptions',
            'App\Listeners\Abbreviations\CreateUsageLevels',
            'App\Listeners\Abbreviations\CreateDescriptionsTypes'
        ],
        'App\Events\DescriptionsTypesCreatedOrUpdated' => [
            'App\Listeners\DescriptionsTypes\CreateDescriptions',
            'App\Listeners\DescriptionsTypes\CreateLanguages',
            'App\Listeners\DescriptionsTypes\CreateSubstitutionsRules',
            'App\Listeners\DescriptionsTypes\CreateQueueElasticSearch',
        ],
        'App\Events\AdditionalFieldsCreatedOrUpdated' => [
            'App\Listeners\AdditionalFields\CreateDescriptions',
            'App\Listeners\AdditionalFields\CreateAdditionalFieldsValues',
        ],
        'App\Events\AdditionalFieldsObjectsCreatedOrUpdate' => [
            'App\Listeners\AdditionalFieldsObjects\CreateObjects',
        ],
        'App\Events\InstancesCreatedOrUpdated' => [
            'App\Listeners\Instances\CreateDescriptions',
            'App\Listeners\Instances\CreateCurrencies',
            'App\Listeners\Instances\CreateLanguages',
            'App\Listeners\Instances\CreateQueueElasticSearch',
            'App\Listeners\Instances\CreateQueueElasticSearchMassItemVisibility',
        ],
        'App\Events\EquivalentsTermsCreatedOrUpdated' => [
            'App\Listeners\EquivalentsTerms\CreateRemarks',
            //'App\Listeners\EquivalentsTerms\CreateSynonyms'
        ],
        'App\Events\TaxonomyRulesCreatedOrUpdated' => [
            'App\Listeners\TaxonomyRules\CreateTaxonomyRulesItems'
        ],
        'App\Events\CategoriesHierarchiesNodeCreatedOrUpdated' => [
            'App\Listeners\CategoriesHierarchies\CreateDescriptionsNode',
        ],
        'App\Events\CategoriesHierarchiesCreatedOrUpdated' => [
            'App\Listeners\CategoriesHierarchies\CreateHighestLevel',
            'App\Listeners\CategoriesHierarchies\CreateDescriptions',
            'App\Listeners\CategoriesHierarchies\StoreCategoriesImages',
        ],
        'App\Events\AttributesGroupsCreatedOrUpdated' => [
            'App\Listeners\Attributes\Groups\CreateDescriptions',
        ],
        'App\Events\ShiftsCreateOrUpdate' => [
            'App\Listeners\Shifts\CreateDescriptions',
            'App\Listeners\Shifts\CreateScales',
        ],
        'App\Events\UsersGroupsCreatedOrUpdated' => [
            'App\Listeners\UsersGroups\CreateAgents',
            'App\Listeners\UsersGroups\CreateDescriptions',
        ],
        'App\Events\BusinessAreasCreatedOrUpdated' => [
            'App\Listeners\BusinessAreas\CreateDescriptions',
        ],
        'App\Events\CalendarCreateOrUpdate' => [
            'App\Listeners\Calendar\CreateDescriptions',
            'App\Listeners\Calendar\CreateHolidays',
            'App\Listeners\Calendar\CreateWorkdays'
        ],
        'App\Events\FixedTextsCreateOrUpdate' => [
            'App\Listeners\FixedTexts\CreateDescriptions',
        ],
        'App\Events\ProcessEmailCreatedOrUpdated' => [
            'App\Listeners\WorkFlow\Emails\CreateDescriptions',
            'App\Listeners\WorkFlow\Emails\CreateContents',
        ],
        'App\Events\ProcessCreatedOrUpdated' => [
            'App\Listeners\WorkFlow\CreateDescriptions',
            'App\Listeners\WorkFlow\CreateSteps',
            'App\Listeners\WorkFlow\UpdateFavoritesStatus'
        ],
        'App\Events\ProcessFormsCreatedOrUpdated' => [
            'App\Listeners\WorkFlow\Forms\CreateDescriptions',
            'App\Listeners\WorkFlow\Forms\CreateAdditionalFields',
            'App\Listeners\WorkFlow\Forms\CreateProcessFormsFields',
        ],
        'App\Events\TagsCreatedOrUpdate' => [
            'App\Listeners\Tags\CreateTags',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
