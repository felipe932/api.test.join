<?php

namespace App\Providers;

use App\Entities\FunctionGeneral;
use App\Entities\InstancesGenerals;
use App\Entities\Users;
use Illuminate\Contracts\Auth\Access\Gate as GateContracts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @param GateContracts $gate
     * @param Request $request
     * @return void
     */
    public function boot(GateContracts $gate, Request $request)
    {
        $this->registerPolicies();

        Passport::setDefaultScope(['general']);
        Passport::personalAccessClientId(1); //Personal Access token and Scope

        if ( ! Schema::hasTable('function_generals') ) {
            return;
        }

        $gate->before(function(Users $user, $ability) {
            if ( $this->_checkCompany($user) === false ) {
               return false;
            }
            if ( $this->_isSpecialRequest($user) ) {
                return true;
            }
        });

        $instanceID = app()->make('getInstanceID');

        if ( ! $instanceID ) {
            $functions = FunctionGeneral::get();
        } else {
            $functions = FunctionGeneral::with('profileFunctions')->get();
        }

        foreach ($functions as $function) {
            $gate->define($function->function, function (Users $logged) use ($function) {
                return $logged->hasPermission($function);
            });
        }
    }

    /**
     * Prepare method to check instance before check permissions
     * This Method check if the instance it is active and if company it is active too.
     *
     * @param $user
     * @return bool
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    private function _checkCompany($user)
    {
        $instanceID = app()->make('getInstanceID');
        if ( ! $instanceID ) {
            return false;
        }

        $userProfiles = $user->instances()->with('instance')->whereHas('instance', function($query) use ($instanceID){
            $query->where(['instances_generals_id' => $instanceID]);
        })->first();

        if ( is_null($userProfiles) ) {
            return false;
        }

        $instance = $userProfiles->instance;

        if ( is_null($instance) ) {
            return false;
        }

        switch ($instance->status) {
            case 'I':
                return false;
                break;
            case 'C':
                return false;
                break;
        }

        switch ($instance->company->status) {
            case 'I':
                return false;
                break;
            case 'C':
                return false;
                break;
        }

        return true;
    }

    private function _isSpecialRequest()
    {
        if ( ! request()->isMethod('get') ||
             ! request()->has('isSelect') ||
             (bool) request()->has('isSelect') === false ) {

            return false;
        }
        return true;
    }
}
