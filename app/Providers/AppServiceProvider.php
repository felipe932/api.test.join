<?php

namespace App\Providers;

use App\Entities\Users;
use App\Traits\SetSchemasTrait;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    use SetSchemasTrait;
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bindMailgun();
        $this->setLanguageID();
        $this->bootEntitiesObservables();
        $this->setInstanceID();
    }

    public function bindMailgun()
    {
        $this->app->bind('mailgun.client', function() {
            return \Http\Adapter\Guzzle6\Client::createWithConfig([
                // your Guzzle6 configuration
            ]);
        });
    }

    public function setLanguageID()
    {
        $this->app->singleton('getLanguageId', function ($app) {

            $languages_generals_id = 'all';

            if ( request()->has('appLanguage') && request()->has('translate') === true && (bool)request()->input('translate') === true ) {
                $where = null;
                $where = [['language_code', 'ilike', request()->input('appLanguage') ], ['active', '=', 1]];
                $language = LanguagesGeneral::where($where)->first('id');

                /** en-US by default */
                if ( ! is_null( $language ) ) {
                    $languages_generals_id = $language->id;
                }
            }

            if ( $languages_generals_id === 'all' && request()->has('translate') === true && (bool)request()->input('translate') === true ) {
                $languages_generals_id = request()->user()->languages_generals_id;
            }

            /** TODO - set language of company */
            return $languages_generals_id;
        });
    }

    public function setInstanceID()
    {
        $this->app->singleton('getInstanceID', function ($app) {
            $uuid = $this->getSchema();
            $instance = InstancesGenerals::where(['uuid' => $uuid])->get();
            if ( $instance->isEmpty() ) {
                return false;
            }
            return $instance->first()->id;
        });
    }

    public function bootEntitiesObservables()
    {
       
    }
}
