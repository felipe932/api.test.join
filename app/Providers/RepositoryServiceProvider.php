<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        $this->app->bind(\App\Repositories\UsersRepository::class, \App\Repositories\UsersRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CountriesRepository::class, \App\Repositories\CountriesRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\StatesRepository::class, \App\Repositories\StatesRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CitiesRepository::class, \App\Repositories\CitiesRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\AreasRepository::class, \App\Repositories\AreasRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\FiscalsRepository::class, \App\Repositories\FiscalsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\PollingPlacesRepository::class, \App\Repositories\PollingPlacesRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\StockingsRepository::class, \App\Repositories\StockingsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\SectionsRepository::class, \App\Repositories\SectionsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ZonesRepository::class, \App\Repositories\ZonesRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\TypeOfFiscalsRepository::class, \App\Repositories\TypeOfFiscalsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\IndicationsRepository::class, \App\Repositories\IndicationsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EventsRepository::class, \App\Repositories\EventsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\EventsFiscalsRepository::class, \App\Repositories\EventsFiscalsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\BanksRepository::class, \App\Repositories\BanksRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ConfigurationsRepository::class, \App\Repositories\ConfigurationsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\LawyerPollingPlacesStockingsRepository::class, \App\Repositories\LawyerPollingPlacesStockingsRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\CategoriesRepository::class, \App\Repositories\CategoriesRepositoryEloquent::class);
        $this->app->bind(\App\Repositories\ProductsRepository::class, \App\Repositories\ProductsRepositoryEloquent::class);
        //:end-bindings:
    }
}
