<?php

namespace App\Services\Export;

use Illuminate\Http\Request;
use Illuminate\Contracts\Filesystem\Factory as Storage;
use App\Exports\CategoriesTemplateExport;
use App\Repositories\CategoryGeneralRepository;
use Maatwebsite\Excel\Facades\Excel;
use League\Flysystem\Filesystem;
use League\Flysystem\ZipArchive\ZipArchiveAdapter;

class CategoryExportService
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var Storage
     */
    private $storage;

    /**
     * @var CategoryGeneralRepository
     */
    protected $repository;

    private $disk = 'mub';
    
    /**
     * CategoryExportService constructor.
     *
     * @param Request $request
     * @param Storage $storage
     * @param CategoryGeneralRepository $repository
    */
    public function __construct(
        Request $request,
        Storage $storage,
        CategoryGeneralRepository $repository
    ){
        $this->request = $request;
        $this->storage = $storage;
        $this->repository = $repository;
    }

    public function export()
    {
        $templates = $this->request->input('templates');
        $templates = explode(",",$templates);

        $imports = [];
        $templates_names = [];
        foreach( $templates as $template)
        {
            switch( $template )
            {
                case 'category_generals':
                    $heading1 = $this->request->input('heading1');
                    $heading = explode(",",$heading1);
                    $templates_names['category_generals'] = $this->request->input('category_generals');
                break;
                case 'category_descriptions':
                    $heading2 = $this->request->input('heading2');
                    $heading = explode(",",$heading2);
                    $templates_names['category_descriptions'] = $this->request->input('category_descriptions');
                break;
                case 'category_attributes':
                    $heading3 = $this->request->input('heading3');
                    $heading = explode(",",$heading3);
                    $templates_names['category_attributes'] = $this->request->input('category_attributes');
                break;
            }

            $headings = [$heading];

            $categoriesTemplateExport = new CategoriesTemplateExport($headings,$template);
            $categoriesTemplateExport->setRepository($this->repository);

            $file = uniqid();
            $store = Excel::store($categoriesTemplateExport, $file . '.xlsx', $this->disk);
            if($store) $imports[$file] = $template;
        }

        $zipFile = uniqid() . '.zip';
        $zipPath = storage_path('app/public/' . $zipFile);

        $zip = new Filesystem(new ZipArchiveAdapter($zipPath));

        foreach($imports as $file_name => $template ) {
            $file_content = $this->storage->disk($this->disk)->get($file_name . '.xlsx');
            $zip->put(str_replace( '/',' ',$templates_names[$template] ) . '.xlsx', $file_content);
        }

        $zip->getAdapter()->getArchive()->close();

        return response()->download($zipPath)->deleteFileAfterSend(true);
    }

}