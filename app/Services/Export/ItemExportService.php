<?php

namespace App\Services\Export;

use Illuminate\Http\Request;
use Illuminate\Contracts\Filesystem\Factory as Storage;
use App\Exports\ItemsExport;
use App\Repositories\ItemGeneralRepository;
use Maatwebsite\Excel\Facades\Excel;
use League\Flysystem\Filesystem;
use League\Flysystem\ZipArchive\ZipArchiveAdapter;

class ItemExportService
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var Storage
     */
    private $storage;

    /**
     * @var ItemGeneralRepository
     */
    protected $repository;

    private $disk = 'mub';
    
    /**
     * ItemExportService constructor.
     *
     * @param Request $request
     * @param Storage $storage
     * @param ItemGeneralRepository $repository
    */
    public function __construct(
        Request $request,
        Storage $storage,
        ItemGeneralRepository $repository
    ){
        $this->request = $request;
        $this->storage = $storage;
        $this->repository = $repository;
    }

    public function export()
    {
        $templates = $this->request->input('templates');
        $templates = explode(",",$templates);

        $imports = [];
        $templates_names = [];
        foreach( $templates as $template)
        {
            switch( $template )
            {
                case 'item_generals':
                    $heading1 = $this->request->input('heading1');
                    $heading = explode(",",$heading1);
                    $templates_names['item_generals'] = $this->request->input('item_generals');
                break;
                case 'item_descriptions':
                    $heading2 = $this->request->input('heading2');
                    $heading = explode(",",$heading2);
                    $templates_names['item_descriptions'] = $this->request->input('item_descriptions');
                break;
                case 'category_item':
                    $heading3 = $this->request->input('heading3');
                    $heading = explode(",",$heading3);
                    $templates_names['category_item'] = $this->request->input('category_item');
                break;
                case 'item_part_numbers':
                    $heading4 = $this->request->input('heading4');
                    $heading = explode(",",$heading4);
                    $templates_names['item_part_numbers'] = $this->request->input('item_part_numbers');
                break;
                case 'item_barcodes':
                    $heading5 = $this->request->input('heading5');
                    $heading = explode(",",$heading5);
                    $templates_names['item_barcodes'] = $this->request->input('item_barcodes');
                break;
                case 'item_units_of_measures':
                    $heading6 = $this->request->input('heading6');
                    $heading = explode(",",$heading6);
                    $templates_names['item_units_of_measures'] = $this->request->input('item_units_of_measures');
                break;
                case 'item_brands':
                    $heading7 = $this->request->input('heading7');
                    $heading = explode(",",$heading7);
                    $templates_names['item_brands'] = $this->request->input('item_brands');
                break;
            }

            $headings = [$heading];

            $itemsExport = new ItemsExport($headings,$template);
            $itemsExport->setRepository($this->repository);

            $file = uniqid();
            $store = Excel::store($itemsExport, $file . '.xlsx', $this->disk);
            if($store) $imports[$file] = $template;
        }

        $zipFile = uniqid() . '.zip';
        $zipPath = storage_path('app/public/' . $zipFile);

        $zip = new Filesystem(new ZipArchiveAdapter($zipPath));

        foreach($imports as $file_name => $template ) {
            $file_content = $this->storage->disk($this->disk)->get($file_name . '.xlsx');
            $zip->put(str_replace( '/',' ',$templates_names[$template] ) . '.xlsx', $file_content);
        }

        $zip->getAdapter()->getArchive()->close();

        return response()->download($zipPath)->deleteFileAfterSend(true);
    }
}