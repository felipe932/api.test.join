<?php
/**
 * Created by FelipeCastro
 * User: felipe
 * Date: 17/07/19
 * Time: 09:53
 */

namespace App\Services;

use App\Events\CompanyCreated;
use App\Exceptions\ExceptionGeneral;
use App\Repositories\QueueEmailsRepository;
use App\Repositories\UsersRepository;
use App\Traits\MyDatabaseTransactions;
use App\Validators\UsersValidator;
use Bogardo\Mailgun\Facades\Mailgun;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use Prettus\Validator\Contracts\ValidatorInterface;

class ResetPasswordService
{

	use MyDatabaseTransactions;
	private $request;
	private $queueEmailsRepository;
	private $repository;
	private $validator;
	private $data = [];
	private $user;
    private $creator_user;

	public function __construct(
		Request $request,
        UsersRepository $repository,
        UsersValidator $validator,
        QueueEmailsRepository $queueEmailsRepository
    ){
        $this->request = $request;
        $this->repository = $repository;
        $this->validator = $validator;
        $this->queueEmailsRepository = $queueEmailsRepository;
    }

    public function resetPassword()
    {

        $this->beginTransaction();

        $this->_validate();

        $this->_configure();

        $this->_updatePasswordUser();

        //$this->_scheduleMail(); // Retirado 10/01/2020 -> Pedido Leonardo

        $this->commit();

        return $this->user;
    }

    private function _validate()
    {
        if ( $this->request->has('old_password') ) {
            $logged = $this->request->user()->toArray(true);
            if ( ! Hash::check($this->request->input('old_password'), $logged['password']) ) {
                $messageBag = new MessageBag([
                    0 => [
                        'useMessage' => true,
                        'message' => 'MUB.T55'
                    ]
                ]);
                throw new ExceptionGeneral($messageBag);
            }
            $this->user = $this->repository->skipPresenter()->find($logged['id']);
        }

        if ( ! $this->request->has('old_password') ) {
            $rules = [
                '_token' => 'required|max:255',
                'password' => 'required',
            ];

            $validator = Validator::make($this->request->all(), $rules);

            if ($validator->fails()) {
                throw new ValidationException($validator);
            }

            $this->user = $this->repository->skipPresenter()->findByField('remember_token', $this->request->input('_token'))->first();

            if (is_null($this->user)) {
                throw new \Dotenv\Exception\ValidationException('Invalid Token');
            }
        }

        return $this;
    }

    private function _configure()
    {
        $language = $this->request->input('appLanguage');

        $this->data = [
            'remember_token' => null,
            'password' => $this->request->input('password'),
            'active' => 1,
            'meta' => [
                "language" => $language,
            ],
            'full_name' => $this->user->full_name
        ];
    }

     private function _updatePasswordUser()
    {
        $this->validator->with($this->data)->passesOrFail(ValidatorInterface::RULE_UPDATE);

        $this->user = $this->repository->skipPresenter()->update($this->data, $this->user->id);

        $this->response['user'] = $this->user;
    }


    private function _scheduleMail()
    {

        $language = $this->request->input('appLanguage');

        $this->data['meta'] = Arr::add($this->data['meta'], 'full_name', $this->user->full_name);

        $this->queueEmailsRepository->skipPresenter()->create([
            'to_email' => $this->user->email,
            'to_name' => $this->user->full_name,
            'from_email' => env('MAILGUN_FROM_ADDRESS'),
            'from_name' => env('MAILGUN_FROM_NAME'),
            'subject' => 'Alteração de senha MUB!',
            'message' => 'ResetPasswordRegister',
            'active' => 2,
            'meta' => [
                "token" => $this->data['remember_token'],
                "full_name" => $this->user->full_name,
                "language" => $language
            ]
        ]); 

        if( !empty( $this->user->creator_user_id ) ) { // Mudou de local -> UserProfileUpdateByTokenService

            $this->creator_user = $this->repository->skipPresenter()->find( $this->user->creator_user_id );

            $this->queueEmailsRepository->skipPresenter()->create([
                'to_email' => $this->creator_user->email,
                'to_name' => $this->user->full_name,
                'from_email' => env('MAILGUN_FROM_ADDRESS'),
                'from_name' => env('MAILGUN_FROM_NAME'),
                'subject' => trans( 'email.T25', [ 'full_name' => $this->user->full_name ], $language ),
                'message' => 'registrationConfirmation',
                'active' => 2,
                'meta' => [
                    "token" => $this->data['remember_token'],
                    "full_name" => $this->user->full_name,
                    "language" => $language
                ]
            ]);
        }
    }

    private function _validateEmail()
    {
        if ( ! $this->request->has('email') ) {
            throw new ValidationException('Tem que ter email!');
        }
        return $this->request->input('email');
    }

}
