<?php

namespace App\Services;

use Illuminate\Http\Request;
use Illuminate\Contracts\Filesystem\Factory as Storage;
use Intervention\Image\Facades\Image;


class ImageService
{

	private $request;
    /**
     * @var Storage
     */
    private $storage;

    /**
     * @var string
     */
    private $disk = 'mub';

    private $defaultExtension = 'png';
    /**
     * ImageService constructor.
     * @param Request $request
     * @param Storage $storage
     */
    public function __construct(Request $request, Storage $storage) {
		$this->request = $request;
        $this->storage = $storage;
        $this->defaultExtension = env('MUB_DEFAULT_IMG_EXT', 'png');
    }

	public function store()
	{
		$image =  $this->request['image'];

		if( $image ) {

			$extension = '';
			if(!$extension)
				$extension = $this->defaultExtension;

			$filename = uniqid(date('HisYmd'));
			if($extension)
				$filename .= '.' . $extension;

			$directory = $this->getDirectory($filename);
			$path = $this->getPath() . $directory;

			$image_save = Image::make($image)->save($path);

			if( $image_save )
				return ['directory' => $directory , 'name' => $filename ];
		}

		return 'error';
	}

	public function show($directory = null)
    {

		$directory =  $this->getPath() . $this->getDirectory();

        if (file_exists($directory)) {

            $image = Image::make(file_get_contents($directory));

            return $image->response($this->defaultExtension);
        }

        return 'error';
    }

    public function destroy()
    {

    	$directory =  $this->getPath() . $this->getDirectory();

        if (file_exists($directory)) {
			$img = Image::make(file_get_contents($directory));
			return $img->destroy();
        }

        return 'error';
    }

    public function getPath()
    {
    	return $this->storage->disk($this->disk);
    }

    public function getDirectory($name = null)
    {

    	$uuid = $this->request['uuid'];

		if(empty($name)){
			$name = $this->request['image'];
			$name = explode("/", $name);
			$name = end($name);
		}

		if($uuid && $name) {
        	return '/'. $uuid . '/' . $name;
		}

		return;
    }

    public function base64Extension( $string )
    {
 	 	return substr($string, 11, strpos($string, ';') - 11);
	}

    public function saveBase64OnInstance($base64 = null, $instance = true)
    {
        if ( is_null($base64) ) {
            return null;
        }
        
        if ( $this->isBase64($base64) === false ) {
            if ( $this->fileExists($base64) ) {
                $base64 = $this->convertUrlToBase64($base64);
            }
        }

        $img = Image::make($base64)->resize(340, null, function ($constraint) {
            $constraint->aspectRatio();
        })->encode($this->defaultExtension, 80);

        if($instance)
            $path = $this->_makeFilePath();
        else
            $path = uniqid() . "." . $this->defaultExtension;

        $put = $this->storage->disk($this->disk)->put($path, $img);
        if ( $put ) {
            return $path;
        }
        return null;
    }

    private function _makeFilePath()
    {
        $uuid = request()->route('uuid');
        return $uuid . "/" . uniqid() . "." . $this->defaultExtension;
    }

    public function fileExists($file)
    {
        return $this->storage->disk($this->disk)->exists($file);
    }

    public function getFile($file)
    {
        return $this->storage->disk($this->disk)->get($file);
    }

    public function destroyFile($file)
    {
        return $this->storage->disk($this->disk)->delete($file);
    }

    public function isBase64($data)
    {
        try {
            list($type, $data) = explode(';', $data);
            list(, $data) = explode(',', $data);
            return base64_encode(base64_decode($data, true)) === $data;
        } catch (\Exception $exception) {
            return false;
        }
    }

    public function convertUrlToBase64($path)
    {
        $file =  $this->getFile($path);
        return (string) Image::make($file)->encode('data-url');
    }
}
