<?php


namespace App\Services;


use App\Repositories\FunctionGeneralRepository;
use App\Repositories\UsersProfilesRepository;
use App\Repositories\UsersRepository;

class PermissionService
{
    /**
     * @var FunctionGeneralRepository
     */
    private $functionGeneralRepository;

    private $path;
    private $profiles = [];
    private $users_id;
    private $instances_generals_id;
    private $hasPermission = false;
    /**
     * @var UsersProfilesRepository
     */
    private $usersProfilesRepository;

    /**
     * @var UsersRepository
     */
    private $usersRepository;

    /**
     * PermissionService constructor.
     * @param FunctionGeneralRepository $functionGeneralRepository
     * @param UsersProfilesRepository $usersProfilesRepository
     * @param UsersRepository $usersRepository
     */
    public function __construct(FunctionGeneralRepository $functionGeneralRepository, UsersProfilesRepository $usersProfilesRepository,UsersRepository $usersRepository)
    {
        $this->functionGeneralRepository = $functionGeneralRepository;
        $this->usersProfilesRepository = $usersProfilesRepository;
        $this->usersRepository = $usersRepository;
    }

    public function check()
    {
        $this->_setPath();
        $this->_setUserProfile();
        $this->_checkProfile();
        return [$this->hasPermission];
    }

    public function expiredToken()
    {
        $token = request()->header('Authorization', '');
        $token = explode('Bearer ',$token);
        $token = array_key_exists(1,$token) ? $token[1] : null;
        $user = $this->usersRepository->makeModel()->select('token_access')->find(request()->user()->id);
        if( !empty($user->token_access) && ( trim($user->token_access) !== trim($token) ) )
            return [true];

        return [false];
    }
    
    private function _setPath()
    {
        $path = request()->get('path');

        if ( is_null($path) ) {
            return;
        }

        $aPath = array_values(array_filter(explode('/', $path)));
        $this->path = "{$aPath[0]}/{$aPath[1]}";
        if ( count($aPath) > 2 ) {
            foreach ( $aPath as $key => $value ) {
                if ( $key <= 2 && $value !== 'form' ) {
                    continue;
                }
                $this->path .= "/" . $value;

            }
        }
    }

    private function _setUserProfile()
    {
        $this->users_id = request()->user()->id;
        $this->instances_generals_id = app()->make('getInstanceID');
        $this->profiles = $this->usersProfilesRepository->skipPresenter()->findWhere([
            'instances_generals_id' => $this->instances_generals_id,
            'users_id' => $this->users_id,
            'status' => 'A'
        ], ['profiles_generals_id'])->map(function($item, $key) {
            return $item->profiles_generals_id;
        });
    }

    private function _checkProfile()    
    {
        $functions = $this->functionGeneralRepository->skipPresenter()->with('profileFunctions')->findWhere(['path' => $this->path], ['id']);
        if ( $functions->isEmpty() ) {
            return;
        }

        foreach ( $functions as $function ) {
            if ( $this->hasPermission === true ) {
                continue;
            }
            if ( $function->profileFunctions->whereIn('profile_generals_id', $this->profiles)->count() > 0 ) {
                $this->hasPermission = true;
            }
        }
    }
}
