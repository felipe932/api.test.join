<?php
/**
 * Created by PhpStorm.
 * User: eduardo
 * Date: 25/06/19
 * Time: 14:29
 */

namespace App\Services;

use App\Events\CompanyCreated;
use App\Exceptions\ExceptionGeneral;
use App\Helpers\Strings;
use App\Repositories\QueueEmailsRepository;
use App\Repositories\UsersRepository;
use App\Repositories\InstancesGeneralsRepository;
use App\Traits\MyDatabaseTransactions;
use App\Validators\UsersValidator;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;

class TrialService
{
    use MyDatabaseTransactions;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var UsersRepository
     */
    private $repository;
    /**
     * @var InstancesGeneralsRepository
     */
    private $instancesGeneralsRepository;
    /**
     * @var UsersValidator
     */
    private $validator;

    private $data = [];

    private $user;

    /**
     * @var QueueEmailsRepository
     */
    private $queueEmailsRepository;

    /**
     * TrialService constructor.
     * @param Request $request
     * @param UsersRepository $repository
     * @param InstancesGeneralsRepository $instancesGeneralsRepository
     * @param UsersValidator $validator
     * @param QueueEmailsRepository $queueEmailsRepository
     */
    public function __construct(
        Request $request,
        UsersRepository $repository,
        InstancesGeneralsRepository $instancesGeneralsRepository,
        UsersValidator $validator,
        QueueEmailsRepository $queueEmailsRepository
    ){
        $this->request = $request;
        $this->repository = $repository;
        $this->validator = $validator;
        $this->queueEmailsRepository = $queueEmailsRepository;
        $this->instancesGeneralsRepository = $instancesGeneralsRepository;
    }

    /**
     * @return mixed
     * @throws ExceptionGeneral
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function trial()
    {
        $this->beginTransaction();

        $this->_checkEmailInstances();
        $this->_configure();
        $this->_createUser();
        $this->_scheduleMail();

        $this->commit();

        return $this->user;
    }

    private function _checkEmailInstances()
    {
        if ( ! $this->request->has('email') ) {
            return;
        }

        Strings::checkEmailOperationAllowed($this->request->input('email'));

        $collection = $this->repository
            ->with('instances.profile')
            ->findWhere(['email' => $this->request->input('email')]);

        if ( $collection->isEmpty() ) {
            return;
        }

        $isAdminMaster = 0;
        foreach ( $collection as $item )
        {
            foreach( $item->instances as $instance )
            {

                if( empty( $instance->profile->instance ) )
                    continue;

                if ( $instance->profile->code === 'ADMIN_MASTER') {

                    $isAdminMaster++;
                }
            }
        }

        if ( $isAdminMaster === 0 ) {
            return;
        }

        $messageBag = new MessageBag([
            0 => [
                'useMessage' => true,
                'message' => 'MSG.T51'
            ]
        ]);

        throw new ExceptionGeneral($messageBag);
    }

    private function _configure()
    {
        $this->data = [
            'full_name' => 'TRIAL',
            'email' => $this->request->input('email'),
            'languages_generals_id' => '46',
            'remember_token' => $this->_generateRememberToken(),
            'active' => 0,
            'userExists' => false
        ];

        $query = $this->repository->findWhere(['email' => $this->request->input('email')]);

        if ( $query->isEmpty() ) {
            return;
        }

        $this->user = $query->first();

        $this->data['userExists'] = true;
        $this->data['full_name'] = $this->user->full_name;
    }

    /**
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    private function _createUser()
    {
        if( ! is_null($this->user) ) {
            $this->user = $this->repository->update(['remember_token' => $this->data['remember_token']], $this->user->id);
            return;
        }
        $this->validator->with($this->data)->passesOrFail($this->validator::RULE_CREATE);
        $this->user = $this->repository->create($this->data);
    }

    private function _generateRememberToken()
    {
        return Str::random(100);
    }

    private function _scheduleMail()
    {
        $language = $this->request->input('appLanguage');

        $this->queueEmailsRepository->skipPresenter()->create([
            'to_email' => $this->user->email,
            'to_name' => $this->user->email,
            'from_email' => env('MAILGUN_FROM_ADDRESS'),
            'from_name' => env('MAILGUN_FROM_NAME'),
            'subject' => trans( 'email.T1', [], $language ),
            'message' => 'CompleteRegister',
            'active' => 2,
            'meta' => [
                "token" => $this->data['remember_token'],
                "userExists" => $this->data['userExists'],
                "full_name" =>  $this->data['full_name'],
                "language" => $language
            ]
        ]);
    }

    /** =================================================== REGISTER ================================================== */

    /**
     * @throws ValidationException
     */
    private function _validate()
    {
        /**
         * Specific validator for this operation, because this operation it's not transacted.
         */
        $rules = [
            '_token' => 'required|max:255',
            'full_name' => 'required|max:255',
            'name' => 'required|max:255',
            'sub_domain' => 'required|max:255|unique:companies_generals',
            'languages_generals_id' => 'required|integer|exists:languages_generals,id',
            'password' => 'required',
        ];

        $validator = Validator::make($this->request->all(), $rules);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $this->user = $this->repository->skipPresenter()->findByField('remember_token', $this->request->input('_token'))->first();

        if ( is_null($this->user) ) {
            throw new \Dotenv\Exception\ValidationException('Invalid Token');
        }
    }

}
