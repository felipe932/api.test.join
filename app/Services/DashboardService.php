<?php

namespace App\Services;

use App\Helpers\Arrays;
use App\Helpers\Strings;
use App\Repositories\ItemGeneralRepository;
use App\Repositories\ItemPartnumbersRepository;
use App\Repositories\CategoryGeneralRepository;
use App\Repositories\CategoryPublicGeneralsRepository;
use App\Repositories\TaxonomyRulesGeneralsRepository;
use App\Repositories\AttributeGeneralRepository;
use App\Repositories\VendorsGeneralsRepository;
use App\Repositories\UsersFavoritesRepository;
use App\Repositories\FixedValuesDomainRepository;
use App\Repositories\LanguagesGeneralRepository;
use App\Repositories\WorkflowStepsGeneralsRepository;
use App\Repositories\WorkflowGeneralsRepository;
use App\Repositories\FavoritesGroupGeneralsRepository;
use App\Repositories\FavoritesRepository;
use App\Criteria\WorkflowStepsBaseWhereCriteria;
use App\Criteria\WorkflowUserGroupCriteria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;
use App;

class DashboardService
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var ItemGeneralRepository
     */
    private $repositoryItemGeneral;

     /**
     * @var ItemPartnumbersRepository
     */
    private $repositoryItemPartnumbers;

    /**
     * @var CategoryGeneralRepository
     */
    private $repositoryCategoryGeneral;

    /**
     * @var CategoryPublicGeneralsRepository
     */
    private $repositoryCategoryPublicGenerals;

    /**
     * @var TaxonomyRulesGeneralsRepository
     */
    private $repositoryTaxonomyRulesGeneral;

    /**
     * @var AttributeGeneralRepository
     */
    private $repositoryAttributeGeneral;

    /**
     * @var VendorsGeneralsRepository
     */
    private $repositorVendorsGeneral;

     /**
     * @var UsersFavoritesRepository
     */
    private $repositorUsersFavorites;

     /**
     * @var FixedValuesDomainRepository
     */
    private $repositorFixedValuesDomain;

     /**
     * @var LanguagesGeneralRepository
     */
    private $repositorLanguagesGeneral;

    /**
     * @var WorkflowStepsGeneralsRepository
     */
    protected $repositoryWorkflowStepsGenerals;

    /**
     * @var WorkflowGeneralsRepository
     */
    private $repositoryWorkflowGenerals;

    /**
     * @var FavoritesGroupGeneralsRepository
     */
    protected $repositoryFavoritesGroupGenerals;

    /**
     * @var FavoritesRepository
     */
    protected $repositoryFavorites;

    private $user_id = null;
    private $condition = null;

    //Items
    private $count_items = null;
    private $count_item_category_technical = null;
    private $percentual_item_category_techical_by_total_items = null;
    private $widthPercentItem = null;
    private $graphicItem = null;
    private $graphicItemLabel = null;
    private $graphicItemMedia = 0;

    //vendors
    private $count_vendors = null;

    //Category
    private $count_category_technical = null;
    private $count_taxonomy_rules = null;
    private $percentual_category_techical_by_taxonomy_rules = null;
    private $topCategoryItems = null;
    private $top = null;

    // Time Line
    private $timeline = [];
    private $arrayItemsCreated = [];
    private $arrayItemsUpdated = [];
    private $arrayVendorsCreated = [];
    private $arrayVendorsUpdated = [];
    private $arrayCategoriesCreated = [];
    private $arrayCategoriesUpdated = [];
    private $arrayAttributeCreated = [];
    private $arrayAttributeUpdated = [];
    private $whereRawCreated = null;
    private $whereRawUpdated = null;

    // Favorites
    private $favorites = [];
    private $process_favorites = [];

    // WorkFlows
    private $pending_task = [];
    private $pending_task_with_sla_expired = 0;
    private $percent_task_with_sla_expired = 0;
    private $my_requests_total = 0;
    private $my_requests_pending = 0;

    // graph
    private $object = 'VENDOR';  // retirar, e deixar null

    /**
     * DashboardService constructor.
     * @param Request $request
     * @param ItemGeneralRepository $repositoryItemGeneral
     * @param ItemPartnumbersRepository $repositoryItemPartnumbers
     * @param CategoryGeneralRepository $repositoryCategoryGeneral
     * @param TaxonomyRulesGeneralsRepository $repositoryTaxonomyRulesGeneral
     * @param AttributeGeneralRepository $repositoryAttributeGeneral
     * @param VendorsGeneralsRepository $repositorVendorsGeneral
     * @param UsersFavoritesRepository $repositorUsersFavorites
     * @param FixedValuesDomainRepository $repositorFixedValuesDomain
     * @param LanguagesGeneralRepository $repositorLanguagesGeneral
     * @param WorkflowStepsGeneralsRepository $repositoryWorkflowStepsGenerals
     * @param FavoritesGroupGeneralsRepository $repositoryFavoritesGroupGenerals
     * @param CategoryPublicGeneralsRepository $repositoryCategoryPublicGenerals
     * @param WorkflowGeneralsRepository $repositoryWorkflowGenerals
     * @param FavoritesRepository $repositoryFavorites
     */
    public function __construct(
        Request $request,
        ItemGeneralRepository $repositoryItemGeneral,
        ItemPartnumbersRepository $repositoryItemPartnumbers,
        CategoryGeneralRepository $repositoryCategoryGeneral,
        TaxonomyRulesGeneralsRepository $repositoryTaxonomyRulesGeneral,
        AttributeGeneralRepository $repositoryAttributeGeneral,
        VendorsGeneralsRepository $repositorVendorsGeneral,
        UsersFavoritesRepository $repositorUsersFavorites,
        FixedValuesDomainRepository $repositorFixedValuesDomain,
        LanguagesGeneralRepository $repositorLanguagesGeneral,
        WorkflowStepsGeneralsRepository $repositoryWorkflowStepsGenerals,
        FavoritesGroupGeneralsRepository $repositoryFavoritesGroupGenerals,
        CategoryPublicGeneralsRepository $repositoryCategoryPublicGenerals,
        WorkflowGeneralsRepository $repositoryWorkflowGenerals,
        FavoritesRepository $repositoryFavorites
    ){

        set_time_limit(0);
        ini_set('memory_limit', '20000M');

        $this->request = $request;
        $this->repositoryItemGeneral = $repositoryItemGeneral;
        $this->repositoryItemPartnumbers = $repositoryItemPartnumbers;
        $this->repositoryCategoryGeneral = $repositoryCategoryGeneral;
        $this->repositoryTaxonomyRulesGeneral = $repositoryTaxonomyRulesGeneral;
        $this->repositoryAttributeGeneral = $repositoryAttributeGeneral;
        $this->repositorVendorsGeneral = $repositorVendorsGeneral;
        $this->repositorUsersFavorites = $repositorUsersFavorites;
        $this->repositorFixedValuesDomain = $repositorFixedValuesDomain;
        $this->repositorLanguagesGeneral  = $repositorLanguagesGeneral;
        $this->repositoryFavoritesGroupGenerals = $repositoryFavoritesGroupGenerals;
        $this->repositoryCategoryPublicGenerals = $repositoryCategoryPublicGenerals;
        $this->repositoryWorkflowGenerals = $repositoryWorkflowGenerals;
        $this->repositoryWorkflowStepsGenerals = $repositoryWorkflowStepsGenerals;
        $this->repositoryFavorites = $repositoryFavorites;

        $this->top = 10;
    }

    public function general()
    {
        $this->_getTotalItems();
        $this->_getGraphicItem();

        $this->_getItemCategoryTechnicalNotPublicCollection();
        $this->_getPercentualItemCategoryTechicalByTotalItems();
        $this->_getCategoryTechnical();
        $this->_getTaxinomyRules();
        $this->_getPercentualCategoryTecnicalByTaxinomyRyles();

        $this->_getTopCategoryItems();
        $this->_getFavorites();
        $this->_getTimeLine();

        $collection = [
            'total_item' => $this->count_items,
            'graphicItemLabel' => '1week',
            'graphicItem' => $this->graphicItem,
            'graphicItemMedia' => $this->graphicItemMedia,
            'total_item_classification' => $this->count_item_category_technical,
            'percent_item' => $this->percentual_item_category_techical_by_total_items,
            'width_percent_item' => $this->widthPercentItem,
            'total_category_technical' => $this->count_category_technical,
            'total_taxonomy_rule' => $this->count_taxonomy_rules,
            'percent_pds' => $this->percentual_category_techical_by_taxonomy_rules,
            'topCategoryItems' => $this->topCategoryItems,
            'favorites' => $this->favorites,
            'timelineLabel' => '1week', // uma semana default
            'timeline' => $this->timeline
        ];

        return $collection;
    }

    public function graphic_items(){
        $this->_getGraphicItem();
        return [
            'graphicItemLabel' => $this->condition,
            'graphicItem' => $this->graphicItem
        ];
    }

    public function _getTotalItems()
    {
        $collection = $this->repositoryItemGeneral->makeModel()->where('status','A')->get();
        $this->count_items = count($collection);
    }

    public function _getTotalVendors()
    {
        $collection = $this->repositorVendorsGeneral->makeModel()->where('status','A')->get();
        $this->count_vendors = count($collection);
    }

    public function _getGraphicItem()
    {
        $this->graphicItemMedia = 0;
        $this->condition = $this->request->input('type');
        $this->user_id = $this->request->user()->id;
        $today = Carbon::yesterday();
        switch($this->condition){
            case '1week':
                $date_inicial = $today->sub('1 week') . PHP_EOL;
                $date_inicial = Carbon::parse($date_inicial);
                $this->_getInterval($date_inicial);
                $this->graphicItemMedia = $this->graphicItemMedia / 7;
            break;
            case '1month':
                $date_inicial = $today->sub('1 month') . PHP_EOL;
                $date_inicial = Carbon::parse($date_inicial);
                $this->_getInterval($date_inicial);
                $this->graphicItemMedia = $this->graphicItemMedia / 180;
            break;
            case '6month':
                $yesterday = Carbon::yesterday();
                $yesterday_concat = date( 'Y-m' , strtotime($yesterday));
                $date_inicial = Carbon::parse($yesterday_concat.'-01');
                $date_inicial = $date_inicial->sub('5 month') . PHP_EOL;
                $this->_getInterval($date_inicial);
            break;
            case '1year':
                $yesterday = Carbon::yesterday();
                $yesterday_concat = date( 'Y-m' , strtotime($yesterday));
                $date_inicial = Carbon::parse($yesterday_concat.'-01');
                $date_inicial = $date_inicial->sub('1 year') . PHP_EOL;
                $date_inicial = Carbon::parse($date_inicial);
                $date_inicial = $date_inicial->add('1 month') . PHP_EOL;
                $this->_getInterval($date_inicial);
            break;
            case 'custom':
                $date_inicial = $this->request->input('date_inicial');
                $date_final = $this->request->input('date_final');
                $this->_getInterval($date_inicial,$date_final);
            break;
            default:
                $date_inicial = $today->sub('1 week') . PHP_EOL;
                $date_inicial = Carbon::parse($date_inicial);
                $this->_getInterval($date_inicial);
            break;
        }
    }

    public function _getInterval($date_inicial , $dataFim = null)
    {
        $today = Carbon::today();
        $limit = $today->sub('1 day'). PHP_EOL;

        if(!empty($dataFim))
            $limit = $dataFim;

        while ( $date_inicial < $limit)
        {
            $date_final = Carbon::parse($date_inicial);

            if( empty($this->condition) || $this->condition === '1week' || $this->condition === '1month' ) // por dias
                $date_final = $date_final->add('1 day') . PHP_EOL;
            else
                $date_final = $date_final->add('1 month') . PHP_EOL;

            $date_inicial = $this->getTotalItem( $date_inicial, $date_final );
            $date_inicial = Carbon::parse($date_inicial);
        }
    }

    public function getTotalItem( $date_inicial, $date_final ){

        $andWhere = " ";
        if( !empty($this->user_id) )
            //$andWhere = " and  ( users_create_id = '". $this->user_id ."' or users_update_id = '" . $this->user_id . "' ) ";

        $itens = [];

        if( !empty($this->object) )
        {
            switch($this->object)
            {
                case 'ITEM':
                    $itens = $this->repositoryItemGeneral->makeModel()
                        ->whereRaw( " ( created_at >= '" . $date_inicial ."' and created_at <= '". $date_final."' ) ".$andWhere )
                        ->get();
                break;
                case 'VENDOR':
                    $itens = $this->repositorVendorsGeneral->makeModel()
                        ->whereRaw( " ( created_at >= '" . $date_inicial ."' and created_at <= '". $date_final."' ) ".$andWhere )
                        ->get();
                break;
                case 'CATEGORY':
                    $itens = $this->repositoryCategoryGeneral->makeModel()
                        ->whereRaw( " type = 'TEC' and ( created_at >= '" . $date_inicial ."' and created_at <= '". $date_final."' ) ".$andWhere )
                        ->get();
                break;
                case 'PD':
                    $itens = $this->repositoryTaxonomyRulesGeneral->makeModel()
                        ->whereRaw( " ( created_at >= '" . $date_inicial ."' and created_at <= '". $date_final."' ) ".$andWhere )
                        ->get();
                break;
            }
        } else {

            $itens = $this->repositoryItemGeneral->makeModel()
                ->whereRaw( " ( created_at >= '" . $date_inicial ."' and created_at <= '". $date_final."' ) ".$andWhere )
                ->get();
        }

        $label = date( 'm/Y' , strtotime($date_inicial)); // mes / ano
        if( empty($this->condition) || $this->condition === '1week' || $this->condition === '1month' ) // dia / mes
            $label = date( 'd/m' , strtotime($date_inicial));

        $this->graphicItemMedia += count($itens);

        $this->graphicItem[] = [
            'label' => $label,
            'total_item' => count($itens),
            'total_media' => 0
        ];

        return $date_final;
    }

    public function _getItemCategoryTechnicalNotPublicCollection()
    {
        $collection = $this->repositoryItemGeneral->makeModel()
            ->technicalCategoryNotPublic()
            ->get();
        $this->count_item_category_technical = count($collection);
    }

    public function _getPercentualItemCategoryTechicalByTotalItems(){
        if($this->count_items > 0 && $this->count_item_category_technical > 0) {
            $percentual = ( ( $this->count_item_category_technical / $this->count_items ) * 100 );
            $this->percentual_item_category_techical_by_total_items = number_format($percentual, 2, ',', ' ');
            $this->widthPercentItem = number_format($percentual, 2, '.', ' ');
        }
    }

    public function _getCategoryTechnical()
    {
        $collection = $this->repositoryCategoryGeneral->makeModel()
            ->where('status','A')
            ->where('type','TEC')
            ->get();

        $this->count_category_technical = count($collection);
    }

    public function _getTaxinomyRules(){

        $taxonomy_rule_group_by_category_generals_id = $this->repositoryTaxonomyRulesGeneral->makeModel()
            ->select('category_generals_id')
            ->where('status','A')
            ->groupBy('category_generals_id')
            ->get();
        $this->count_taxonomy_rules = count($taxonomy_rule_group_by_category_generals_id);
    }

    public function _getPercentualCategoryTecnicalByTaxinomyRyles() {

        if($this->count_category_technical > 0 && $this->count_taxonomy_rules > 0){
            $this->percentual_category_techical_by_taxonomy_rules = ($this->count_taxonomy_rules / $this->count_category_technical ) * 100;
            $this->percentual_category_techical_by_taxonomy_rules = number_format($this->percentual_category_techical_by_taxonomy_rules, 2, ',', ' ');
        }
    }

    public function _getLanguage(){
        $language = $this->repositorLanguagesGeneral->makeModel()
            ->where('language_code',trim($this->request->input('appLanguage')))
            ->first();
        if(!empty($language)){
            return $language->id;
        }
        return null;
    }

    public function _getObject( $object ){

        $languages_generals_id = $this->_getLanguage();
        if(empty($languages_generals_id))
            $languages_generals_id = 46; // en

        $this->repositorFixedValuesDomain->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));

        $collection = $this->repositorFixedValuesDomain->makeModel()
            ->join('fixed_values_domain_descriptions','fixed_values_domain_descriptions.fixed_values_domains_id','=','fixed_values_domains.id')
            ->where('fixed_values_domain_descriptions.languages_generals_id', $languages_generals_id )
            ->where('fixed_values_domains.object', 'FavoriteTransactions')
            ->where('fixed_values_domains.key', $object)
            ->first();

        $description = '';
        if(!empty($collection))
            $description = $collection->description;

        return $description;
    }

    public function _getPic( $object )
    {

        $pic = '';
        $url = './assets/media/mub/icons/';

        if(array_key_exists(2 , $object ) ) {

            switch( $object[2] ){
                case 'items':
                    $pic = 'Item.svg';
                break;
                case 'vendors':
                    $pic = 'Fab.svg';
                break;
                case 'attributes':
                    $pic = 'Atr.svg';
                break;
                case 'categories':
                    $pic = 'Cat.svg';
                break;
                case 'categories-hierarchies':
                    $pic = 'Arv.svg';
                break;
                case 'file-list-uploads':
                    $pic = 'Imp.svg';
                break;
                case 'search':
                    $pic = 'Search.svg';
                break;
                case 'taxonomy-rules':
                    $pic = 'Pds.svg';
                break;
                case 'abbreviations':
                    $pic = 'Abr.svg';
                break;
                case 'equivalents-terms':
                    $pic = 'Ter.svg';
                break;
                case 'descriptions-types':
                    $pic = 'Tde.svg';
                break;
                case 'units-of-measures':
                    $pic = 'Uom.svg';
                break;
                case 'users':
                    $pic = 'Usu.svg';
                break;
                case 'profiles':
                    $pic = 'Usu.svg';
                break;
            }
        }

        return $url.$pic;
    }

    public function _getPicObject( $object )
    {
        $pic = '';
        $url = './assets/media/mub/icons/';

        switch( $object ){
            case 'items':
                $pic = 'Item.svg';
            break;
            case 'vendors':
                $pic = 'Fab.svg';
            break;
            case 'attributes':
                $pic = 'Atr.svg';
            break;
            case 'categories':
                $pic = 'Cat.svg';
            break;
        }
        return $url.$pic;
    }

    public function _getTimeLine(){

        $this->_getCondition();

        $this->getLatterItemCreated();
        $this->getLatterItemUpdated();

        $this->getLatterCategoryCreated();
        $this->getLatterCategoryUpdated();

        $this->getLatterAttributeCreated();
        $this->getLatterAttributeUpdated();

        $this->getLatterVendorCreated();
        $this->getLatterVendorUpdated();

        $this->_mergeTimeLine();
    }

    public function _getCondition(){

        $this->condition = $this->request->input('type');

        $this->user_id = $this->request->user()->id;

        switch($this->condition)
        {
            case '1day':
                $date = Carbon::today(); // 1 day
            break;
            case '1week':
                $date = Carbon::today();
                $date = $date->sub('1 week') . PHP_EOL;
            break;
            case '1month':
                $date = Carbon::today();
                $date = $date->sub('1 month') . PHP_EOL;
            break;
            default:
            case '1week':
                $date = Carbon::today();
                $date = $date->sub('1 week') . PHP_EOL;
            break;
        }

        $orWhere = '';
        if(!empty($this->user_id))
            $orWhere = " and  ( users_create_id = '". $this->user_id ."' or users_update_id = '" . $this->user_id . "' ) ";

        $this->whereRawCreated = " ( created_at >= '" . $date . "' ) " . $orWhere;
        $this->whereRawUpdated = " ( updated_at >= '" . $date . "' ) " . $orWhere;
    }

    private function _mergeTimeLine()
    {
        $this->timeline = array_merge(
            $this->arrayItemsCreated,
            $this->arrayItemsUpdated,
            $this->arrayCategoriesCreated,
            $this->arrayCategoriesUpdated,
            $this->arrayAttributeCreated,
            $this->arrayAttributeUpdated,
            $this->arrayVendorsCreated,
            $this->arrayVendorsUpdated
        );
        $this->timeline = Arrays::orderByArray($this->timeline, 'date', 'desc');
    }

    public function getFormatDate($item,$type){
        $data_create_update = null;
        if($item->users_create_id === $this->user_id){
            if($type ==='created_at')
                $data_create_update = $item->created_at;
            if($type ==='updated_at')
                $data_create_update = $item->updated_at;
        }
        if($data_create_update)
            return date( 'd/m/Y H:i:s' , strtotime($data_create_update));

        return null;
    }

    public function getLatterItemCreated()
    {
        $this->repositoryItemGeneral->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $collection = $this->repositoryItemGeneral->makeModel()
            ->whereRaw($this->whereRawCreated)
            ->orderBy('created_at','desc')
            ->limit(50)
            ->get();
        $this->arrayItemsCreated = $collection->map(function($item) {
            $item->date = $item->created_at;
            $item->time = $this->getFormatDate($item,'created_at');
            $item->icon = 'fa fa-genderless kt-font-info';
            $item->text = $item->code;
            $item->action = 'created';
            $item->object = 'items';
            $item->url_icon = $this->_getPicObject('items');
            return $item;
        })->toArray();
    }

    public function getLatterItemUpdated()
    {
        $this->repositoryItemGeneral->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $collection = $this->repositoryItemGeneral->makeModel()
            ->whereRaw($this->whereRawUpdated)
            ->orderBy('updated_at','desc')
            ->limit(50)
            ->get();
        $this->arrayItemsUpdated = $collection->map(function($item) {
            $create = $this->getFormatDate($item,'created_at');
            $update = $this->getFormatDate($item,'updated_at');
            if( $update > $create ){
                $item->date = $item->updated_at;
                $item->time = $this->getFormatDate($item,'updated_at');
                $item->icon = 'fa fa-genderless kt-font-info';
                $item->text = $item->code;
                $item->action = 'updated';
                $item->object = 'items';
                $item->url_icon = $this->_getPicObject('items');
                return $item;
            }
        })->toArray();

        $this->arrayItemsUpdated = array_filter($this->arrayItemsUpdated);
    }

    public function getLatterCategoryCreated()
    {
        $this->repositoryCategoryGeneral->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $collection = $this->repositoryCategoryGeneral->makeModel()
            ->whereRaw($this->whereRawCreated)
            ->orderBy('created_at','desc')
            ->limit(50)
            ->get();
        $this->arrayCategoriesCreated = $collection->map(function($category) {
            $category->date = $category->created_at;
            $category->time = $this->getFormatDate($category,'created_at');
            $category->icon = 'fa fa-genderless kt-font-danger';
            $category->text = $category->code . ': '.$category->description;
            $category->action = 'created';
            $category->object = 'categories';
            $category->url_icon = $this->_getPicObject('categories');
            return $category;
        })->toArray();
    }

    public function getLatterCategoryUpdated()
    {
        $this->repositoryCategoryGeneral->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $collection = $this->repositoryCategoryGeneral->makeModel()
            ->whereRaw($this->whereRawUpdated)
            ->orderBy('updated_at','desc')
            ->limit(50)
            ->get();
        $this->arrayCategoriesUpdated = $collection->map(function($category) {
            $create = $this->getFormatDate($category,'created_at');
            $update = $this->getFormatDate($category,'updated_at');
            if( $update > $create ){
                $category->date = $category->updated_at;
                $category->time = $this->getFormatDate($category,'updated_at');
                $category->icon = 'fa fa-genderless kt-font-danger';
                $category->text = $category->code . ': '.$category->description;
                $category->action = 'updated';
                $category->object = 'categories';
                $category->url_icon = $this->_getPicObject('categories');
                return $category;
            }
        })->toArray();

        $this->arrayCategoriesUpdated = array_filter($this->arrayCategoriesUpdated);
    }

    public function getLatterAttributeCreated()
    {
        $this->repositoryAttributeGeneral->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $collection = $this->repositoryAttributeGeneral->makeModel()
            ->whereRaw($this->whereRawCreated)
            ->orderBy('created_at','desc')
            ->limit(50)
            ->get();
        $this->arrayAttributeCreated = $collection->map(function($attribute) {
            $attribute->date = $attribute->created_at;
            $attribute->time = $this->getFormatDate($attribute,'created_at');
            $attribute->icon = 'fa fa-genderless kt-font-success';
            $attribute->text = $attribute->code . ' - '.$attribute->description;
            $attribute->action = 'created';
            $attribute->object = 'attributes';
            $attribute->url_icon = $this->_getPicObject('attributes');
            return $attribute;
        })->toArray();
    }

    public function getLatterAttributeUpdated()
    {
        $this->repositoryAttributeGeneral->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $collection = $this->repositoryAttributeGeneral->makeModel()
            ->whereRaw($this->whereRawUpdated)
            ->orderBy('updated_at','desc')
            ->limit(50)
            ->get();
        $this->arrayAttributeUpdated = $collection->map(function($attribute) {
            $create = $this->getFormatDate($attribute,'created_at');
            $update = $this->getFormatDate($attribute,'updated_at');
            if( $update > $create ){
                $attribute->date = $attribute->updated_at;
                $attribute->time = $this->getFormatDate($attribute,'updated_at');
                $attribute->icon = 'fa fa-genderless kt-font-success';
                $attribute->text = $attribute->code . ' - '.$attribute->description;
                $attribute->action = 'updated';
                $attribute->object = 'attributes';
                $attribute->url_icon = $this->_getPicObject('attributes');
                return $attribute;
            }
        })->toArray();

        $this->arrayAttributeUpdated = array_filter($this->arrayAttributeUpdated);
    }

    public function getLatterVendorCreated()
    {
        $this->repositorVendorsGeneral->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $collection = $this->repositorVendorsGeneral->makeModel()
            ->whereRaw($this->whereRawCreated)
            ->orderBy('created_at','desc')
            ->limit(50)
            ->get();
        $this->arrayVendorsCreated = $collection->map(function($vendor) {
            $vendor->date = $vendor->created_at;
            $vendor->time = $this->getFormatDate($vendor,'created_at');
            $vendor->icon = 'fa fa-genderless kt-font-warning';
            $vendor->text = $vendor->name;
            $vendor->action = 'created';
            $vendor->object = 'vendors';
            $vendor->url_icon = $this->_getPicObject('vendors');
            return $vendor;
        })->toArray();
    }

    public function getLatterVendorUpdated()
    {
        $this->repositorVendorsGeneral->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $collection = $this->repositorVendorsGeneral->makeModel()
            ->whereRaw($this->whereRawUpdated)
            ->orderBy('updated_at','desc')
            ->limit(50)
            ->get();
        $this->arrayVendorsUpdated = $collection->map(function($vendor) {
            $create = $this->getFormatDate($vendor,'created_at');
            $update = $this->getFormatDate($vendor,'updated_at');
            if( $update > $create ){
                $vendor->date = $vendor->updated_at;
                $vendor->time = $this->getFormatDate($vendor,'updated_at');
                $vendor->icon = 'fa fa-genderless kt-font-warning';
                $vendor->text = $vendor->name;
                $vendor->action = 'updated';
                $vendor->object = 'vendors';
                $vendor->url_icon = $this->_getPicObject('vendors');
                return $vendor;
            }
        })->toArray();

        $this->arrayVendorsUpdated = array_filter($this->arrayVendorsUpdated);
    }

    public function _getTopCategoryItems()
    {
        if(!empty($this->request->input('top'))){
            $this->top = $this->request->input('top');
        }

        $categoriesWithItems = [];

        $collectionCategoriesWithItems = $this->repositoryCategoryGeneral->makeModel()
            ->with('items')
            ->whereRaw(" type = 'TEC' ")
            ->get(['id','code']);

        if(count($collectionCategoriesWithItems)>0) {
            foreach ( $collectionCategoriesWithItems as $category ) {
                if ( count($category->items) === 0 ) {
                    continue;
                }
                $categoriesWithItems[] = [
                    'label' => $category->code,
                    'total_item' => count($category->items)
                ];
            }
        }

        $this->_getCategoryItems($categoriesWithItems);
    }

    public function _getCategoryItems($categoriesWithItems)
    {
        $topCategories = $categoriesWithItems;

        if(count($categoriesWithItems) > $this->top){
            $i = 0;
            $topCategories = [];
            foreach($categoriesWithItems as $category){
                if( $i < $this->top ){
                    $topCategories[] = $category;
                }else{
                    $this->topCategoryItems = $topCategories;
                    $this->topCategoryItems = Arrays::orderByArray($this->topCategoryItems, 'total_item', 'desc');
                    continue;
                }
                $i++;
            }
        }else{
            $this->topCategoryItems = $topCategories;
            $this->topCategoryItems = Arrays::orderByArray($this->topCategoryItems, 'total_item', 'desc');
        }
    }

    public function _getFavorites(){

        $collection = $this->repositorUsersFavorites->makeModel()
            ->where('users_id',$this->user_id)
            ->get();

        $this->favorites = $collection->map(function($favorite) {

            $object = explode('/',$favorite->object);
            $picUrl = $this->_getPic($object);

            $favorite->pic = $picUrl;
            $favorite->title = $this->_getObject($favorite->object);
            $favorite->url = $favorite->object;

            return $favorite;

        })->toArray();
    }

    /* news */

    public function timeline(){

        $this->_getTimeLine();
        return [
            'timelineLabel' => $this->condition,
            'timeline' => $this->timeline
        ];
    }

    public function favorites(){

        $this->user_id = $this->request->user()->id;

        $this->_getFavorites();
        return [
            'favorites' => $this->favorites
        ];
    }

    public function process_favorites_group()
    {
        $this->user_id = $this->request->user()->id;

        $collection = $this->repositoryFavoritesGroupGenerals->makeModel()
            ->with('favorites')
            ->where('users_id',$this->user_id)
            ->where('object','process_generals')
            ->get();

        $this->process_favorites = $collection->map(function($process_favorite) { return $process_favorite;})->toArray();
        return [
            'process_favorites_group' => $this->process_favorites
        ];
    }

    public function process_favorites()
    {
        $this->user_id = $this->request->user()->id;

        $collection = $this->repositoryFavorites->makeModel()
            ->with('favoriteGroupGeneral')
            ->with('processGeneral')
            ->where('users_id',$this->user_id)
            ->where('object','process_generals')
            ->get();

        $this->process_favorites = $collection->map(function($process_favorite) { return $process_favorite;})->toArray();

        return [
            'process_favorites' => $this->process_favorites
        ];
    }

    public function totals_for_instance()
    {
        $this->_getTotalItems();
        $this->_getTotalVendors();
        $this->_getCategoryTechnical();
        $this->_getTaxinomyRules();
        $this->_getPercentualCategoryTecnicalByTaxinomyRyles();

        $collection = [
            'total_item' => $this->count_items,
            'total_vendor' => $this->count_vendors,
            'total_category_technical' => $this->count_category_technical,
            'total_taxonomy_rule' => $this->count_taxonomy_rules,
            'percent_pds' => $this->percentual_category_techical_by_taxonomy_rules,
        ];

        return $collection;
    }

    public function totals_for_workflow()
    {

        $users_id = request()->user()->id;
        $whereIn = ['PENDING','RESERVED','INCORRECTION'];

        $this->_getWorkflowSteps( $users_id, $whereIn );
        $this->_getMyRequests( $users_id, $whereIn );

        $collection = [
            'pending_task' => $this->pending_task->count(),
            'pending_task_with_sla_expired' => $this->_calcSLA('count'),
            'percent_task_with_sla_expired' => $this->_calcSLA("percent"),
            'my_requests_total' => $this->my_requests_total->count(),
            'my_requests_pending' => $this->my_requests_pending->count(),
        ];

        return $collection;
    }

    private function _calcSLA($type)
    {
        $pendingTasks = $this->pending_task->count();
        $overdueTasks = $this->pending_task->filter(function($item) {
            return $this->_isOverdueTask($item) > 100;
        })->count();
        switch ($type) {
            case 'count':
                return $overdueTasks;
                break;
            case 'percent':
                if ( $pendingTasks === 0 ) {
                    return 0;
                }
                return dec($overdueTasks*100/$pendingTasks);
                break;
            default:
                return $overdueTasks;
                break;
        }
    }

    private function _getWorkflowSteps( $users_id, $whereIn )
    {
        $business_area_pending_task = $this->repositoryWorkflowStepsGenerals->makeModel()
            ->whereIn('status', $whereIn)
            ->whereNotNull('process_steps_business_area_generals_id')
            ->whereHas('business_area.users_group.agents', function ($query) use ($users_id) {
                $query->where('users_id', $users_id);
            })
            ->get();

        $triggering_events_pending_task = $this->repositoryWorkflowStepsGenerals->makeModel()
            ->where('users_id', $users_id)
            ->whereIn('status', $whereIn)
            ->whereNotNull('process_steps_triggering_events_generals_id')
            ->get();

        $this->pending_task = $business_area_pending_task->merge($triggering_events_pending_task);
    }

    private function _isOverdueTask($item) {
        try {
            $now = Carbon::now()->getPreciseTimestamp(0);
            $start_date_time = Carbon::createFromFormat('Y-m-d H:i:s', $item->start_date_time)->getPreciseTimestamp(0);
            $sla_date_time = Carbon::createFromFormat('Y-m-d H:i:s', $item->sla_date_time)->getPreciseTimestamp(0);
            return 100 - ((($sla_date_time - $now) * 100) / ($sla_date_time - $start_date_time));
        } catch (\Exception $e) {
            if ( is_null($item->start_date_time) || is_null($item->sla_date_time) ) {
                return 0;
            }
            return 101;
        }
    }

    private function _getMyRequests( $users_id, $whereIn )
    {
        $this->my_requests_total = $this->repositoryWorkflowGenerals->makeModel()
            ->where('users_id', $users_id)
            ->get();

        $this->my_requests_pending = $this->repositoryWorkflowGenerals->makeModel()
            ->whereIn('status', $whereIn)
            ->where('users_id', $users_id)
            ->get();
    }

    public function time_graph()
    {
        $this->graphicItem = [];

        $this->condition = $this->request->input('type');
        $this->object = $this->request->input('object');

        $this->_getGraphicItem();

        return [
            'graphicItemLabel' => $this->condition,
            'graphicItem' => $this->graphicItem
        ];
    }

    public function volumetry()
    {
        $object = $this->request->input('object_volumetry');

        switch( $object )
        {
            case 'VENDOR':
                return $this->_getVendorByItem();
            break;
            case 'CAT_TEC':
                return $this->_getCatTecByItem();
            break;
            case 'CAT_HIE':
                return $this->_getCatHieByItem();
            break;
        }
    }

    private function _getVendorByItem()
    {
        /*

        Verificar em todas essas tabelas, se tiver em pelo menos uma, ou mais de uma tabela, contar apenas 1:

        - Unidade alternativa
        - Barcode
        - Marca
        - Part number
        */

        $collection = $this->repositoryItemGeneral->makeModel()
            ->with('partnumbers')
            ->with('brands')
            ->with('barcodes')
            ->with('uoms')
            ->whereHas('partnumbers.vendor')
            ->orWhereHas('brands.vendor')
            ->orWhereHas('barcodes.vendor')
            ->orWhereHas('uoms.vendor')
            ->get();

        $object = [];
        if( count($collection) >0  ){
            foreach( $collection as $coll )
            {
                if($coll->partnumbers) {
                    foreach($coll->partnumbers as $value )
                    {
                        $object[$value->vendors_generals_id][] = $coll->id;
                    }
                    continue;
                }

                if( $coll->brands ) {
                    foreach($coll->brands as $value )
                    {
                        $object[$value->vendors_generals_id][] = $coll->id;
                    }
                    continue;
                }

                if( $coll->barcodes ){
                    foreach($coll->barcodes as $value )
                    {
                        $object[$value->vendors_generals_id][] = $coll->id;
                    }
                    continue;
                }

                if( $coll->uoms ) {
                    foreach($coll->uoms as $value )
                    {
                        $object[$value->vendors_generals_id][] = $coll->id;
                    }
                    continue;
                }

            }
        }

        $agroup = [];
        if( count($object) > 0 ) {
            foreach( $object as $vendor_id => $obj)
            {
                $vendor = $this->repositorVendorsGeneral->makeModel()->find($vendor_id);
                $obj = array_unique($obj);
                $agroup[] = [
                    'description' => $vendor->name,
                    'total_items' => count($obj)
                ];
            }
        }

        $agroup = Arrays::orderByArray($agroup, 'total_items', 'desc');

        return $agroup;
    }

    private function _getCatTecByItem()
    {

        $object = $this->_getCategoryGeneneralByItem('TEC');
        $agroup = [];

        if( count($object) > 0 ) {
            foreach( $object as $category_tec_id => $qtd) {
                $category = $this->repositoryCategoryGeneral->makeModel()->find($category_tec_id);
                $agroup[] = [
                    'description' => $category->description,
                    'total_items' => $qtd
                ];
            }
        }

        $agroup = Arrays::orderByArray($agroup, 'total_items', 'desc');

        return $agroup;
    }

    private function _getCategoryGeneneralByItem( $type )
    {
        $object = [];

        $collection = $this->repositoryCategoryGeneral->makeModel()
            ->with('items')
            ->whereHas('items')
            ->where('type', $type )
            ->get();

        if( count($collection) >0  ){
            foreach( $collection as $coll )
            {
                $object[$coll->id] = $coll->items->count();
            }
        }

        return $object;
    }

    private function _getCatHieByItem()
    {

        $agroup = [];

        $object = $this->_getCategoryGeneneralByItem('HIE');

        if( count($object) > 0 ) {
            foreach( $object as $category_id => $qtd) {
                $category = $this->repositoryCategoryGeneral->makeModel()->find($category_id);
                $category_superior = $this->repositoryCategoryGeneral->makeModel()->find($category->superior_category_generals_id);
                $agroup[] = [
                    'description' => $category_superior->description . ' | '. $category->code,
                    'total_items' => $qtd
                ];
            }
        }

        $object_public = [];
        $collection_public = $this->repositoryCategoryPublicGenerals->makeModel()
            ->with('items')
            ->whereHas('items')
            ->where('type','HIE')
            ->get();

        if( count($collection_public) >0  ){
            foreach( $collection_public as $coll )
            {
                $object_public[$coll->id] = $coll->items->count();
            }
        }

        if( count($object_public) > 0 ) {
            foreach( $object_public as $category_hie_id => $qtd) {
                $category = $this->repositoryCategoryPublicGenerals->makeModel()->find($category_hie_id);
                $category_superior = $this->repositoryCategoryPublicGenerals->makeModel()->find($category->superior_category_public_generals_id);
                $agroup[] = [
                    'description' => $category_superior->description . ' | '. $category->code,
                    'total_items' => $qtd
                ];
            }
        }

        $agroup = Arrays::orderByArray($agroup, 'total_items', 'desc');

        return $agroup;

    }
}
