<?php
/**
 * Created by PhpStorm.
 * User: eduardo
 * Date: 27/06/19
 * Time: 14:44
 */

namespace App\Services;

use App\Events\InstancesCreatedOrUpdated;
use App\Repositories\FunctionGeneralRepository;
use App\Repositories\ProfileFunctionsRepository;
use App\Repositories\QueueElasticsearchRepository;
use App\Traits\ExceptionTrait;
use App\Validators\QueueElasticsearchValidator;
use Doctrine\DBAL\Schema\SchemaException;
use Exception;
use App\Exceptions\TenantAlreadyExistsException;
use App\Repositories\CompaniesGeneralRepository;
use App\Repositories\CompaniesLanguagesRepository;
use App\Repositories\CompaniesSegmentsRepository;

use App\Repositories\InstancesGeneralsRepository;
use App\Repositories\InstancesLanguagesRepository;
use App\Repositories\QueueEmailsRepository;
use App\Repositories\UsersRepository;
use App\Repositories\UsersProfilesRepository;
use App\Repositories\ProfileGeneralsRepository;
use App\Validators\CompaniesGeneralValidator;
use App\Validators\CompaniesLanguagesValidator;
use App\Validators\CompaniesSegmentsValidator;

use App\Validators\InstancesGeneralsValidator;
use App\Validators\InstancesLanguagesValidator;
use App\Validators\QueueEmailsValidator;
use App\Validators\UsersValidator;
use App\Validators\UsersProfilesValidator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Artisan;
use Laravel\Passport\TokenRepository;
use Prettus\Validator\Contracts\ValidatorInterface;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\DB;
use function Matrix\trace;

class RegisterService
{
    use ExceptionTrait;
    /**
     * @var CompaniesGeneralRepository
     */
    private $companiesGeneralRepository;
    /**
     * @var CompaniesGeneralValidator
     */
    private $companiesGeneralValidator;
    /**
     * @var CompaniesLanguagesRepository
     */
    private $companiesLanguagesRepository;
    /**
     * @var CompaniesLanguagesValidator
     */
    private $companiesLanguagesValidator;

     /**
     * @var CompaniesSegmentsRepository
     */
    private $companiesSegmentsRepository;
    /**
     * @var CompaniesSegmentsValidator
     */
    private $companiesSegmentsValidator;

    /**
     * @var UsersRepository
     */
    private $usersRepository;
    /**
     * @var UsersProfilesRepository
     */
    private $usersProfilesRepository;
    /**
     * @var UsersValidator
     */
    private $usersValidator;
    /**
     * @var UsersProfilesValidator
     */
    private $usersProfilesValidator;
    /**
     * @var InstancesGeneralsRepository
     */
    private $instancesGeneralsRepository;
    /**
     * @var InstancesGeneralsValidator
     */
    private $instancesGeneralsValidator;
    /**
     * @var QueueEmailsRepository
     */
    private $queueEmailsRepository;
    /**
     * @var QueueEmailsValidator
     */
    private $queueEmailsValidator;
    /**
     * @var Request
     */
    private $request;

     /**
     * @var ProfileGeneralsRepository
     */
    private $profileGeneralsRepository;

    private $user;
    private $data;
    private $response = [];
    private $schemaCreated = false;
    private $languages_generals_id;

    /**
     * @var InstancesLanguagesRepository
     */
    private $instancesLanguagesRepository;
    /**
     * @var InstancesLanguagesValidator
     */
    private $instancesLanguagesValidator;
    /**
     * @var PostgresSchemasService
     */
    private $schemasService;
    /**
     * @var ProfileFunctionsRepository
     */
    private $profileFunctionsRepository;
    /**
     * @var FunctionGeneralRepository
     */
    private $functionGeneralRepository;
    /**
     * @var ElasticsearchMubService
     */
    private $elasticsearchMubService;
    /**
     * @var TokenRepository
     */
    private $tokenRepository;
    /**
     * @var QueueElasticsearchRepository
     */
    private $queueElasticsearchRepository;
    /**
     * @var QueueElasticsearchValidator
     */
    private $queueElasticsearchValidator;

    /**
     * RegisterService constructor.
     * @param Request $request
     * @param CompaniesGeneralRepository $companiesGeneralRepository
     * @param CompaniesGeneralValidator $companiesGeneralValidator
     * @param CompaniesLanguagesRepository $companiesLanguagesRepository
     * @param CompaniesLanguagesValidator $companiesLanguagesValidator
     * @param CompaniesSegmentsRepository $companiesSegmentsRepository
     * @param CompaniesSegmentsValidator $companiesSegmentsValidator
     * @param UsersRepository $usersRepository
     * @param UsersProfilesRepository $usersProfilesRepository
     * @param UsersValidator $usersValidator
     * @param UsersProfilesValidator $usersProfilesValidator
     * @param InstancesGeneralsRepository $instancesGeneralsRepository
     * @param InstancesGeneralsValidator $instancesGeneralsValidator
     * @param InstancesLanguagesRepository $instancesLanguagesRepository
     * @param InstancesLanguagesValidator $instancesLanguagesValidator
     * @param QueueEmailsRepository $queueEmailsRepository
     * @param QueueEmailsValidator $queueEmailsValidator
     * @param ProfileGeneralsRepository $profileGeneralsRepository
     * @param PostgresSchemasService $schemasService
     * @param ProfileFunctionsRepository $profileFunctionsRepository
     * @param FunctionGeneralRepository $functionGeneralRepository
     * @param ElasticsearchMubService $elasticsearchMubService
     * @param TokenRepository $tokenRepository
     * @param QueueElasticsearchRepository $queueElasticsearchRepository
     * @param QueueElasticsearchValidator $queueElasticsearchValidator
     */
    public function __construct(Request $request,
                                CompaniesGeneralRepository $companiesGeneralRepository,
                                CompaniesGeneralValidator $companiesGeneralValidator,
                                CompaniesLanguagesRepository $companiesLanguagesRepository,
                                CompaniesLanguagesValidator $companiesLanguagesValidator,
                                CompaniesSegmentsRepository $companiesSegmentsRepository,
                                CompaniesSegmentsValidator $companiesSegmentsValidator,
                                UsersRepository $usersRepository,
                                UsersProfilesRepository $usersProfilesRepository,
                                UsersValidator $usersValidator,
                                UsersProfilesValidator $usersProfilesValidator,
                                InstancesGeneralsRepository $instancesGeneralsRepository,
                                InstancesGeneralsValidator $instancesGeneralsValidator,
                                InstancesLanguagesRepository $instancesLanguagesRepository,
                                InstancesLanguagesValidator $instancesLanguagesValidator,
                                QueueEmailsRepository $queueEmailsRepository,
                                QueueEmailsValidator $queueEmailsValidator,
                                ProfileGeneralsRepository $profileGeneralsRepository,
                                PostgresSchemasService $schemasService,
                                ProfileFunctionsRepository $profileFunctionsRepository,
                                FunctionGeneralRepository $functionGeneralRepository,
                                ElasticsearchMubService $elasticsearchMubService,
                                TokenRepository $tokenRepository,
                                QueueElasticsearchRepository $queueElasticsearchRepository,
                                QueueElasticsearchValidator $queueElasticsearchValidator)
    {
        $this->companiesGeneralRepository = $companiesGeneralRepository;
        $this->companiesSegmentsValidator = $companiesSegmentsValidator;
        $this->companiesSegmentsRepository = $companiesSegmentsRepository;
        $this->companiesGeneralValidator = $companiesGeneralValidator;
        $this->companiesLanguagesRepository = $companiesLanguagesRepository;
        $this->companiesLanguagesValidator = $companiesLanguagesValidator;
        $this->usersRepository = $usersRepository;
        $this->usersValidator = $usersValidator;
        $this->usersProfilesValidator = $usersProfilesValidator;
        $this->usersProfilesRepository = $usersProfilesRepository;
        $this->instancesGeneralsRepository = $instancesGeneralsRepository;
        $this->instancesGeneralsValidator = $instancesGeneralsValidator;
        $this->queueEmailsRepository = $queueEmailsRepository;
        $this->queueEmailsValidator = $queueEmailsValidator;
        $this->request = $request;
        $this->instancesLanguagesRepository = $instancesLanguagesRepository;
        $this->instancesLanguagesValidator = $instancesLanguagesValidator;
        $this->schemasService = $schemasService;
        $this->profileGeneralsRepository = $profileGeneralsRepository;
        $this->profileFunctionsRepository = $profileFunctionsRepository;
        $this->functionGeneralRepository = $functionGeneralRepository;
        $this->elasticsearchMubService = $elasticsearchMubService;
        $this->tokenRepository = $tokenRepository;
        $this->queueElasticsearchRepository = $queueElasticsearchRepository;
        $this->queueElasticsearchValidator = $queueElasticsearchValidator;
    }

    /**
     * @return array
     * @throws TenantAlreadyExistsException
     * @throws ValidationException
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    public function register()
    {
        try {

            $this->_validate()->_configure();
            $this->_registerTenant()->_runMigrations();

            $this->beginTransaction();

                $this->_createCompany();
                $this->_createCompanyLanguages();
                $this->_createCompanySegments();
                $this->_updateFirstUser();
                $this->_createFirstInstance();
                $this->_runProfileGenerals();
                $this->_scheduleCompanyCreatedEmail();
                $this->_createElasticsearchIndex();
                $this->_generateResponseToken();

            $this->commit();

            return $this->response;

        } catch ( Exception $exception ) {

            $this->rollback();

            $this->_manualRollback();
            $this->setStatusCode($exception);
            $this->setMessage($exception);
            $this->checkSpecialCases($exception);

            return [
                "success" => $this->bSuccess,
                "message" => $this->message
            ];
        }
    }

    /**
     * Create a new instance
     */
    public function create()
    {
        try {

            $this->_configure(false);
            $this->_registerTenant()->_runMigrations();

            $this->beginTransaction();

                $this->_createFirstInstance();
                $this->_runProfileGenerals();
                $this->_createElasticsearchIndex();

            $this->commit();

            return $this->response;

        } catch ( Exception $exception ) {

            $this->rollback();

            $this->_manualRollback();
            $this->setStatusCode($exception);
            $this->setMessage($exception);
            $this->checkSpecialCases($exception);

            throw new \Exception($this->getMessage());
        }
    }

    private function _manualRollback()
    {
        if ( $this->schemaCreated === false ) {
            return;
        }
        $this->schemasService->drop($this->data['uuid']);
    }
    /**
     * @return $this
     * @throws ValidationException
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    private function _validate()
    {
        $this->usersValidator->with($this->request->all())->passesOrFail('register');
        $this->user = $this->usersRepository->skipPresenter()->findByField('remember_token', $this->request->input('_token'))->first();
        if ( is_null($this->user) ) {
            throw new \Dotenv\Exception\ValidationException('Invalid Token');
        }
        return $this;
    }

    /**
     * @param bool $isFirstInstance
     */
    private function _configure($isFirstInstance = true)
    {
        $sUuid = $this->schemasService->generateUUID('uuid');

        $language = $this->request->input('appLanguage');

        $this->languages_generals_id = $this->request->input('languages_generals_id');

        if ( $isFirstInstance ) {

            $this->data = array_merge($this->request->all(), [
                'uuid' => $sUuid,
                'fqdn' => $this->request->input('sub_domain'),
                'active' => 1,
                'status' => 'A',
                'code' => $this->_setCodeFirstInstance(),
                'description' => $this->_setDescriptionFirstInstance(),
                'languages' => [
                    [
                        'languages_generals_id' => $this->request->input('languages_generals_id'),
                        'description' => $this->_setDescriptionFirstInstance()
                    ]
                ],
                'instances_generals_id_master' => TRUE,
                'companies_generals_id' => null,
                'instances_generals_id' => null,
                'profiles_generals_id' => 1,
                'users_id' => $this->user->id,
                'isMainUser' => TRUE,
                'email_verified_at' => Carbon::now(),
                'remember_token' => null,
                'to_email' => $this->user->email,
                'to_name' => $this->request->input('full_name'),
                'from_email' => env('MAILGUN_FROM_ADDRESS'),
                'from_name' => env('MAILGUN_FROM_NAME'),
                'subject' => trans( 'email.T5', [], $language ),
                'message' => 'RegisterCompleted',
                'meta' => [
                    'language' => $language
                ],
                'attachments' => [
                    public_path('files/[mub] Termos de Uso e Serviços.pdf')
                ],
                'auto_translation_base_languages_generals_id' => $this->languages_generals_id
            ]);

            if (!isset($this->data['password']) || empty($this->data['password']) || is_null($this->data['password'])) {
                unset($this->data['password']);
            }
        }

        if ( $isFirstInstance === false ) {
            $this->data = array_merge($this->request->all(), [
                'uuid' => $sUuid,
                'instances_generals_id' => null,
                'isMainUser' => TRUE,
            ]);
        }

        $this->response = [
            'company' => null,
            'instance' => null,
            'user' => null,
            'access_token' => null,
            'queue' => false
        ];
    }

    private function _setCodeFirstInstance()
    {
        return preg_replace('/[.,;-]/i', '_', $this->request->input('sub_domain'));
    }

    private function _setDescriptionFirstInstance()
    {
        return "Primeira instância de {$this->request->input('sub_domain')}";
    }

    /**
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    private function _createCompany()
    {
        $this->companiesGeneralValidator->with($this->data)->passesOrFail(ValidatorInterface::RULE_CREATE);
        $company = $this->companiesGeneralRepository->skipPresenter()->create($this->data);
        $this->response['company'] = $company;
        $this->data['companies_generals_id'] = $company->id;
    }

    /**
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    private function _createCompanyLanguages()
    {
        $this->companiesLanguagesValidator->with($this->data)->passesOrFail(ValidatorInterface::RULE_CREATE);
        $this->companiesLanguagesRepository->skipPresenter()->create($this->data);
    }

    private function _createCompanySegments()
    {
        if( isset($this->data['segments_generals_id']) && $this->data['segments_generals_id'] > 0){
            foreach($this->data['segments_generals_id'] as $segments_generals_id){
                $object = [
                    'companies_generals_id' => $this->data['companies_generals_id'],
                    'segments_generals_id' => $segments_generals_id,
                    'is_secondary' => false,
                    'active' => 1
                ];
                $this->companiesSegmentsValidator->with($object)->passesOrFail(ValidatorInterface::RULE_CREATE);
                $this->companiesSegmentsRepository->skipPresenter()->create($object);
            }
        }
    }

    /**
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    private function _updateFirstUser()
    {
        $this->usersValidator->with($this->data)->passesOrFail(ValidatorInterface::RULE_UPDATE);
        $this->user = $this->usersRepository->skipPresenter()->update($this->data, $this->user->id);
        $this->response['user'] = $this->user;

        /** Set languages_generals_id to array because Instance General Events expects an Array */
        $this->data['languages_generals_id'] = [$this->data['languages_generals_id']];
    }

    /**
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    private function _setFirstUserAsAdmin()
    {
        $this->usersProfilesValidator->with($this->data)->passesOrFail(ValidatorInterface::RULE_CREATE);
        $this->usersProfilesRepository->skipPresenter()->create($this->data);
    }

    /**
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    private function _scheduleCompanyCreatedEmail()
    {
        $this->data['active'] = 2;
        $this->data['meta'] = Arr::add($this->data['meta'], 'full_name', $this->data['full_name']);
        $this->queueEmailsValidator->with($this->data)->passesOrFail(ValidatorInterface::RULE_CREATE);
        $this->queueEmailsRepository->skipPresenter()->create($this->data);
    }

    private function _createElasticsearchIndex()
    {
        $data = [
            "object_instance" => $this->data['uuid'],
            "object_type" => "INSTANCE",
            "object_id" => $this->data['instances_generals_id'],
            "send_at" => Carbon::now()->format('Y-m-d H:i:s')
        ];

        $this->queueElasticsearchValidator->with($data)->passesOrFail(ValidatorInterface::RULE_CREATE);
        $this->queueElasticsearchRepository->skipPresenter()->create($data);
        $this->response['queue'] = true;
    }

    private function _generateResponseToken()
    {
        try {
            $token = $this->user->createToken(null);
            $this->response['access_token'] = [
                "access_token" => $token->accessToken,
                "expires_at" => $token->token->expires_at->timestamp,
                "expires_in" => $token->token->expires_at->timestamp,
                "refresh_token" => null,
                "token_type" => "Bearer"
            ];
        } catch (\Exception $e ) {
            $this->response['access_token'] = null;
        }
    }

    /**
     * @return void
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    private function _createFirstInstance()
    {
        $this->instancesGeneralsValidator->with($this->data)->passesOrFail(ValidatorInterface::RULE_CREATE);
        $instance = $this->instancesGeneralsRepository->skipPresenter()->create($this->data);

        event( new InstancesCreatedOrUpdated( $this->data,$instance->id) );

        $this->response['instance'] = $instance;
        $this->data['instances_generals_id'] = $instance->id;
    }

    /**
     * @throws \Prettus\Validator\Exceptions\ValidatorException
     */
    private function _createFirstInstanceLanguages()
    {
        $this->instancesLanguagesValidator->with($this->data)->passesOrFail(ValidatorInterface::RULE_CREATE);
        $this->instancesLanguagesRepository->skipPresenter()->create($this->data);
    }

    private function _tenantExists()
    {
        return $this->schemasService->schemaExists("'{$this->data['uuid']}'");
    }

    /**
     * @return $this
     * @throws TenantAlreadyExistsException
     */
    private function _registerTenant()
    {
        if ( $this->_tenantExists() ) {
            throw new TenantAlreadyExistsException("Tenant {$this->data['uuid']} already exists on Database. Please check");
        }

        $this->schemasService->create($this->data['uuid']);
        return $this;
    }

    /**
     * @return $this
     */
    private function _runMigrations()
    {
        $this->schemaCreated = true;
        $this->schemasService->switchTo([$this->data['uuid'], 'public']);
        /** Run Migrations */
        $this->schemasService->migrate($this->data['uuid'], ['--force' => true,'--path' => 'database/migrations/tenant']);
        /** Run Seeders */
        Artisan::call('db:seed', ['--class' => 'TenantSeeder']);
        $this->schemasService->switchTo('public');
        return $this;
    }

    public function _runProfileGenerals()
    {
        $profiles = [
            [ "code" => "ADMIN_MASTER", "description" => 'Master admin' , "status" => 'A',  "system_standard" => true, "instances_generals_id" => $this->data['instances_generals_id'], "created_at" => new \Datetime, "updated_at" => new \Datetime,
                "descriptions" => [
                    ["languages_generals_id" => 16, "description" => "Master admin", "created_at" => new \Datetime, "updated_at" => new \Datetime],
                    ["languages_generals_id" => 57, "description" => "Administrador master", "created_at" => new \Datetime, "updated_at" => new \Datetime],
                    ["languages_generals_id" => 18, "description" => "Administrador maestro", "created_at" => new \Datetime, "updated_at" => new \Datetime],
                ]
            ],
            [ "code" => "ADMIN_TECH", "description" => 'Technical admin', "status" => 'A',  "system_standard" => true, "instances_generals_id" => $this->data['instances_generals_id'], "created_at" => new \Datetime, "updated_at" => new \Datetime,
                "descriptions" => [
                    ["languages_generals_id" => 16, "description" => "Technical admin", "created_at" => new \Datetime, "updated_at" => new \Datetime],
                    ["languages_generals_id" => 57, "description" => "Administrador técnico", "created_at" => new \Datetime, "updated_at" => new \Datetime],
                    ["languages_generals_id" => 18, "description" => "Administrador técnico", "created_at" => new \Datetime, "updated_at" => new \Datetime],
                ]
            ],
            [ "code" => "AGENT", "description" => 'Agent', "status" => 'A',  "system_standard" => true, "instances_generals_id" => $this->data['instances_generals_id'], "created_at" => new \Datetime, "updated_at" => new \Datetime,
                "descriptions" => [
                    ["languages_generals_id" => 16, "description" => "Agent", "created_at" => new \Datetime, "updated_at" => new \Datetime],
                    ["languages_generals_id" => 57, "description" => "Agente", "created_at" => new \Datetime, "updated_at" => new \Datetime],
                    ["languages_generals_id" => 18, "description" => "Agente", "created_at" => new \Datetime, "updated_at" => new \Datetime],
                ]
            ],
            [ "code" => "DATA_CLEANER", "description" => 'Data cleaner', "status" => 'A',  "system_standard" => true, "instances_generals_id" => $this->data['instances_generals_id'], "created_at" => new \Datetime, "updated_at" => new \Datetime,
                "descriptions" => [
                    ["languages_generals_id" => 16, "description" => "Data cleaner", "created_at" => new \Datetime, "updated_at" => new \Datetime],
                    ["languages_generals_id" => 57, "description" => "Saneador", "created_at" => new \Datetime, "updated_at" => new \Datetime],
                    ["languages_generals_id" => 18, "description" => "Saneador", "created_at" => new \Datetime, "updated_at" => new \Datetime],
                ]
            ],
        ];

        foreach( $profiles as $profile )
        {
            $profile_generals = $this->profileGeneralsRepository->skipPresenter()->create( $profile );
            $this->_runProfileDescriptions( $profile_generals->id , $profile['descriptions']);
            $this->data['profiles_generals_id'] = $profile_generals->id;

            switch( $profile['code'] )
            {
                case 'ADMIN_MASTER':
                    $this->_setFirstUserAsAdmin();
                    $this->_runProfileFunctionsAM( $profile_generals->id );
                break;
                case 'ADMIN_TECH':
                    $this->_runProfileFunctionsAT( $profile_generals->id );
                break;
                case 'AGENT':
                    $this->_runProfileFunctionsAG( $profile_generals->id );
                break;
                case 'DATA_CLEANER':
                    $this->_runProfileFunctionsDC( $profile_generals->id );
                break;
            }
        }
    }

    public function _runProfileDescriptions( $profiles_generals_id , $descriptions )
    {
        foreach( $descriptions as $description ) {
            $description['profile_generals_id'] = $profiles_generals_id;
            DB::table('profile_descriptions')->insert($description);
        }
    }

    public function _runProfileFunctionsAT( $profiles_generals_id )
    {
        DB::table('profile_functions')->insert([
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "1", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "2", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "3", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "4", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "5", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "6", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "7", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "8", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "15", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "16", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "17", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "18", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "19", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "20", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "21", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "22", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "23", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "24", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "25", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "26", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "27", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "28", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "29", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "30", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "32", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "33", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "34", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "35", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "36", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "37", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "38", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "39", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "40", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "41", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "42", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "43", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "44", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "45", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "52", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "53", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "54", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "55", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "56", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "57", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "58", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "59", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "60", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "62", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "63", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "64", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "65", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "66", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "67", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "68", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "69", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "76", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "77", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "78", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "79", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "80", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "81", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "82", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "83", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "84", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "85", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "86", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "87", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "88", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "89", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "90", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "91", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "92", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "93", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "94", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "95", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "96", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "97", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "98", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "99", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "100", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "101", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "102", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "103", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "104", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "111", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "112", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "113", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "114", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "115", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "116", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "117", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "118", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "119", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "120", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "121", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "122", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "123", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "124", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "125", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "126", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "127", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "128", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "129", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "130", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "131", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "132", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "133", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "134", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "135", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "136", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "137", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "138", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "139", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "140", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "141", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "142", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "143", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "144", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "145", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "146", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "147", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "148", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "149", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "150", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "151", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "152", "created_at" => new \Datetime, "updated_at" => new \Datetime],
        ]);
    }

    public function _runProfileFunctionsAG( $profiles_generals_id )
    {
        DB::table('profile_functions')->insert([
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "5", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "8", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "19", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "20", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "27", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "30", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "36", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "37", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "42", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "45", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "56", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "57", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "60", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "80", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "83", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "88", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "91", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "93", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "94", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "99", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "100", "created_at" => new \Datetime, "updated_at" => new \Datetime],
        ]);
    }

    public function _runProfileFunctionsDC( $profiles_generals_id )
    {
        DB::table('profile_functions')->insert([
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "1", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "2", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "3", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "4", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "5", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "6", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "7", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "8", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "15", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "16", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "17", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "18", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "19", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "20", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "21", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "22", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "23", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "24", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "25", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "26", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "27", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "28", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "29", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "30", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "32", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "33", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "34", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "35", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "36", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "37", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "38", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "39", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "40", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "41", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "42", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "43", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "44", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "45", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "52", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "53", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "54", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "55", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "56", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "57", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "58", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "59", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "60", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "62", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "63", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "64", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "65", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "66", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "67", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "68", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "69", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "76", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "77", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "78", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "79", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "80", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "81", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "82", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "83", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "84", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "85", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "86", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "87", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "88", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "89", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "90", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "91", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "93", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "94", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "95", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "96", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "97", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "98", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "99", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "100", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "101", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "102", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "103", "created_at" => new \Datetime, "updated_at" => new \Datetime],
            ["profile_generals_id" => $profiles_generals_id , "function_generals_id" => "104", "created_at" => new \Datetime, "updated_at" => new \Datetime],
        ]);
    }

    /**
     * Get all functions and enabled to Admin Master Profile
     * @updatedBy Eduardo Sirangelo
     * @param $profiles_generals_id
     */
    public function _runProfileFunctionsAM( $profiles_generals_id )
    {
        $collection = $this->functionGeneralRepository->skipPresenter()->findWhere(['status' => 'A'], ['id']);
        foreach ( $collection as $item ) {
            $this->profileFunctionsRepository->skipPresenter()->create([
                "profile_generals_id" => $profiles_generals_id,
                "function_generals_id" => $item->id
            ]);
        }
    }
}
