<?php

namespace App\Services;

use App\Repositories\FixedValuesDomainRepository;
use Illuminate\Contracts\Filesystem\Factory as Storage;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class FilesDownloadService
{
    /**
     * @var Request
     */
    private $request;
    /**
     * @var FixedValuesDomainRepository
     */
    private $fixedValuesDomainRepository;
    /**
     * @var Storage
     */
    private $storage;

    private $disk = 'mub';
    private $route;
    private $file;
    private $path;
    private $response;
    private $defaultExtension = 'png';

    /**
     * FilesDownloadService constructor.
     * @param Request $request
     * @param FixedValuesDomainRepository $fixedValuesDomainRepository
     * @param Storage $storage
     * @param Image $image
     */
    public function __construct(
        Request $request,
        FixedValuesDomainRepository $fixedValuesDomainRepository,
        Storage $storage
    ){
        $this->request = $request;
        $this->fixedValuesDomainRepository = $fixedValuesDomainRepository;
        $this->storage = $storage;
        $this->defaultExtension = env('MUB_DEFAULT_IMG_EXT', 'png');
    }

    public function getFileUrl()
    {
        $this->_configure();
        if ( ! $this->_fileExists() ) {
            return [false];
        }
        return $this->_makeImage();
    }

    public function download()
    {
        $path = $this->request->get('path');
        $exists = $this->storage->disk($this->disk)->exists($path);
        if ( ! $exists ) {
            return [false];
        }
        return $this->storage->disk($this->disk)->download( $path );
    }

    private function _configure()
    {
        if ( $this->request->route('uuid') && $this->request->route('file') ) {

            $this->route = $this->request->route('uuid');
            $this->file = $this->request->route('file');
            $this->path = "{$this->route}/{$this->file}";

            if($this->request->route('folder')) {
                $this->path = "{$this->route}/{$this->request->route('folder')}/{$this->file}";
            }
        }
    }

    private function _fileExists()
    {
        $exists = $this->storage->disk($this->disk)->exists($this->path);
        if ( $exists ) {
            $this->file = $this->storage->disk($this->disk)->get($this->path);
        }
        return $exists;
    }

    private function _makeImage()
    {
        $img = Image::make($this->file);
        return $img->response($this->defaultExtension);
    }

    public function destroy()
    {

    }
}
