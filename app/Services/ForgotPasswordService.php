<?php
/**
 * Created by FelipeCastro
 * User: felipe
 * Date: 16/07/19
 * Time: 14:54
 */

namespace App\Services;

use App\Events\CompanyCreated;
use App\Exceptions\ExceptionGeneral;
use App\Repositories\QueueEmailsRepository;
use App\Repositories\UsersRepository;
use App\Traits\MyDatabaseTransactions;
use App\Validators\UsersValidator;
use Bogardo\Mailgun\Facades\Mailgun;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use Prettus\Validator\Contracts\ValidatorInterface;

class ForgotPasswordService
{
	use MyDatabaseTransactions;

	private $request;
	private $queueEmailsRepository;
	private $repository;
	private $validator;
	private $data = [];
	private $user;

	public function __construct(
		Request $request,
        UsersRepository $repository,
        UsersValidator $validator,
        QueueEmailsRepository $queueEmailsRepository
    )
    {
        $this->request = $request;
        $this->repository = $repository;
        $this->validator = $validator;
        $this->queueEmailsRepository = $queueEmailsRepository;
    }

    public function forgotPassword()
    {

        $this->beginTransaction();

        $this->_validate();
        $this->_setRememberToken();
        $this->_scheduleMail();

        $this->commit();

        return [true];
    }

    private function _validate()
    {
        $this->data = $this->request->all();
        $this->data = Arr::add($this->data, 'remember_token', $this->_generateRememberToken());

        $this->validator->with($this->data)->passesOrFail('recoverPassword');
        $collection = $this->repository->skipPresenter()->findByField('email', $this->request->input('email'));

        if ( $collection->isEmpty() ) {
            $this->_invalidEmail();
        }

        unset($this->data['email']);

        $this->user = $collection->first();
    }

    private function _setRememberToken()
    {
        $this->user = $this->repository->skipPresenter()->update($this->data, $this->user->id);
    }

    private function _scheduleMail()
    {
        $language = $this->request->input('appLanguage');

        $this->queueEmailsRepository->skipPresenter()->create([
            'to_email' => $this->user->email,
            'to_name' => $this->user->email,
            'from_email' => env('MAILGUN_FROM_ADDRESS'),
            'from_name' => env('MAILGUN_FROM_NAME'),
            'subject' => trans( 'email.T12', [], $language ),
            'message' => 'ForgotPasswordRegister',
            'active' => 2,
            'meta' => [
                "token" => $this->data['remember_token'],
                "language" => $language,
                "full_name" => $this->user->full_name
            ]
        ]);
    }

    private function _generateRememberToken()
    {
        return Str::random(100);
    }

    private function _invalidEmail()
    {
        $messageBag = new MessageBag([
            0 => [
                'useMessage' => true,
                'message' => 'MSG.T46'
            ]
        ]);
        throw new ExceptionGeneral($messageBag);
    }
}
