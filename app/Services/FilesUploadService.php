<?php

namespace App\Services;

use App\Helpers\Files;
use App\Repositories\FixedValuesDomainRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Contracts\Filesystem\Factory as Storage;
use Illuminate\Support\Facades\Validator;
use League\Flysystem\FileExistsException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

/**
 * WARNING - REMEMBER TO CONFIGURE THE PHP.INI FILE TO ACCEPT LONG POST REQUESTS
 * $ php -i | grep php.ini - Find php.ini
 * $ sudo nano /etc/php/<PHPVERSION>>/fpm/php.ini
 * post_max_size = 128M
 * upload_max_filesize = 128M
 * memory_limit = 256M
 * Change on both files
 * $ sudo systemctl restart php7.2-fpm
 *
 * Class FilesUploadService
 * @package App\Services
 */
class FilesUploadService
{
    /**
     * @var Request
     */
    private $request;
    private $response;
    private $fileInfo;
    private $path;
    private $key = 'file';
    private $disk = 'mub';
    private $dir = 'tmp';

    private $fileData = [];
    private $moveEntity;
    private $file;
    private $instanceDir;

    private $isMove = true;

    /**
     * @var FixedValuesDomainRepository
     */
    private $fixedValuesDomainRepository;
    /**
     * @var Storage
     */
    private $storage;

    /**
     * FilesUploadService constructor.
     * @param Request $request
     * @param FixedValuesDomainRepository $fixedValuesDomainRepository
     * @param Storage $storage
     */
    public function __construct(Request $request, FixedValuesDomainRepository $fixedValuesDomainRepository, Storage $storage)
    {
        $this->request = $request;
        $this->fixedValuesDomainRepository = $fixedValuesDomainRepository;
        $this->storage = $storage;
    }

    public function upload()
    {
        $this->_configureRequest();
        $this->_validateFile();
        $this->_getFileInfo();
        $this->_getFixedValueFileType();
        $this->_saveTmpFile();
        $this->_setResponse();

        return $this->response;
    }

    public function destroy($file = null)
    {
        if ( is_null($file) ) {
            return false;
        }

        $this->file = $file;

        if ( ! $this->_fileExists() ) {
            return false;
        }

        return $this->storage->disk($this->disk)->delete($this->file);

    }

    public function moveFiles()
    {
        $this->file = $this->fileData['url'];

        if ( ! $this->_fileExists() ) {
            return false;
        }

        /** Checks if it belongs to a workflow object and set isMove with
         * false and keeps a copy of the file in the workflow directory of the instance */
        if ( strpos($this->file, '/workflow/') ) {
            $this->isMove = false;
        }

        $this->_firstOrCreateInstanceDir();
        $this->_moveFile();
        $this->_setResponse();

        return $this->response;
    }

    /**
     * @param $data
     * @param bool $existsCondition
     * @throws FileExistsException
     */
    public function checkFileExists($data, $existsCondition = true)
    {
        list($path, $basename) = explode('/', $data['url']);
        $pathFile = request()->route('uuid') . '/' . $basename;
        $exists = $this->storage->disk($this->disk)->exists($pathFile);
        if ( $exists === $existsCondition ) {
            throw new FileExistsException($pathFile);
        }
    }

    /**
     * @return array
     */
    public function getFileData(): array
    {
        return $this->fileData;
    }

    /**
     * @param array $fileData
     * @return FilesUploadService
     */
    public function setFileData(array $fileData)
    {
        $this->fileData = $fileData;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMoveEntity()
    {
        return $this->moveEntity;
    }

    /**
     * @param mixed $moveEntity
     * @return FilesUploadService
     */
    public function setMoveEntity($moveEntity)
    {
        $this->moveEntity = $moveEntity;
        return $this;
    }

    private function _configureRequest()
    {
        if ( $this->request->has('key') ) {
            $this->key = $this->request->input('key');
        }

        if ( $this->request->has('disk') ) {
            $this->disk = $this->request->input('disk');
        }

        if ( $this->request->has('dir') ) {
            $this->dir = $this->request->input('dir');
        }
    }

    private function _validateFile()
    {
       /*  if ( $this->request->hasFile($this->key) === false || $this->request->file($this->key)->isValid() === false ) {
            throw new FileException();
        } */
        // TODO - Implement here any extra validation
    }

    private function _getFileInfo()
    {
        $oFileInfo = $this->request->file($this->key);
        $this->fileInfo = [
            "description" => $oFileInfo->getClientOriginalName(),
            "file_name" => $oFileInfo->getClientOriginalName(),
            "file_size" => Files::formatBytes($oFileInfo->getSize()),
            "file_mime_type" => $oFileInfo->getClientMimeType(),
            "file_extension" => $this->request->file($this->key)->getClientOriginalExtension(),
            "file_type" => Files::mime2ext($oFileInfo->getClientMimeType()),
            "attachment_type" => Files::fileType($oFileInfo->getClientMimeType(), $this->_isImage()),
            "isImage" => $this->_isImage()
        ];
    }

    private function _getFixedValueFileType()
    {
        $oFixedValueDomain = $this->fixedValuesDomainRepository->findWhere([['object','=','AttachmentType'],['key', '=', $this->fileInfo['attachment_type']]]);
        if ( $oFixedValueDomain->isEmpty() === true ) {
            return;
        }

        $this->fileInfo['attachment_type'] = [
            'attachment_type' => $oFixedValueDomain[0]->key,
            'attachment_description' => $oFixedValueDomain[0]->description
        ];
    }

    private function _getTmpFileName()
    {
        return uniqid() . "." . $this->fileInfo['file_extension'];
    }

    private function _saveTmpFile()
    {
        if ( $this->request->has('fileUploadDir') ) {
            $this->dir = $this->request->input('fileUploadDir');
        }
        $this->path = $this->request->file($this->key)->storeAs($this->dir, $this->_getTmpFileName(), $this->disk);
        $this->fileInfo = Arr::add($this->fileInfo, 'url', $this->path);
    }

    private function _isImage()
    {
        $rules = array(
            'file' => 'image'
        );
        $validator = Validator::make($this->request->all(), $rules);
        return $validator->fails() === false;
    }

    private function _fileExists()
    {
        $exists = $this->storage->disk($this->disk)->exists($this->file);
        return $exists;
    }

    private function _firstOrCreateInstanceDir()
    {
        $uuid = request()->route('uuid');
        if ( ! $this->storage->disk($this->disk)->exists($uuid) ) {
            $this->storage->disk($this->disk)->makeDirectory($uuid);
        }
        $this->instanceDir = $uuid;
    }

    private function _moveFile()
    {
        $newPath = $this->_setPathNotTmp();
        if ( ! $this->storage->disk($this->disk)->exists($newPath) ) {
            if ( $this->isMove ) {
                $this->storage->disk($this->disk)->move($this->file, $newPath);
            } else {
                $this->storage->disk($this->disk)->copy($this->file, $newPath);
            }
        }
        $this->fileInfo = [
            "url" => $newPath
        ];
    }

    private function _setPathNotTmp()
    {
        if ( strpos($this->file, 'tmp/') !== false ) {

            if( strpos($this->file, '.zip') || strpos($this->file, '.rar')  ){

                $folder = $this->file;
                $folder = str_replace('tmp/', '', $folder);
                $folder = str_replace('.zip', '', $folder);
                $folder = str_replace('.rar', '', $folder);

                return $this->instanceDir . "/" . $folder . "/" . str_replace('tmp/', '', $this->file);
            }

            return $this->instanceDir . "/" . str_replace('tmp/', '', $this->file);
        }

        return $this->file;
    }

    private function _setResponse() {

        $this->response = $this->fileInfo;
    }
}
