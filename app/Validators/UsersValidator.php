<?php

namespace App\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

/**
 * Class UsersValidator.
 *
 * @package namespace App\Validators;
 */
class UsersValidator extends LaravelValidator
{
    /**
     * Validation Rules
     *
     * @var array
     */
    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'full_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:320|unique:users,email',
            'active' => 'required|integer'
        ],
        ValidatorInterface::RULE_UPDATE => [],
        'trial' => [
            'email' => 'required|string|email|max:320|unique:users,email',
        ],
        'register' => [
            '_token' => 'required|max:255',
            'full_name' => 'required|max:255',
            'name' => 'required|max:255',
            'sub_domain' => 'required|max:255|unique:companies_generals,fqdn',
            'languages_generals_id' => 'required'
        ],
        'activeByTokenWithPassword' => [
            'password' => 'required|min:6',
            'accepted_at' => 'required'
        ],
        'recoverPassword' => [
            'email' => 'required|string|email|max:320',
        ]
    ];
}
