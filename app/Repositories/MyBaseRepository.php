<?php

namespace App\Repositories;

use Illuminate\Support\Arr;
use Prettus\Repository\Eloquent\BaseRepository;
use Illuminate\Container\Container as Application;

abstract class MyBaseRepository extends BaseRepository
{

    protected $isDuplicated = false;

    protected $response = [];

    public function __construct(Application $app)
    {
        parent::__construct($app);

        $this->isDuplicated = request()->has('duplicate') && request()->input('duplicate') === true;
    }

    public function createWithDuplicate($input, $rewrite = true)
    {
        $where = Arr::except($input, ['id', 'description', '_isEditMode', 'appLanguage']);
        if ( $rewrite ) {
            $this->deleteWhere($where);
        }

        $this->response = $this->create($input);

        return $this->response;
    }

}