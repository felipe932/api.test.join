<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Categories.
 *
 * @package namespace App\Entities;
 */
class Categories extends Model implements Transformable
{
    use TransformableTrait;
    
    protected $schema = 'public';

    protected $table = 'categories';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'description',
        'active',
        'users_id',
    ];

    protected $casts = [
        'description' => 'string',
        'active' => 'integer',
        'users_id' => 'integer',
    ];

    protected $hidden = [];

    public static $rules = [
       
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

        $this->table = "{$this->schema}.{$this->table}";
    }

}
