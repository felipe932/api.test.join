<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Products.
 *
 * @package namespace App\Entities;
 */
class Products extends Model implements Transformable
{
    use TransformableTrait;

    protected $schema = 'public';

    protected $table = 'products';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'description',
        'value',
        'categories_id',
        'active',
        'users_id',
    ];

    protected $casts = [
        'description' => 'string',
        'value' => 'string',
        'categories_id' => 'integer',
        'active' => 'integer',
        'users_id' => 'integer',
    ];

    protected $hidden = [];

    public static $rules = [
       
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

        $this->table = "{$this->schema}.{$this->table}";
    }

    public function category()
    {
        return $this->belongsTo(Categories::class, 'categories_id');
    }
}
