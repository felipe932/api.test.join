<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use App\Traits\SetLanguageTrait;
use App\Traits\SetSchemasTrait;

/**
 * Class Users.
 *
 * @package namespace App\Entities;
 */
class Users extends Authenticatable implements Transformable
{
    use TransformableTrait, HasApiTokens, Notifiable, SetLanguageTrait, SetSchemasTrait, SoftDeletes;

    protected $schema = 'public';

    protected $table = 'users';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'full_name',
        'email',
        'contacts',
        'address',
        'meta',
        'password',
        'cities_id',
        'active',
        'remember_token',
        'logo_url',
        'token_access'
    ];

    protected $casts = [
        'full_name' => 'string',
        'email' => 'string',
        'contacts' => 'json',
        'address' => 'json',
        'meta' => 'json',
        'password' => 'string',
        'cities_id' => 'integer',
        'active' => 'integer',
        'logo_url' => 'string',
        'token_access' => 'string'
    ];

    protected $hidden = [
        'password',
        'remember_token'
    ];

    public static $rules = [
        'full_name' => 'required',
        'email' => 'required',
        'cities_id' => 'required',
        'active' => 'required',
    ];

    /**
     * Users constructor.
     */
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

        $this->table = "{$this->schema}.{$this->table}";
    }

     /**
     * @param $value
     */
    public function setPasswordAttribute($value)
    {
        if( \Illuminate\Support\Facades\Hash::needsRehash($value) ) {
            $value = \Illuminate\Support\Facades\Hash::make($value);
        }
        $this->attributes['password'] = $value;
    }

    public function city()
    {
        return $this->belongsTo(Cities::class, 'cities_id');
    }
}
