<?php

namespace App\Listeners\Users;

use App\Events\UsersCreatedOrUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Repositories\UsersRepository;
use App\Services\ImageService;
use App\Helpers\Arrays;

class StoreUserImages
{
    /**
     * @var UsersRepository
     */
    private $repository;
    /**
     * @var ImageService
     */
    private $service;

    private $data = [];
    private $users_id;
    private $tmpImageUrl = null;

    /**
     * Create the event listener.
     *
     * @param UsersRepository $repository
     * @param ImageService $service
     */
    public function __construct(UsersRepository $repository, ImageService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

   /**
     * Handle the event.
     *
     * @param  UsersCreatedOrUpdated  $event
     * @return void
     */
    public function handle(UsersCreatedOrUpdated $event)
    {
        
        $this->data = $event->getData();
        $this->users_id = $event->getUserId();

        if ( Arrays::isActiveInactiveAction($this->data) ) {
            return;
        }

        if ( !isset($this->data['logo_url']) || empty($this->data['logo_url']) ) {
            return;
        }

        $this->prepareDeleteImageUrl();
        $this->saveImageUrl();
        $this->deleteImageUrl();
    }

    private function prepareDeleteImageUrl()
    {
        if ( $this->service->isBase64($this->data['logo_url']) === false && $this->service->fileExists($this->data['logo_url']) ) {
            $this->tmpImageUrl = $this->data['logo_url'];
        }
    }

    private function saveImageUrl()
    {
        if ( ! isset($this->data['logo_url']) || empty($this->data['logo_url']) ) {
            return;
        }

        $this->data['logo_url'] = $this->service->saveBase64OnInstance($this->data['logo_url']);
        $this->repository->skipPresenter()->update($this->data, $this->users_id);
    }

    private function deleteImageUrl()
    {
        if ( is_null($this->tmpImageUrl) ) {
            return;
        }

        if ( $this->service->isBase64($this->tmpImageUrl) === false && $this->service->fileExists($this->tmpImageUrl) ) {
            $this->service->destroyFile($this->tmpImageUrl);
        }
    }
}
