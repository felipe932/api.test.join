<?php

namespace App\Console\Commands;

use App\Services\PostgresSchemasService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Pacuna\Schemas\Facades\PGSchema;

class TenantSeeder extends Command
{
    private $schemas = [];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tenant:seed {--schemas= : Schemas to run migrates separated by comma } {--all : Run migration on all schemas independent of schemas option} {--class= : Option to specify a specific seeder class to run individually. By default command runs the DatabaseSeeder }';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to seed the schema. e.g. php artisan tenant:db:seed --schemas=mub,schema2 or php artisan tenant:db:seed --all or php artisan tenant:db:seed --all --class=UsersTableSeeder';
    /**
     * @var PostgresSchemasService
     */
    private $schemasService;

    /**
     * Create a new command instance.
     *
     * @param PostgresSchemasService $schemasService
     */
    public function __construct(PostgresSchemasService $schemasService)
    {
        parent::__construct();
        $this->schemasService = $schemasService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ( $this->option('all') ) {
            $this->listAllSchemas();
        }

        if (! $this->option('all') ) {
            $this->splitSchemas();
        }

        if ( empty($this->schemas) ) {
            return;
        }

        $this->tenantDbSeeder();
    }

    private function listAllSchemas()
    {
        $schemas = DB::table('information_schema.schemata')->where([
            ['schema_name', 'not like', 'pg_%'],
            ['schema_name', '<>', 'public'],
            ['schema_name', '<>', 'information_schema'],
        ])->get(['schema_name']);

        foreach( $schemas->toArray() as $schema ) {
            array_push($this->schemas, $schema->schema_name);
        }
    }

    private function splitSchemas()
    {
        if ( ! $this->option('schemas') ) {
            $this->error("--schema option can't  be null");
            return;
        }

        $this->schemas = explode(',', $this->option('schemas'));
    }

    private function tenantDbSeeder()
    {
        foreach( $this->schemas as $key => $schema ) {
            if ( ! $this->tenantExists($schema) ) {
                continue;
            }
            $this->schemasService->switchTo([$schema, 'public']);
            if ( is_null($this->option('class')) ) {
                Artisan::call('db:seed', ['--class' => 'TenantSeeder']);
            }
            if (! is_null($this->option('class')) ) {
                Artisan::call('db:seed', ['--class' => $this->option('class')]);
            }
            Artisan::output();
            $this->schemasService->switchTo('public');
        }
    }

    private function tenantExists($schema)
    {
        return $this->schemasService->schemaExists($schema);
    }
}
