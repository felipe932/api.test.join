<?php

namespace App\Console\Commands;

use App\Services\PostgresSchemasService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Pacuna\Schemas\Facades\PGSchema;

class TenantMigrate extends Command
{
    private $schemas = [];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tenant:migrate {--schemas= : Schemas to run migrates separated by comma } {--all : Run migration on all schemas independent of schemas option}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate a list of schemas or all schemas with the provided schemas e.g. php artisan tenant:migrate --schemas=mub,schema2 or php artisan tenant:migrate --all';
    /**
     * @var PostgresSchemasService
     */
    private $schemasService;

    /**
     * Create a new command instance.
     *
     * @param PostgresSchemasService $schemasService
     */
    public function __construct(PostgresSchemasService $schemasService)
    {
        parent::__construct();
        $this->schemasService = $schemasService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if ( $this->option('all') ) {
            $this->listAllSchemas();
        }

        if (! $this->option('all') ) {
            $this->splitSchemas();
        }

        if ( empty($this->schemas) ) {
            return;
        }

        $this->tenantMigrate();
    }

    private function listAllSchemas()
    {
        $schemas = DB::table('information_schema.schemata')->where([
            ['schema_name', 'not like', 'pg_%'],
            ['schema_name', '<>', 'public'],
            ['schema_name', '<>', 'information_schema'],
        ])->get(['schema_name']);

        foreach( $schemas->toArray() as $schema ) {
            array_push($this->schemas, $schema->schema_name);
        }
    }

    private function splitSchemas()
    {
        if ( ! $this->option('schemas') ) {
            $this->error("--schemas option can't  be null");
            return;
        }

        $this->schemas = explode(',', $this->option('schemas'));
    }

    private function tenantMigrate()
    {
        foreach( $this->schemas as $key => $schema ) {
            if ( ! $this->tenantExists($schema) ) {
                continue;
            }

            $this->schemasService->switchTo([$schema, 'public']);
            $this->schemasService->migrate($schema, ['--path' => 'database/migrations/tenant']);
            $this->schemasService->switchTo('public');
        }
    }

    private function tenantExists($schema)
    {
        return $this->schemasService->schemaExists($schema);
    }
}
