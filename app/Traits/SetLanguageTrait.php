<?php
/**
 * Created by PhpStorm.
 * User: eduardo
 * Date: 04/07/19
 * Time: 15:55
 */

namespace App\Traits;


trait SetLanguageTrait
{
    protected $languages_generals_id;

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function getLanguageId()
    {
        $this->languages_generals_id = app()->make('getLanguageId');

        return $this->languages_generals_id;
    }

    public function getDescriptionAttribute($value)
    {
        $languages_generals_id = $this->getLanguageId();
        if ( $languages_generals_id !== 'all' && $this->exists && $this->languages()->exists() ) {
            if ( $languages_generals_id === 57 ) {
                $oLanguage = $this->languages()->whereIn('languages_generals_id', [$languages_generals_id, 57] )->first();
            } else {
                $oLanguage = $this->languages()->where('languages_generals_id', $languages_generals_id )->first();
            }

            if ( is_null($oLanguage) ) {
                return $value;
            }

            return $oLanguage->description;
        }
        return $value;
    }

    /**
     * User: felipecastro
     * Date: 08/10/19
     * Time: 08:44
     */
    public function getSegmentAttribute($value)
    {
        $languages_generals_id = $this->getLanguageId();

        if ( $languages_generals_id !== 'all' && $this->exists && $this->languages()->exists() ) {

            $oLanguage = $this->languages()->where('languages_generals_id', $languages_generals_id )->first();
            if ( is_null($oLanguage) ) {
                return $value;
            }
            return $oLanguage->segment;
        }
        return $value;
    }
}
