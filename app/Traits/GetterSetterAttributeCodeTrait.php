<?php
/**
 * Created by PhpStorm.
 * User: eduardo
 * Date: 04/07/19
 * Time: 15:55
 */

namespace App\Traits;

use App\Exceptions\ExceptionGeneral;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\MessageBag;
use Illuminate\Support\Carbon;

trait GetterSetterAttributeCodeTrait
{
    public function setCodeAttribute($value)
    {
        if ( is_null($value) ) {
            $value = $this->generateCodeByID();
        }
        $this->attributes['code'] = mb_strtoupper($value);
    }

    public function getCodeAttribute($value)
    {
        return mb_strtoupper($value);
    }

    protected function generateCodeByID()
    {
        $messageBag = new MessageBag([
            "code" => [
                "The code field is required and cannot be automatically generated"
            ]
        ]);

        list($generateCodeSchema, $generateCodeTable) = explode('.', $this->table);

        $generateCodeQuery = DB::select("SELECT column_default from information_schema.columns where table_schema = '{$generateCodeSchema}' and table_name='{$generateCodeTable}' and column_name = 'id'");
        if ( empty($generateCodeQuery) ) {
            throw new ExceptionGeneral($messageBag);
        }
        $generateCodeSequence = str_replace("nextval('", '', str_replace("'::regclass)", '', $generateCodeQuery[0]->column_default));
        $generateCodeSequenceQuery = DB::select("SELECT last_value FROM {$generateCodeSequence};");
        if ( empty($generateCodeSequenceQuery) ) {
            throw new ExceptionGeneral($messageBag);
        }
        $code = $this->checkCode( (int) $generateCodeSequenceQuery[0]->last_value );
        return  $code;
    }

    protected function checkCode($last_value, $inc = 1)
    {
        $code = $last_value + $inc;
        $collection = DB::select("SELECT code FROM {$this->table} WHERE code ilike '{$code}';");
        if ( ! empty($collection) ) {
            $inc += 1;
            return $this->checkCode($last_value, $inc);
        } else {
            return $code;
        }
    }
}
