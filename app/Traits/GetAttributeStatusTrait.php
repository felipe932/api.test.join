<?php
/**
 * Created by PhpStorm.
 * User: eduardo
 * Date: 04/07/19
 * Time: 15:55
 */

namespace App\Traits;

use App\Entities\FixedValuesDomain;

trait GetAttributeStatusTrait
{
    public function attributeStatus()
    {
        return $this->hasOne(FixedValuesDomain::class, 'key', 'status')->where(['object' => 'Status']);
    }
}
