<?php
/**
 * Created by PhpStorm.
 * User: eduardo
 * Date: 02/07/19
 * Time: 15:26
 */

namespace App\Traits;

use App\Entities\InstancesGenerals;
use App\Services\PostgresSchemasService;
use Illuminate\Support\Facades\DB;
use App\Entities\InstancesTokens;

trait SetSchemasTrait

{

    private $shema = null;

    public function getSchema()
    {
        if( !is_null($this->shema) )
            return $this->shema;

        $this->_getRouteByPath();

        $this->_getAutorization();

        if( is_null($this->shema))
            $this->shema = 'public';

        return $this->shema;
    }

    public function setSchema($schema)
    {
        $this->shema = $schema;
    }

    protected function _getRouteByPath()
    {

        if( request()->index ) {
            $this->shema = request()->index;
        }

        if ( request()->route('uuid') && is_null($this->shema) ) {
            $this->shema = request()->route('uuid');
        }

        if ( !is_null( request()->path() )  && is_null($this->shema) ) {
            $aPath = explode("/", request()->path());
            if ( isset($aPath[2]) && !empty($aPath[2]) && substr( $aPath[2], 0, 3 ) === "mub" ) {
                $this->shema = $aPath[2];
            }
        }

        if ( is_null($this->shema) ) {
            $service = new PostgresSchemasService();
            $searchPath = $service->getSearchPath();
            if ( ! is_null($searchPath) && $searchPath !== '') {
                $this->shema = $searchPath;
            }
        }
    }

    protected function _getAutorization(){

        if( is_null($this->shema) ) {
            $authorization = explode('Bearer',request()->header('Authorization'));
            if ( isset($authorization[1]) && !empty($authorization[1]) ){
                $token = trim($authorization[1]);
                $instance_token = InstancesTokens::where('token',$token)->first();
                if(  !empty($instance_token) ) {
                    $this->shema = $instance_token->uuid;
                }
            }
        }

        if ( is_null( request()->path() ) && is_null($this->shema) ) {
            $this->shema = 'public';
        }
    }
}
