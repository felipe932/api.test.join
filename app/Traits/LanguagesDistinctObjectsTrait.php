<?php
/**
 * Created by Felipe.
 * Date: 29/01/20
 * Time: 10:21
 */

namespace App\Traits;

use App\Entities\LanguagesGeneral;
use App\Entities\InstancesLanguages;

trait LanguagesDistinctObjectsTrait
{

    protected $appLanguage;

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function getAppLanguage()
    {
        $request = $this->request->all();
        $this->appLanguage = $request['appLanguage'];
    }

    public function language_object()
    {
        $this->getAppLanguage();

        $languages_generals_id = $this->_getDescriptionsTypesDescriptions( $this->_getInstanceLanguages() );
        $languages_generals_id = array_unique($languages_generals_id);

        $this->data = LanguagesGeneral::whereIn('id',$languages_generals_id)->get();

        return $this->setData($this->data)->setMessage('Descriptions distinct show with success!')->success();
    }

    public function _getInstanceLanguages()
    {
        $request = $this->request->all();
        $search = $request['search'];
        list($object, $id) = explode(':', $search);

        $instance_languages_ids = [];
        $instances_languages = InstancesLanguages::where('instances_generals_id',$id)->get();
        if(count($instances_languages)>0) {
            foreach($instances_languages as $instances_language){
                $instance_languages_ids[] = $instances_language->languages_generals_id;
            }
        }
        return $instance_languages_ids;
    }

    public function _getDescriptionsTypesDescriptions( $languages_generals_id = [ 57 ] )
    {
        if( isset($this->repositoryDescriptions) ) {
            $objects = $this->repositoryDescriptions->makeModel()->select('languages_generals_id')->groupBy('languages_generals_id')->get();
            if($objects){
                foreach($objects as $object) {
                    $languages_generals_id[] = $object->languages_generals_id;
                }
            }
        }

        return array_unique($languages_generals_id);
    }
}
