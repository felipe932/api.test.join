<?php

namespace App\Traits;

use App\Helpers\Arrays;
use Illuminate\Support\Arr;

use Matrix\Exception;
use Symfony\Component\HttpFoundation\Response;

trait ResponseTrait
{
    protected $data;
    protected $message;
    protected $file = 'response.xlsx';
    protected $collection;

    public function error()
    {
        return $this->json(Response::HTTP_INTERNAL_SERVER_ERROR);
    }

    public function success()
    {
        return $this->json();
    }

    protected function json($status = Response::HTTP_OK)
    {
        return response()->json([
            "success" => true,
            "message" => $this->getMessage(),
            "data" => $this->getData()
        ], $status);
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     * @return ResponseTrait
     */
    public function setData($data)
    {
        if( ! is_array($data)) {
            $data = $data->toArray();
        }

        $this->data = $data;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     * @return ResponseTrait
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

}
