<?php

namespace App\Traits;

use App\Entities\AdditionalFieldsGenerals;
use App\Entities\AdditionalFieldsObjects;

trait AdditionalFieldsObjectsTrait
{
    protected $additional_fields_id;
    protected $object_id;

    public function findObject($object_type)
    {
        $query = AdditionalFieldsGenerals::where('object_type', $object_type)->get()->first();
        if ( ! is_null( $query ) ) {
            $this->additional_fields_id = $query->id;
        }
        return $this;
    }

    public function deleteObjects()
    {
        if ( is_null($this->additional_fields_id) ) {
            return;
        }

        AdditionalFieldsObjects::where([
            [ "additional_fields_id", '=', $this->additional_fields_id ],
            [ "object_id", '=', $this->getObjectId() ]
        ])->delete();
    }

    /**
     * @return mixed
     */
    public function getObjectId()
    {
        return $this->object_id;
    }

    /**
     * @param mixed $object_id
     * @return AdditionalFieldsObjectsTrait
     */
    public function setObjectId($object_id)
    {
        $this->object_id = $object_id;
        return $this;
    }

}
