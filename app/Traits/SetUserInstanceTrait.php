<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpFoundation\Response;

trait SetUserInstanceTrait
{
    public function setUserInstanceTrait()
    {
        $users_id = request()->user()->id;
        $instances_generals_id = app()->make('getInstanceID');
        request()->request->add([
            'users_create_id' => $users_id,
            'instances_generals_id' => $instances_generals_id
        ]);
    }
}
