<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpFoundation\Response;

trait ExceptionTrait
{
    use MyDatabaseTransactions;

    protected $bSuccess = false;
    protected $statusCode = 500;
    protected $message = null;

    public function apiException($request, $e)
    {
        $this->setStatusCode($e);
        $this->setMessage($e);
        $this->checkSpecialCases($e);

        $this->rollback();

        return response()->json([
            "success" => $this->bSuccess,
            "message" => $this->message
        ], $this->statusCode);
    }

    protected function setStatusCode($e): void
    {
        if ( $this->hasGetCode($e) ) {
            $this->statusCode = $e->getCode();
        }

        if ( $this->hasGetStatusCode($e) ) {
            $this->statusCode = $e->getStatusCode();
        }

        if ( $this->hasGetHttpStatusCode($e) ) {
            $this->statusCode = $e->getHttpStatusCode();
        }
    }

    protected function setMessage($e): void
    {
        if ( $this->hasGetMessageBag($e) ) {
            $this->message = $e->getMessageBag();
        }

        if ( $this->hasGetMessage($e) && is_null($this->message) ) {
            $this->message = $e->getMessage();
        }

        if ( ! array_key_exists($this->statusCode, Response::$statusTexts) ) {
            $this->statusCode = 500;
        }

        if ( is_null($this->message) || empty($this->message) ) {
            $this->message = Response::$statusTexts[$this->statusCode];
        }
    }

    /**
     * @return null
     */
    public function getMessage()
    {
        return $this->message;
    }

    protected function hasGetHttpStatusCode($e)
    {
        return method_exists($e, 'getHttpStatusCode');
    }

    protected function hasGetStatusCode($e)
    {
        return method_exists($e, 'getStatusCode');
    }

    protected function hasGetCode($e)
    {
        return method_exists($e, 'getCode');
    }

    protected function hasGetMessage($e)
    {
        return method_exists($e, 'getMessage');
    }

    protected function hasGetMessageBag($e)
    {
        return method_exists($e, 'getMessageBag');
    }

    protected function checkSpecialCases($e)
    {
        if ( request()->method() === "DELETE" && $e instanceof ModelNotFoundException ) {
            $this->statusCode = 200;
            $this->bSuccess = true;
        }
    }

}
