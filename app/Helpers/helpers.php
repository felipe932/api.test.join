<?php
/**
 * Created by PhpStorm.
 * User: eduardo
 * Date: 27/09/18
 * Time: 09:37
 */

use App\Helpers\Arrays;

if (! function_exists('array_empty')) {
    /**
     * Check if an item or items exist in an array using "dot" notation.
     *
     * @param  \ArrayAccess|array  $array
     * @param  string|array  $keys
     * @return bool
     */
    function array_empty($array, $keys)
    {
        return Arrays::is_empty($array, $keys) === false;
    }
}

if (! function_exists('array_set_merge')) {
    /**
     * Check if an item or items exist in an array using "dot" notation.
     *
     * @param  \ArrayAccess|array $array
     * @param array $values
     * @return array
     */
    function array_set_merge(array $array, array $values)
    {
        return Arrays::set($array, $values);
    }
}

if (! function_exists('generate_username')) {
    /**
     * Check if an item or items exist in an array using "dot" notation.
     *
     * @param array $array
     * @param string $key
     * @param string $objKey
     * @return void
     */
    function generate_username(array $array, $key = 'contacts', $objKey = 'cellphone')
    {
        if ( array_empty($array, $key)) {
            return null;
        }

        $phone = Arrays::findValueObjectKeyInArray($array[$key], $objKey);

        return str_phone($phone, false);
    }
}

if (! function_exists('str_phone')) {
    /**
     * Check if an item or items exist in an array using "dot" notation.
     *
     * @param $phone
     * @param bool $ddd
     * @param bool $format
     * @param bool $ddi
     * @return void
     */
    function str_phone($phone, $ddd = true, $format = false, $ddi = false)
    {
        $newPhone = preg_replace('/\D/', '', $phone);

        if ( strlen($newPhone) < 9 || strlen($newPhone) > 12 ) {
            return $newPhone;
        }

        $newPhone = $ddd === false && ( strlen($newPhone) == 11 || strlen($newPhone) == 12 ) ? substr($phone, 2) : $newPhone;

        if ( $format ) {
            //TODO
        }

        if ( $ddi ) {
            //TODO
        }

        return $newPhone;
    }
}

if(! function_exists("dec") ) {
    /**
     * Format a string do decimal value
     * Ex: 300,45 - 300.45
     * @param string $value
     * @param int $precision
     * @return float
     */
    function dec($value, $precision = 2)
    {
        if ( is_null($value) ) {
            return $value;
        }
        $value = preg_replace("/[^0-9,.-]/", "", $value);
        if (strpos($value, ',') > 0) {
            $value = str_replace(".", "", $value);
            $value = str_replace(",", ".", $value);
        } elseif (substr_count($value, ".") > 1) {
            $new_num = "";
            $count = substr_count($value, ".");
            $array = explode('.', $value);
            foreach ($array as $key => $number) {
                if ($key == $count) {
                    $new_num .= "." . $number;
                } else {
                    $new_num .= $number;
                }
            }
            $value = $new_num;
        }
        /**  @deprecated bcmul - $value = bcmul((float)$value, 100, $precision) */
        $value = round((float)$value * 100, $precision);
        /**  @deprecated bcdiv - $value = bcdiv((float)$value, 100, $precision) */
        $value = round((float)$value/100, $precision);
        return $value;
    }
}

if(! function_exists("generate_schema") ) {
    function generate_schema()
    {
        return "z_" . str_random(6) . date('YmdHis');
    }
}

if(! function_exists("voucher_code") ) {
    function voucher_code($voucher_type = 1, $prefix = null)
    {
        if ( is_null($prefix)) {
            switch ($voucher_type) {
                case 1:
                    $prefix = "GB";
                    break;
                case 2:
                    $prefix = "RF";
                    break;
                default:
                    $prefix = "GB";
            }
        }
        return $prefix . "-" . strtoupper(substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, 6));
    }
}

if(! function_exists("array_keep_keys") ) {
    function array_keep_keys($array, $keys = array())
    {
        if (empty($array) || (!is_array($array))) {
            return $array;
        }
        if (is_string($keys)) {
            $keys = explode(',', $keys);
        }
        if (!is_array($keys)) {
            return $array;
        }
        foreach ($array as $key => $value) {
            if ( in_array($key, $keys) ) {
                continue;
            }
            unset($array[$key]);
        }
        return $array;
    }
}

if(! function_exists("dec_keep_zeros") ) {
    function dec_keep_zeros($value, $precision = 2)
    {
        if ( is_null($value) ) {
            return $value;
        }

        $value = dec($value, $precision);
        $aValue = explode('.', $value);

        if ( ! isset($aValue[1]) || empty($aValue[1]) ) {
            $value = $aValue[0] . '.' . str_pad(null, $precision, '0', STR_PAD_RIGHT);
        }
        if ( isset($aValue[1]) && ! empty($aValue[1]) ) {
            $value = $aValue[0] . '.' . str_pad($aValue[1], $precision, '0', STR_PAD_RIGHT);
        }

        return rtrim($value, '.');
    }
}

if(! function_exists("is_json") ) {
    function is_json($value)
    {
        try {
            json_decode($value);
            return true;
        } catch (\Exception $e ) {
            return false;
        }
    }
}

if(! function_exists("is_not_empty_array") ) {
    function is_not_empty_array($array, $key)
    {
        return isset($array[$key]) && ! empty($array[$key]);
    }
}

