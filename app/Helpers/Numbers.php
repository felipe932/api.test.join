<?php


namespace App\Helpers;

use App\Entities\AttributeValuesShowAs;
use App\Entities\UnitsOfMesuares;
use Illuminate\Support\Facades\DB;
use PHPUnit\Exception;

class Numbers
{
    public static function setUOMSourceDestination($base_uom_id, $revert = false)
    {
        try {
            $baseUOM = UnitsOfMesuares::find($base_uom_id);
            $standardUOM = UnitsOfMesuares::where(['dimension' => $baseUOM->dimension, 'dimension_standard_uom' => TRUE])->get()->first();
            if ($revert) {
                return [
                    "source" => $standardUOM->id,
                    "destination" => $baseUOM->id
                ];
            }

            return [
                "source" => $baseUOM->id,
                "destination" => $standardUOM->id
            ];
        } catch (\Exception $exception) {
            return [
                "source" => null,
                "destination" => null
            ];
        }
    }

    public static function convertUOM($value = null, $source_uom = null, $destination_uom = null)
    {
        if ( is_null($value) || is_null($source_uom) || is_null($destination_uom) ) {
            return $value;
        }

        $sourceUOM = UnitsOfMesuares::find($source_uom);

        /** INCH */
        if ( $sourceUOM->uom === '"' ) {
            $aValue = explode(' ', $value);
            if ( isset($aValue[1]) && !empty($aValue[1]) && $aValue[1] === '/' ) {
                $value = (int) preg_replace("/[^0-9,.-]/", "", $value);
            } else {
                $integer = null;
                $valueAux = $aValue[0];
                if ( isset($aValue[1]) && !empty($aValue[1]) ) {
                    $integer = $aValue[0];
                    $valueAux = $aValue[1];
                }

                $oShowAs = AttributeValuesShowAs::where(['uom' => $sourceUOM->uom, 'show_as' => str_replace(' /', '/', $valueAux)])->get()->first();

                if ( ! is_null($oShowAs) ) {
                    $value = $oShowAs->numeric_value;
                }
                if ( ! is_null($integer) && is_numeric($value) ) {
                    $value += $integer;
                }

            }

            if( strpos($value, '/') > 0 ) {
                $value = preg_replace("/[^0-9,.-]/", "", $value);
            }

            return $value;
        }

        $destinationUOM = UnitsOfMesuares::find($destination_uom);

        if ( empty($sourceUOM->denominator) || empty($sourceUOM->numerator) ||
                empty($destinationUOM->numerator) || empty($destinationUOM->denominator)) {

            $destination_value = self::_convertTemperatureUOM($value, $sourceUOM, $destinationUOM);
            if ( ! $destination_value ) {
                $destination_value = $value;
            }
            return $destination_value;
        }

        $standard_uom_value = dec($value, 9) * ($sourceUOM->denominator / $sourceUOM->numerator);
        $destination_value = $standard_uom_value * ($destinationUOM->numerator / $destinationUOM->denominator);

        /** INCH */
        if ( $destinationUOM->uom === '"' ) {
            $destination_value = dec($destination_value, 7);
            $oDestinationValue = explode('.', $destination_value);

            if ( ! isset($oDestinationValue[1]) || empty($oDestinationValue[1]) ) {
                return $oDestinationValue[0];
            }

            $destination_value = "0.{$oDestinationValue[1]}00";

            $oShowAs = AttributeValuesShowAs::where(['uom' => $destinationUOM->uom, 'numeric_value' => $destination_value])->get()->first();

            if ( $oDestinationValue[0] === '0' && ! is_null($oShowAs) ) {
                return $oShowAs->show_as;
            } elseif ( $oDestinationValue[0] !== '0' && ! is_null($oShowAs) ) {
                return $oDestinationValue[0] . ' ' . $oShowAs->show_as;
            } else {
                return $oDestinationValue[0];
            }
        }

        return dec($destination_value, 9);
    }

    public static function generateCode($value, $table, $attributes)
    {
        if ( isset($value) && !empty($value) && !is_null($value) && $value != "null" ) {
            return $value;
        }

        if ( isset($attributes['id']) && !empty($attributes['id']) && !is_null($attributes['id']) ) {
            return $attributes['id'];
        }

        $sequence = "{$table}_id_seq";
        $query = DB::select("SELECT last_value FROM {$sequence};");
        if ( isset($query[0]) && !empty($query[0]) ) {
           $value = $query[0]->last_value + 1;
        }
        return $value;
    }

    public static function generateMubDNA($value, $table, $id = null)
    {
        if ( isset($value) && !empty($value) && !is_null($value) && $value != "null" && $value != "###mub_dna###" ) {
            return $value;
        }

        try {
            list($auxUUID, $auxTable) = explode('.', $table);

            if ( !is_null($id) ) {
                $attrId = $id;
            } else {
                $sequence = "{$table}_id_seq";
                $query = DB::select("SELECT last_value FROM {$sequence};");
                $attrId = $query[0]->last_value + 1;
            }

            $value = "{$auxUUID}_{$attrId}";
            return $value;
        } catch (\Exception $e ) {
            return "###mub_dna###";
        }
    }

    private static function _convertTemperatureUOM($value, $sourceUOM, $destinationUOM)
    {
        if ( $sourceUOM->dimension != 'TEMPARATURE' ) {
            return false;
        }

        $value = dec($value, 9);

        if ( $sourceUOM->dimension_standard_uom ) {
            if ( $destinationUOM->conversion_formula == '1' ) {
                return ($value * 9/5) + 32;
            }

            if ( $destinationUOM->conversion_formula == '2' ) {
                return ($value + 273.15);
            }
        } else {
            if ( $sourceUOM->conversion_formula == '1' ) {
                return ($value - 32) * 5/9;
            }

            if ( $sourceUOM->conversion_formula == '2' ) {
                return ($value - 273.15);
            }
        }
        return $value;
    }
}
