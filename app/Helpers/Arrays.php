<?php
/**
 * Created by PhpStorm.
 * User: eduardo
 * Date: 27/09/18
 * Time: 09:49
 */

namespace App\Helpers;

use Illuminate\Support\Arr;

class Arrays
{

    public static function is_empty($array, $keys)
    {
        if ( Arr::has($array, $keys) === false ) {
            return false;
        }

        $keys = (array) $keys;

        foreach ($keys as $key) {
            $subKeyArray = $array;

            if (Arr::exists($array, $key) && !empty($subKeyArray[$key]) ) {
                continue;
            }

            foreach (explode('.', $key) as $segment) {
                if (Arr::accessible($subKeyArray) && Arr::exists($subKeyArray, $segment) && !empty($subKeyArray[$segment])) {
                    $subKeyArray = $subKeyArray[$segment];
                } else {
                    return false;
                }
            }
        }

        return true;
    }

    public static function set(array $array, array $values)
    {
        foreach ($array as $key => $rules) {
            foreach($rules as $segment => $value) {
                if (Arr::accessible($rules) && Arr::exists($rules, $segment) && Arr::accessible($values) && Arr::exists($values, $segment)) {
                    $array[$key][$segment] = $values[$segment];
                }
            }
        }

        return $array;
    }

    public static function findValueObjectKeyInArray(array $array, $objKey)
    {
        $return = '';
        foreach ($array as $item) {
            if( $item['type'] != $objKey ) {
                continue;
            }
            $return = $item['value'];
        }
        return $return;
    }

    public static function arrayValuesRecursive($array) {
        $flat = array();

        foreach($array as $value) {
            if (is_array($value)) {
                $flat = array_merge($flat, static::arrayValuesRecursive($value));
                dd($flat);
            }
            else {
                $flat[] = $value;
            }
        }
        return $flat;
    }

    public static function orderByArray($array, $field, $sortBy = 'asc')
    {
        if ( $sortBy === 'desc' ) {
            usort($array, function ($a, $b) use ($field) { return strcmp($b[$field], $a[$field]); });
            return $array;
        }
        usort($array, function ($a, $b) use ($field) { return strcmp($a[$field], $b[$field]); });
        return $array;
    }

    public static function arrayUnique($array)
    {
        return array_map("unserialize", array_unique(array_map("serialize", $array)));
    }

    public static function jsonToArray($string)
    {
        return json_decode($string, true);
    }

    /**
     * @param $search_value
     * @param $array
     * @param $id_path
     * @return string|null
     */
    public static function arraySearchMultidimensional($search_value, $array, $id_path) {
        if(is_array($array) && count($array) > 0) {
            foreach($array as $key => $value) {
                $temp_path = $id_path;
                array_push($temp_path, $key);
                if(is_array($value) && count($value) > 0) {
                    $res_path = static::arraySearchMultidimensional ($search_value, $value, $temp_path);
                    if ($res_path != null) {
                        return $res_path;
                    }
                }
                else if($value == $search_value) {
                    return join(" --> ", $temp_path);
                }
            }
        }
        return false;
    }

    public static function arrayUniqueValuesSort($array, $isValues = true, $isSort = false)
    {
        $array = array_unique($array);
        if ( $isValues ) $array = array_values($array);
        if ( $isSort ) sort($array);
        return $array;
    }

    public static function collectionUnique($array, $fields = ['languages_generals_id', 'description'])
    {
        return collect($array)->unique(function ($item) use ($fields) {
            $return = null;
            foreach ( $fields as $field ) {
                if ( ! isset($item[$field]) ) {
                    continue;
                }
                $return .= $item[$field];
            }
            return $return;
        })->toArray();
    }

    public static function collectionDescriptionsNotNull($array, $fields = ['languages_generals_id', 'description'])
    {
        return collect($array)->filter(function($item) use ($fields) {
            return collect($fields)->filter(function($key) use ($item){
                return ! isset( $item[$key] ) || is_null($item[$key]);
            })->count() === 0;
        })->toArray();
    }

    public static function isActiveInactiveAction($array)
    {
        unset($array['appLanguage']);
        unset($array['updated_at']);
        return count($array) === 1 && array_key_exists('status', $array);
    }
}
