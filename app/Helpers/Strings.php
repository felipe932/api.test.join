<?php


namespace App\Helpers;


use App\Entities\LanguagesGeneral;
use App\Exceptions\ExceptionGeneral;
use Illuminate\Support\MessageBag;

class Strings
{
    public static function formatAttributeValue($attribute_value, $precision = 2)
    {

    }

    public static function configDescriptionOperation($value_from = null, $operator_value_from = null, $value_to = null, $operator_value_to = null) {
        $description = $value_from;
        if ( $operator_value_from != "=" ) {
            $description = "{$operator_value_from} {$value_from}";
        }
		if ( (int) self::configAttributeOperator($operator_value_from, $operator_value_to) <= 4 && (int) self::configAttributeOperator($operator_value_from, $operator_value_to) > 0 ) {
            $description .= " {$operator_value_to} {$value_to}";
        }
		return $description;
	}

    public static function configAttributeOperator($operator_value_from, $operator_value_to = null)
    {
        $attribute_operator = null;

        if ( $operator_value_from === '>=' && $operator_value_to === '<' ) {
            $attribute_operator = '1';
        }

        if ( $operator_value_from === '>=' && $operator_value_to === '<=' ) {
            $attribute_operator = '2';
        }

        if ( $operator_value_from === '>' && $operator_value_to === '<' ) {
            $attribute_operator = '3';
        }

        if ( $operator_value_from === '>' && $operator_value_to === '<=' ) {
            $attribute_operator = '4';
        }

        if ( $operator_value_from === '<' && $operator_value_to === null ) {
            $attribute_operator = '5';
        }

        if ( $operator_value_from === '<=' && $operator_value_to === null ) {
            $attribute_operator = '6';
        }

        if ( $operator_value_from === '>' && $operator_value_to === null ) {
            $attribute_operator = '7';
        }

        if ( $operator_value_from === '>=' && $operator_value_to === null ) {
            $attribute_operator = '8';
        }

        return $attribute_operator;
    }

    public static function sanitize($string, $removeBlankSpaces = false) {
        if ( $removeBlankSpaces ) {
            $string = str_replace(' ', '-', $string);
        }
        return preg_replace('/[^A-Za-z0-9]/', '', $string);
    }

    public static function strlenHtml($string)
    {

    }

    public static function getArgo2iHash($hash)
    {
        list($argo, $p) = explode(',p=', $hash);
        return $p;
    }

    public static function normalize ($string) {
        $table = array(
            'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
            'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
            'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
            'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
            'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
            'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
            'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r',
        );

        return strtr($string, $table);
    }

    public static function checkEmailOperationAllowed($email = null)
    {
        if ( (bool) env('MUB_BLOCK_EMAILS', false ) === false ) {
            return;
        }

        if ( is_null($email) || empty($email) || $email === '' ) {
            return;
        }

        $mub_enabled_emails = explode( ';',  env('MUB_ENABLED_EMAILS', null) );
        $isValidEmail = empty( array_filter($mub_enabled_emails, function($value) { return !is_null($value) && $value !== ''; }) ) ;

        foreach ( $mub_enabled_emails as $rule ) {
            if ( $isValidEmail ) {
                continue;
            }
            $pattern = '|' . $rule . "|";
            if (preg_match($pattern, $email)) {
                $isValidEmail = true;
            }
        }

        if ( ! $isValidEmail ) {
            $messageBag = new MessageBag([
                0 => [
                    'useMessage' => true,
                    'message' => 'MSG.T79',
                    'code' => 'EMAIL_NOT_ALLOWED'
                ]
            ]);
            throw new ExceptionGeneral($messageBag);
        }
    }

    public static function getLanguageData($id = null, $language_code = null)
    {
        if ( ! is_null($id) ) {
            $collection = LanguagesGeneral::where(['id' => $id])->first();
            return $collection->language_code;
        }

        if ( ! is_null($language_code) ) {
            $collection = LanguagesGeneral::where(['language_code' => $language_code])->first();
            return $collection->id;
        }
    }
}
