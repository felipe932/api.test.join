<?php

use Illuminate\Database\Seeder;

class PassportInstallSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("TRUNCATE TABLE oauth_clients RESTART IDENTITY CASCADE");
        DB::table('oauth_clients')->insert([
            [
                "id" => 1,
                "user_id" => null,
                "name" => "Laravel Personal Access Client",
                "secret" => "Q7i8LQJkSVRGE33yLSkKlNVP589ymEI6beA1Chj7",
                "redirect" => "https://localhost:8000",
                "personal_access_client" => TRUE,
                "password_client" => FALSE,
                "revoked" => FALSE,
                "created_at" => new Datetime,
                "updated_at" => new Datetime,
            ],
            [
                "id" => 2,
                "user_id" => null,
                "name" => "Laravel Password Grant Client",
                "secret" => "m60M1ExvAD69RJ6h1amtsxaGGE1X4nC30vdSFgKz",
                "redirect" => "https://localhost:8000",
                "personal_access_client" => FALSE,
                "password_client" => TRUE,
                "revoked" => FALSE,
                "created_at" => new Datetime,
                "updated_at" => new Datetime,
            ]
        ]);
        DB::statement("ALTER SEQUENCE public.oauth_clients_id_seq RESTART WITH 3");
        DB::statement("VACUUM(FULL) oauth_clients");
    }
}
