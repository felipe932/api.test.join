<?php

use Illuminate\Database\Seeder;

class GrantAllSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO ' . env('DB_USERNAME') . ';');
        DB::statement("DROP EXTENSION IF EXISTS unaccent");
        DB::statement('CREATE EXTENSION unaccent;');
    }
}
