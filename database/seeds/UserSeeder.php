<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        DB::statement("TRUNCATE TABLE users RESTART IDENTITY CASCADE");
        DB::table('users')->insert([
            [
                "id" => 1, 
                "full_name" => "Join", 
                "email" => "developer@join.com.br", 
                "address" => '{"address": null, "cities_id": 79, "states_id": 1, "countries_id": 1}',
                "meta" => '{"language": "pt-br"}',
                "password" => '$argon2i$v=19$m=1024,t=2,p=2$U2hqYmlTSGNLdlQvYjFvMg$+S+em68KsW+KrOuGDG15Z+DgXvBGhrxducxhGJSrC/U',
                "active" => 1,
                "created_at" => new Datetime, 
                "updated_at" => new Datetime 
            ]
        ]);
        DB::statement("ALTER SEQUENCE users_id_seq RESTART WITH 28");
        DB::statement("VACUUM(FULL) users");
    }
}
