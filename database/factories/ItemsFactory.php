<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Entities\ItemGeneral::class, function (Faker $faker) {
    return [
        'code' => str_replace(' ', '_', strtoupper($faker->words(2, true))),
        "status" => $faker->randomElement(['A','I','C']),
        "unit_of_measures_generals_id" => $faker->randomElement([1,2,3,4,5,6,7]),
        'mub_zero_code' => $faker->boolean(),
        'users_id' => 1,
    ];
});
