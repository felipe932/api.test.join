<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Entities\ItemPartnumbers::class, function (Faker $faker) {
    return [
        "vendors_generals_id" => 1,
        "part_number" => $faker->phoneNumber(),
        "plain_part_number" => $faker->phoneNumber(),
        "substitute_part_number" => $faker->phoneNumber(),
        "status" => 'A',
    ];
});
