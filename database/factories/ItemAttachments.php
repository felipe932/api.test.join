<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Entities\ItemAttachmentsGeneral::class, function (Faker $faker) {
    return [
        'description' => $faker->realText(200, 2),
        "url" => $faker->imageUrl(600, 300, 'transport'),
        "file_extension" => 'JPG',
        "file_size" => '123456',
        "attachment_type"  => 'I',
        "status" => $faker->randomElement(['A','I','C']),
        "thumbnail" => false
    ];
});
