<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Entities\CategoryGeneral::class, function (Faker $faker) {
    $codes = ['SUS430','SUS303','SUS304','SUS316','SUS316Ti','ABS','PA','PP','POM','PE','PC','PVC','PBT','PS','UP','PUR','NR','NBR','CR','IR','SBR','BR','SI','EPDM'];
    return [
        'code' => $faker->randomElement($codes) . $faker->randomNumber(),
        'description' => $faker->realText(200, 2),
        'type' => 'TEC',
        'status' => $faker->randomElement(['A','I','C']),
        'duplicity_check' => false,
        'image_url' => $faker->imageUrl(600, 300, 'transport')
    ];
});
