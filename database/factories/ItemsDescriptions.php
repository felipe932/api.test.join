<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Entities\ItemDescriptions::class, function (Faker $faker) {
    $description_id = $faker->randomElement([1,2]);
    return [
        "languages_generals_id" => $faker->randomElement([167,46,61]),
        "descriptions_types_generals_id" => $description_id,
        "description" => $description_id === 1 ? $faker->realText(200, 2) : $faker->realText(7000, 5),
        "fixed" => $faker->boolean(),
    ];
});
