<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('full_name');
            $table->string('email')->unique();
            $table->jsonb('contacts')->nullable();
            $table->jsonb('address')->nullable();
            $table->jsonb('meta')->nullable();
            $table->string('password')->nullable();
            $table->smallInteger('active')->default(0);
            $table->rememberToken();
            $table->text('logo_url')->nullable();
            $table->text('token_access')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
