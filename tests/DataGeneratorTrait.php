<?php
/**
 * Created by PhpStorm.
 * User: eduardo
 * Date: 10/10/18
 * Time: 15:18
 */

namespace Tests;

use Illuminate\Support\Arr;

trait DataGeneratorTrait
{
    public function code()
    {
        $string = str_replace(' ', '-', $this->faker->name());
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        return mb_strtoupper($string);
    }

    public function status()
    {
        return Arr::random(['A','I','C']);
    }

    public function boolean()
    {
        return Arr::random([true, false]);
    }

    public function languageID()
    {
        return Arr::random([167,57,67]);
    }

    public function table()
    {
        return $this->schema . '.' . $this->sTable;
    }

    public function setSeeInDatabase($input)
    {
        $aReturn = [];
        foreach ($input as $key => $value) {
            if (is_array($value)) {
                continue;
            }
            $aReturn[$key] = $value;
        }
        return $aReturn;
    }

    public function setDataResponse($isCreateOrUpdate = false, $withRelations = false, $withSuccess = true, $withMessage = true, $withData = true)
    {
        $aResponse = [];

        if ( ! $withRelations ) {
            foreach( $this->dataResponse as $key => $item ) {
                if ( $key === '*' ) {
                    continue;
                }
                unset($this->dataResponse[$key]);
            }
        }

        if ( $withSuccess ) {
            array_push($aResponse, 'success');
        }

        if ( $withMessage ) {
            array_push($aResponse, 'message');
        }

        if ( $withData && ! $isCreateOrUpdate ) {
            $aResponse = Arr::add($aResponse, 'data', $this->dataResponse);
        }

        if ( $isCreateOrUpdate ) {
            $aResponse = Arr::add($aResponse, 'data', $this->dataResponse['*']);
        }

        return $aResponse;
    }

    public function _generateDataCategory($id = null)
    {
        $data = [
            'code' => $this->code(),
            'description' => $this->faker->text(),
            'type' => Arr::random(['TEC','STD','HIE']),
            'status' => $this->status(),
            'duplicity_check' => $this->boolean(),
            'image_url' => $this->faker->text(),
            'descriptions' => [
                'languages_generals_id' => $this->languageID(),
                'category_generals_id' => $id,
                'description' => $this->faker->text(),
            ]
        ];

        return $data;
    }

    public function _generateDataItem($id = null)
    {
        $data = [
            'code' => $this->code(),
            'status' => $this->status(),
            'unit_of_measures_generals_id' => 1,
            'mub_zero_code' => $this->faker->text(),
            'users_id' => 1,
        ];
        return $data;
    }

    public function _generateData($fields = [], $extra_data = [])
    {
        $aData = [];
        foreach( $fields as $key => $value ) {
            $aData = Arr::add( $aData, $key, $this->_generateDataByType($value) );
        }

        return array_merge($aData, $extra_data);
    }

    public function _generateDataResponse()
    {

    }

    private function _generateDataByType($type = 'text')
    {
        switch ($type) {
            case 'name':
                return $this->faker->name();
                break;
            case 'text':
                return $this->faker->text();
                break;
            case 'status':
                return $this->status();
                break;
            case 'bool':
                return $this->boolean();
                break;
            case 'languageID':
                return $this->languageID();
                break;
            case 'fileExtension':
                return $this->faker->fileExtension();
                break;
            case 'imageUrl':
                return $this->faker->imageUrl($width = 640, $height = 480);
                break;
            case 'randomFileSize':
                return $this->faker->randomNumber($nbDigits = NULL, $strict = false) . "Mb";
                break;
            case 'integer':
                return $this->faker->randomDigitNotNull();
                break;
            case 'decimal4':
                return $this->faker->randomFloat($nbMaxDecimals = 4, $min = 0, $max = NULL);
                break;
            case 'decimal9':
                return $this->faker->randomFloat($nbMaxDecimals = 9, $min = 0, $max = NULL);
                break;
            case 'operators':
                return $this->faker->randomElements($array = array ('<=','<','>=','=','>'), $count = 1)[0];
                break;
            case 'barcode':
                return $this->faker->ean13();
                break;
            case 'partnumber':
                return $this->faker->currencyCode() . $this->faker->sha1();
                break;
            case 'code':
                return $this->code();
                break;
            default:
                return $this->faker->text();
                break;
        }
    }

    public function _generateDescriptions($fieldId = [], $extra_data = [])
    {
        $aDescription = [
            'languages_generals_id' => $this->languageID(),
            'description' => $this->faker->text()
        ];

        return array_merge($aDescription, $fieldId, $extra_data);
    }


}
