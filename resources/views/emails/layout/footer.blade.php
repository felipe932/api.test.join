                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <p></p>
                            <p></p>
                            <table class="es-footer" cellspacing="0" cellpadding="0" align="center">
                                <tbody>
                                    <tr>
                                        <td class="esd-stripe" align="center">
                                            <table class="es-footer-body" width="600" cellspacing="0" cellpadding="0" align="center">
                                                <tbody>
                                                    <tr>
                                                        <td class="esd-structure es-p20t es-p20b es-p20r es-p20l" align="left">
                                                            <table width="100%" cellspacing="0" cellpadding="0" top="20px" >
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="esd-container-frame" width="560" valign="top" align="center">
                                                                            <table width="100%" cellspacing="0" cellpadding="0">
                                                                                <tbody>
                                                                                    <tr style="margin" >
                                                                                        <td class="esd-block-spacer es-p20" align="center">
                                                                                            <table width="75%" height="100%" cellspacing="0" cellpadding="0" border="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td style="border-bottom: 1px solid #cccccc; background:none; height:1px; width:100%; margin:0px 0px 0px 0px;"></td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="esd-block-text es-p10t es-p10b" align="center">
                                                                                            <p> @lang( 'email.T28' , [] , isset($language) && !empty($language) ? $language : env('APP_LANG') ) </p>
                                                                                            <p> @lang( 'email.T29' , [] , isset($language) && !empty($language) ? $language : env('APP_LANG') ) </p>
                                                                                            <p> @lang( 'email.T30' , [] , isset($language) && !empty($language) ? $language : env('APP_LANG') ) </p>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td class="esd-block-text es-p10t es-p10b" align="center">
                                                                                            <p> @lang( 'email.T31' , [] , isset($language) && !empty($language) ? $language : env('APP_LANG') ) <a target="_blank" href="mailto:support@mub.one"> @lang('email.T33' , [] , isset($language) && !empty($language) ? $language : env('APP_LANG') )</a>.</p>
                                                                                            <p><br></p>
                                                                                            <p> @lang( 'email.T32' , [] , isset($language) && !empty($language) ? $language : env('APP_LANG') ) </p>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="esd-footer-popover es-content" cellspacing="0" cellpadding="0" align="center">
                                <tbody>
                                    <tr>
                                        <td class="esd-stripe" align="center">
                                            <table class="es-content-body" style="background-color: transparent;" width="600" cellspacing="0" cellpadding="0" align="center">
                                                <tbody>
                                                    <tr>
                                                        <td class="esd-structure es-p30b es-p20r es-p20l" align="left">
                                                            <table width="100%" cellspacing="0" cellpadding="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td class="esd-container-frame" width="560" valign="top" align="center">
                                                                            <table width="100%" cellspacing="0" cellpadding="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td class="esd-block-image es-infoblock" align="center"><a target="_blank" href="https://mub.one/wp-content/uploads/2019/11/mub-novo-logo01-1200x517.png"><img src="https://mub.one/wp-content/uploads/2019/11/mub-novo-logo01-1200x517.png" alt width="125" style="display: block;"></a></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
</html>
