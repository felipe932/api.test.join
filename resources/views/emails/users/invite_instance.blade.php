@extends('emails.layout.main')

@section('content')
    
    <table width="100%" cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td align="left" class="esd-block-text es-p15b">
                    <h2> @lang( 'email.T22' , [] , $language ) </h2>
                </td>
            </tr>
            <tr>
                <td class="esd-block-text es-p20t" align="left">
                    <p> @lang( 'email.T14' , ['full_name' => $full_name ] , $language ) </p>
                </td>
            </tr>
            <tr>
                <td class="esd-block-text es-p15t" align="left">
                    <p> @lang( 'email.T23' , [ 'instance_description' => $instance_description, 'company' => $company ] , $language ) </p>
                </td>
            </tr>
            <tr>
                <td class="esd-block-text es-p15t" align="left">
                    <p> @lang( 'email.T24' , [] , $language ) <a href="{{env('APP_FRONT_URL') . "/user_profile/active/" . $token }}{{ $reset_password === true ? '?resetPassword=true' : null }}"> @lang( 'email.T34' , [] , $language ) </a>.<o:p></o:p></span> </p>
                </td>
            </tr>
            <tr>
                <td class="esd-block-text es-p15t" align="left">
                    <p> @lang( 'email.T4' , [] , $language ) </p>
                </td>
            </tr>
            <tr>
                <td class="esd-block-text es-p15t" align="left">
                    <p class=MsoNormal style='Calibri"sans-serif;orphans: 2;-webkit-text-stroke-width: 0px;word-spacing:0px'>
                        <span style='color:#4472C4'>
                        <a href="{{env('APP_FRONT_URL') . "/user_profile/active/" . $token }}{{ $reset_password === true ? '?resetPassword=true' : null }}">{{env('APP_FRONT_URL') . "/user_profile/active/" . $token }}{{ $reset_password === true ? '?resetPassword=true' : null }}</a>
                        </span>
                    </p>
                </td>
            </tr>
        </tbody>
    </table>

@endsection