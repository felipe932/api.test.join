
@extends('emails.layout.main')

@section('content')
    
    <table width="100%" cellspacing="0" cellpadding="0">
        <tbody>
            <tr>
                <td align="left" class="esd-block-text es-p15b">
                    <h2> @lang( 'email.T6' , [] , $language ) </h2>
                </td>
            </tr>
            <tr>
                <td class="esd-block-text es-p20t" align="left">
                    <p> @lang( 'email.T7' , ['full_name' => $full_name ] , $language ) </p>
                </td>
            </tr>
            <tr>
                <td class="esd-block-text es-p15t" align="left">
                    <p> @lang( 'email.T8' , [] , $language ) </p>
                </td>
            </tr>
            <tr>
                <td class="esd-block-text es-p15t" align="left">
                    <p> @lang( 'email.T9' , [] , $language ) </p>
                </td>
            </tr>
            <tr>
                <td class="esd-block-text es-p15t" align="left">
                    <p> @lang( 'email.T10' , [] , $language ) </p>
                </td>
            </tr>
            <tr>
                <td class="esd-block-text es-p15t" align="left">
                    <p> @lang( 'email.T11' , [] , $language ) </p>
                </td>
            </tr>
        </tbody>
    </table>

@endsection