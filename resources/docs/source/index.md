---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://127.0.0.1:8888/docs/collection.json)

<!-- END_INFO -->

#Vendors


<!-- START_b91a53f5cf98e9338c0884a58291a392 -->
## GetList

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8888/api/v2/vendors/general" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImRmYWQxNjQ0NGJlYzUyMDIyNDQ0M2RkZTBlOGYyMTdmNDg5YTU1MjViM2I3Nzc2ZDgyMzc1NjQ2NTRhY2JjZjJjYmRmZjdjMzJkMmQ2OTIzIn0.eyJhdWQiOiIxIiwianRpIjoiZGZhZDE2NDQ0YmVjNTIwMjI0NDQzZGRlMGU4ZjIxN2Y0ODlhNTUyNWIzYjc3NzZkODIzNzU2NDY1NGFjYmNmMmNiZGZmN2MzMmQyZDY5MjMiLCJpYXQiOjE1ODA5MzQ4MTYsIm5iZiI6MTU4MDkzNDgxNiwiZXhwIjoxNjEyNTU3MjE2LCJzdWIiOiIxMiIsInNjb3BlcyI6W119.M6CvzgSDLmrHajrD7pK66RyoxEsKpIb0ZsmUHpSTFIzbdjysCacghg3d6XRCgAj8XF0v9DRHOVM_Mel8nQoPXlgeS89dK5qoXbu7JmLia0PK6oK60bX4DaIQnGSi80P5KoOuLm2DyVAgQNZjatXR6Z_KjPGsXl7QSbvPrWVIDZJVjvGuIeXeUHf96Nfy6mZBuu8HxVhJKXLgnwG6XUJ6bF-rSTLaWsWcvLOyI9uVaL68ajLtVUju_09wpmwJR5nrLBm1bzum-hylrypqqYQs52RyoWSVyyu7wsvvoDjQ8CGReQU27SZKHvQEDMewP3UFHuB3ahSAbUcz3tkQsgv1pZ2g_-U0VVqBY1O25k9xRavU15_UIhaYqxMPRO1a2kEdJA8nS2ix_Y8sXwdXvbxSixga9jwh834NH_RqKGlW362b3aDzmTbskQuxFrop6RRJr-0Y4kliBzSg14E3btlNizavgF8mtdK0yH2_6vMcV5L6pbZKeMGw45CGhL0TgOx6fkqcq1MmuiyRtujBCZY5J13YA-iR-5rURzo1iK5mfXsUkgsn-9p3x53J80YaHhqGWhQnPeaQGpboyo1dIbaYlTfH30hj9rWDHzIWIvks2BFmsMPTh6dUZ_dMevRjWuAvKWmhNnjqQg2cT9ouDFQZ1ejQngyLOwYzQMuGiSCF-6g"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8888/api/v2/vendors/general"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImRmYWQxNjQ0NGJlYzUyMDIyNDQ0M2RkZTBlOGYyMTdmNDg5YTU1MjViM2I3Nzc2ZDgyMzc1NjQ2NTRhY2JjZjJjYmRmZjdjMzJkMmQ2OTIzIn0.eyJhdWQiOiIxIiwianRpIjoiZGZhZDE2NDQ0YmVjNTIwMjI0NDQzZGRlMGU4ZjIxN2Y0ODlhNTUyNWIzYjc3NzZkODIzNzU2NDY1NGFjYmNmMmNiZGZmN2MzMmQyZDY5MjMiLCJpYXQiOjE1ODA5MzQ4MTYsIm5iZiI6MTU4MDkzNDgxNiwiZXhwIjoxNjEyNTU3MjE2LCJzdWIiOiIxMiIsInNjb3BlcyI6W119.M6CvzgSDLmrHajrD7pK66RyoxEsKpIb0ZsmUHpSTFIzbdjysCacghg3d6XRCgAj8XF0v9DRHOVM_Mel8nQoPXlgeS89dK5qoXbu7JmLia0PK6oK60bX4DaIQnGSi80P5KoOuLm2DyVAgQNZjatXR6Z_KjPGsXl7QSbvPrWVIDZJVjvGuIeXeUHf96Nfy6mZBuu8HxVhJKXLgnwG6XUJ6bF-rSTLaWsWcvLOyI9uVaL68ajLtVUju_09wpmwJR5nrLBm1bzum-hylrypqqYQs52RyoWSVyyu7wsvvoDjQ8CGReQU27SZKHvQEDMewP3UFHuB3ahSAbUcz3tkQsgv1pZ2g_-U0VVqBY1O25k9xRavU15_UIhaYqxMPRO1a2kEdJA8nS2ix_Y8sXwdXvbxSixga9jwh834NH_RqKGlW362b3aDzmTbskQuxFrop6RRJr-0Y4kliBzSg14E3btlNizavgF8mtdK0yH2_6vMcV5L6pbZKeMGw45CGhL0TgOx6fkqcq1MmuiyRtujBCZY5J13YA-iR-5rURzo1iK5mfXsUkgsn-9p3x53J80YaHhqGWhQnPeaQGpboyo1dIbaYlTfH30hj9rWDHzIWIvks2BFmsMPTh6dUZ_dMevRjWuAvKWmhNnjqQg2cT9ouDFQZ1ejQngyLOwYzQMuGiSCF-6g",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):

```json
{
    "success": false,
    "message": "Unauthenticated."
}
```

### HTTP Request
`GET api/v2/vendors/general`


<!-- END_b91a53f5cf98e9338c0884a58291a392 -->

<!-- START_63719e50e659b13afc6bc12cf28b06d2 -->
## Create

> Example request:

```bash
curl -X POST \
    "http://127.0.0.1:8888/api/v2/vendors/general" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImRmYWQxNjQ0NGJlYzUyMDIyNDQ0M2RkZTBlOGYyMTdmNDg5YTU1MjViM2I3Nzc2ZDgyMzc1NjQ2NTRhY2JjZjJjYmRmZjdjMzJkMmQ2OTIzIn0.eyJhdWQiOiIxIiwianRpIjoiZGZhZDE2NDQ0YmVjNTIwMjI0NDQzZGRlMGU4ZjIxN2Y0ODlhNTUyNWIzYjc3NzZkODIzNzU2NDY1NGFjYmNmMmNiZGZmN2MzMmQyZDY5MjMiLCJpYXQiOjE1ODA5MzQ4MTYsIm5iZiI6MTU4MDkzNDgxNiwiZXhwIjoxNjEyNTU3MjE2LCJzdWIiOiIxMiIsInNjb3BlcyI6W119.M6CvzgSDLmrHajrD7pK66RyoxEsKpIb0ZsmUHpSTFIzbdjysCacghg3d6XRCgAj8XF0v9DRHOVM_Mel8nQoPXlgeS89dK5qoXbu7JmLia0PK6oK60bX4DaIQnGSi80P5KoOuLm2DyVAgQNZjatXR6Z_KjPGsXl7QSbvPrWVIDZJVjvGuIeXeUHf96Nfy6mZBuu8HxVhJKXLgnwG6XUJ6bF-rSTLaWsWcvLOyI9uVaL68ajLtVUju_09wpmwJR5nrLBm1bzum-hylrypqqYQs52RyoWSVyyu7wsvvoDjQ8CGReQU27SZKHvQEDMewP3UFHuB3ahSAbUcz3tkQsgv1pZ2g_-U0VVqBY1O25k9xRavU15_UIhaYqxMPRO1a2kEdJA8nS2ix_Y8sXwdXvbxSixga9jwh834NH_RqKGlW362b3aDzmTbskQuxFrop6RRJr-0Y4kliBzSg14E3btlNizavgF8mtdK0yH2_6vMcV5L6pbZKeMGw45CGhL0TgOx6fkqcq1MmuiyRtujBCZY5J13YA-iR-5rURzo1iK5mfXsUkgsn-9p3x53J80YaHhqGWhQnPeaQGpboyo1dIbaYlTfH30hj9rWDHzIWIvks2BFmsMPTh6dUZ_dMevRjWuAvKWmhNnjqQg2cT9ouDFQZ1ejQngyLOwYzQMuGiSCF-6g" \
    -d '{"code":"dolor","name":"qui","status":"accusantium","logo_url":"natus","reduced_logo_url":"dolorem","vendor_type":"M","users_create_id":9,"users_update_i":9}'

```

```javascript
const url = new URL(
    "http://127.0.0.1:8888/api/v2/vendors/general"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImRmYWQxNjQ0NGJlYzUyMDIyNDQ0M2RkZTBlOGYyMTdmNDg5YTU1MjViM2I3Nzc2ZDgyMzc1NjQ2NTRhY2JjZjJjYmRmZjdjMzJkMmQ2OTIzIn0.eyJhdWQiOiIxIiwianRpIjoiZGZhZDE2NDQ0YmVjNTIwMjI0NDQzZGRlMGU4ZjIxN2Y0ODlhNTUyNWIzYjc3NzZkODIzNzU2NDY1NGFjYmNmMmNiZGZmN2MzMmQyZDY5MjMiLCJpYXQiOjE1ODA5MzQ4MTYsIm5iZiI6MTU4MDkzNDgxNiwiZXhwIjoxNjEyNTU3MjE2LCJzdWIiOiIxMiIsInNjb3BlcyI6W119.M6CvzgSDLmrHajrD7pK66RyoxEsKpIb0ZsmUHpSTFIzbdjysCacghg3d6XRCgAj8XF0v9DRHOVM_Mel8nQoPXlgeS89dK5qoXbu7JmLia0PK6oK60bX4DaIQnGSi80P5KoOuLm2DyVAgQNZjatXR6Z_KjPGsXl7QSbvPrWVIDZJVjvGuIeXeUHf96Nfy6mZBuu8HxVhJKXLgnwG6XUJ6bF-rSTLaWsWcvLOyI9uVaL68ajLtVUju_09wpmwJR5nrLBm1bzum-hylrypqqYQs52RyoWSVyyu7wsvvoDjQ8CGReQU27SZKHvQEDMewP3UFHuB3ahSAbUcz3tkQsgv1pZ2g_-U0VVqBY1O25k9xRavU15_UIhaYqxMPRO1a2kEdJA8nS2ix_Y8sXwdXvbxSixga9jwh834NH_RqKGlW362b3aDzmTbskQuxFrop6RRJr-0Y4kliBzSg14E3btlNizavgF8mtdK0yH2_6vMcV5L6pbZKeMGw45CGhL0TgOx6fkqcq1MmuiyRtujBCZY5J13YA-iR-5rURzo1iK5mfXsUkgsn-9p3x53J80YaHhqGWhQnPeaQGpboyo1dIbaYlTfH30hj9rWDHzIWIvks2BFmsMPTh6dUZ_dMevRjWuAvKWmhNnjqQg2cT9ouDFQZ1ejQngyLOwYzQMuGiSCF-6g",
};

let body = {
    "code": "dolor",
    "name": "qui",
    "status": "accusantium",
    "logo_url": "natus",
    "reduced_logo_url": "dolorem",
    "vendor_type": "M",
    "users_create_id": 9,
    "users_update_i": 9
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`POST api/v2/vendors/general`

#### Body Parameters
Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    `code` | string |  optional  | 
        `name` | string |  required  | 
        `status` | string |  required  | 
        `logo_url` | string |  optional  | 
        `reduced_logo_url` | string |  optional  | 
        `vendor_type` | string |  required  | "M" or "V" (Manufacturer or Vendor).
        `users_create_id` | integer |  required  | The id of the user.
        `users_update_i` | integer |  required  | The id of the user.
    
<!-- END_63719e50e659b13afc6bc12cf28b06d2 -->

<!-- START_9a4a76a27cbc5e0da4c8e534668d1641 -->
## GetDetail

> Example request:

```bash
curl -X GET \
    -G "http://127.0.0.1:8888/api/v2/vendors/general/atque" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImRmYWQxNjQ0NGJlYzUyMDIyNDQ0M2RkZTBlOGYyMTdmNDg5YTU1MjViM2I3Nzc2ZDgyMzc1NjQ2NTRhY2JjZjJjYmRmZjdjMzJkMmQ2OTIzIn0.eyJhdWQiOiIxIiwianRpIjoiZGZhZDE2NDQ0YmVjNTIwMjI0NDQzZGRlMGU4ZjIxN2Y0ODlhNTUyNWIzYjc3NzZkODIzNzU2NDY1NGFjYmNmMmNiZGZmN2MzMmQyZDY5MjMiLCJpYXQiOjE1ODA5MzQ4MTYsIm5iZiI6MTU4MDkzNDgxNiwiZXhwIjoxNjEyNTU3MjE2LCJzdWIiOiIxMiIsInNjb3BlcyI6W119.M6CvzgSDLmrHajrD7pK66RyoxEsKpIb0ZsmUHpSTFIzbdjysCacghg3d6XRCgAj8XF0v9DRHOVM_Mel8nQoPXlgeS89dK5qoXbu7JmLia0PK6oK60bX4DaIQnGSi80P5KoOuLm2DyVAgQNZjatXR6Z_KjPGsXl7QSbvPrWVIDZJVjvGuIeXeUHf96Nfy6mZBuu8HxVhJKXLgnwG6XUJ6bF-rSTLaWsWcvLOyI9uVaL68ajLtVUju_09wpmwJR5nrLBm1bzum-hylrypqqYQs52RyoWSVyyu7wsvvoDjQ8CGReQU27SZKHvQEDMewP3UFHuB3ahSAbUcz3tkQsgv1pZ2g_-U0VVqBY1O25k9xRavU15_UIhaYqxMPRO1a2kEdJA8nS2ix_Y8sXwdXvbxSixga9jwh834NH_RqKGlW362b3aDzmTbskQuxFrop6RRJr-0Y4kliBzSg14E3btlNizavgF8mtdK0yH2_6vMcV5L6pbZKeMGw45CGhL0TgOx6fkqcq1MmuiyRtujBCZY5J13YA-iR-5rURzo1iK5mfXsUkgsn-9p3x53J80YaHhqGWhQnPeaQGpboyo1dIbaYlTfH30hj9rWDHzIWIvks2BFmsMPTh6dUZ_dMevRjWuAvKWmhNnjqQg2cT9ouDFQZ1ejQngyLOwYzQMuGiSCF-6g"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8888/api/v2/vendors/general/atque"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImRmYWQxNjQ0NGJlYzUyMDIyNDQ0M2RkZTBlOGYyMTdmNDg5YTU1MjViM2I3Nzc2ZDgyMzc1NjQ2NTRhY2JjZjJjYmRmZjdjMzJkMmQ2OTIzIn0.eyJhdWQiOiIxIiwianRpIjoiZGZhZDE2NDQ0YmVjNTIwMjI0NDQzZGRlMGU4ZjIxN2Y0ODlhNTUyNWIzYjc3NzZkODIzNzU2NDY1NGFjYmNmMmNiZGZmN2MzMmQyZDY5MjMiLCJpYXQiOjE1ODA5MzQ4MTYsIm5iZiI6MTU4MDkzNDgxNiwiZXhwIjoxNjEyNTU3MjE2LCJzdWIiOiIxMiIsInNjb3BlcyI6W119.M6CvzgSDLmrHajrD7pK66RyoxEsKpIb0ZsmUHpSTFIzbdjysCacghg3d6XRCgAj8XF0v9DRHOVM_Mel8nQoPXlgeS89dK5qoXbu7JmLia0PK6oK60bX4DaIQnGSi80P5KoOuLm2DyVAgQNZjatXR6Z_KjPGsXl7QSbvPrWVIDZJVjvGuIeXeUHf96Nfy6mZBuu8HxVhJKXLgnwG6XUJ6bF-rSTLaWsWcvLOyI9uVaL68ajLtVUju_09wpmwJR5nrLBm1bzum-hylrypqqYQs52RyoWSVyyu7wsvvoDjQ8CGReQU27SZKHvQEDMewP3UFHuB3ahSAbUcz3tkQsgv1pZ2g_-U0VVqBY1O25k9xRavU15_UIhaYqxMPRO1a2kEdJA8nS2ix_Y8sXwdXvbxSixga9jwh834NH_RqKGlW362b3aDzmTbskQuxFrop6RRJr-0Y4kliBzSg14E3btlNizavgF8mtdK0yH2_6vMcV5L6pbZKeMGw45CGhL0TgOx6fkqcq1MmuiyRtujBCZY5J13YA-iR-5rURzo1iK5mfXsUkgsn-9p3x53J80YaHhqGWhQnPeaQGpboyo1dIbaYlTfH30hj9rWDHzIWIvks2BFmsMPTh6dUZ_dMevRjWuAvKWmhNnjqQg2cT9ouDFQZ1ejQngyLOwYzQMuGiSCF-6g",
};

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):

```json
{
    "id": 1,
    "code": "TESTE",
    "name": "Vendor Teste",
    "status": "A",
    "logo_url": "",
    "reduced_logo_url": "",
    "vendor_type": "V",
    "users_create_id": 1,
    "users_update_id": 1
}
```

### HTTP Request
`GET api/v2/vendors/general/{id}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The ID of the update.

<!-- END_9a4a76a27cbc5e0da4c8e534668d1641 -->

<!-- START_114a0071073bfa055ddcafe00dc78ad5 -->
## Change

> Example request:

```bash
curl -X PATCH \
    "http://127.0.0.1:8888/api/v2/vendors/general/aperiam" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImRmYWQxNjQ0NGJlYzUyMDIyNDQ0M2RkZTBlOGYyMTdmNDg5YTU1MjViM2I3Nzc2ZDgyMzc1NjQ2NTRhY2JjZjJjYmRmZjdjMzJkMmQ2OTIzIn0.eyJhdWQiOiIxIiwianRpIjoiZGZhZDE2NDQ0YmVjNTIwMjI0NDQzZGRlMGU4ZjIxN2Y0ODlhNTUyNWIzYjc3NzZkODIzNzU2NDY1NGFjYmNmMmNiZGZmN2MzMmQyZDY5MjMiLCJpYXQiOjE1ODA5MzQ4MTYsIm5iZiI6MTU4MDkzNDgxNiwiZXhwIjoxNjEyNTU3MjE2LCJzdWIiOiIxMiIsInNjb3BlcyI6W119.M6CvzgSDLmrHajrD7pK66RyoxEsKpIb0ZsmUHpSTFIzbdjysCacghg3d6XRCgAj8XF0v9DRHOVM_Mel8nQoPXlgeS89dK5qoXbu7JmLia0PK6oK60bX4DaIQnGSi80P5KoOuLm2DyVAgQNZjatXR6Z_KjPGsXl7QSbvPrWVIDZJVjvGuIeXeUHf96Nfy6mZBuu8HxVhJKXLgnwG6XUJ6bF-rSTLaWsWcvLOyI9uVaL68ajLtVUju_09wpmwJR5nrLBm1bzum-hylrypqqYQs52RyoWSVyyu7wsvvoDjQ8CGReQU27SZKHvQEDMewP3UFHuB3ahSAbUcz3tkQsgv1pZ2g_-U0VVqBY1O25k9xRavU15_UIhaYqxMPRO1a2kEdJA8nS2ix_Y8sXwdXvbxSixga9jwh834NH_RqKGlW362b3aDzmTbskQuxFrop6RRJr-0Y4kliBzSg14E3btlNizavgF8mtdK0yH2_6vMcV5L6pbZKeMGw45CGhL0TgOx6fkqcq1MmuiyRtujBCZY5J13YA-iR-5rURzo1iK5mfXsUkgsn-9p3x53J80YaHhqGWhQnPeaQGpboyo1dIbaYlTfH30hj9rWDHzIWIvks2BFmsMPTh6dUZ_dMevRjWuAvKWmhNnjqQg2cT9ouDFQZ1ejQngyLOwYzQMuGiSCF-6g"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8888/api/v2/vendors/general/aperiam"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImRmYWQxNjQ0NGJlYzUyMDIyNDQ0M2RkZTBlOGYyMTdmNDg5YTU1MjViM2I3Nzc2ZDgyMzc1NjQ2NTRhY2JjZjJjYmRmZjdjMzJkMmQ2OTIzIn0.eyJhdWQiOiIxIiwianRpIjoiZGZhZDE2NDQ0YmVjNTIwMjI0NDQzZGRlMGU4ZjIxN2Y0ODlhNTUyNWIzYjc3NzZkODIzNzU2NDY1NGFjYmNmMmNiZGZmN2MzMmQyZDY5MjMiLCJpYXQiOjE1ODA5MzQ4MTYsIm5iZiI6MTU4MDkzNDgxNiwiZXhwIjoxNjEyNTU3MjE2LCJzdWIiOiIxMiIsInNjb3BlcyI6W119.M6CvzgSDLmrHajrD7pK66RyoxEsKpIb0ZsmUHpSTFIzbdjysCacghg3d6XRCgAj8XF0v9DRHOVM_Mel8nQoPXlgeS89dK5qoXbu7JmLia0PK6oK60bX4DaIQnGSi80P5KoOuLm2DyVAgQNZjatXR6Z_KjPGsXl7QSbvPrWVIDZJVjvGuIeXeUHf96Nfy6mZBuu8HxVhJKXLgnwG6XUJ6bF-rSTLaWsWcvLOyI9uVaL68ajLtVUju_09wpmwJR5nrLBm1bzum-hylrypqqYQs52RyoWSVyyu7wsvvoDjQ8CGReQU27SZKHvQEDMewP3UFHuB3ahSAbUcz3tkQsgv1pZ2g_-U0VVqBY1O25k9xRavU15_UIhaYqxMPRO1a2kEdJA8nS2ix_Y8sXwdXvbxSixga9jwh834NH_RqKGlW362b3aDzmTbskQuxFrop6RRJr-0Y4kliBzSg14E3btlNizavgF8mtdK0yH2_6vMcV5L6pbZKeMGw45CGhL0TgOx6fkqcq1MmuiyRtujBCZY5J13YA-iR-5rURzo1iK5mfXsUkgsn-9p3x53J80YaHhqGWhQnPeaQGpboyo1dIbaYlTfH30hj9rWDHzIWIvks2BFmsMPTh6dUZ_dMevRjWuAvKWmhNnjqQg2cT9ouDFQZ1ejQngyLOwYzQMuGiSCF-6g",
};

fetch(url, {
    method: "PATCH",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`PATCH api/v2/vendors/general/{id}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The ID of the update.

<!-- END_114a0071073bfa055ddcafe00dc78ad5 -->

<!-- START_2d1b57a225ca42eaebe6151d17d2ba73 -->
## Delete

> Example request:

```bash
curl -X DELETE \
    "http://127.0.0.1:8888/api/v2/vendors/general/perferendis" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -H "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImRmYWQxNjQ0NGJlYzUyMDIyNDQ0M2RkZTBlOGYyMTdmNDg5YTU1MjViM2I3Nzc2ZDgyMzc1NjQ2NTRhY2JjZjJjYmRmZjdjMzJkMmQ2OTIzIn0.eyJhdWQiOiIxIiwianRpIjoiZGZhZDE2NDQ0YmVjNTIwMjI0NDQzZGRlMGU4ZjIxN2Y0ODlhNTUyNWIzYjc3NzZkODIzNzU2NDY1NGFjYmNmMmNiZGZmN2MzMmQyZDY5MjMiLCJpYXQiOjE1ODA5MzQ4MTYsIm5iZiI6MTU4MDkzNDgxNiwiZXhwIjoxNjEyNTU3MjE2LCJzdWIiOiIxMiIsInNjb3BlcyI6W119.M6CvzgSDLmrHajrD7pK66RyoxEsKpIb0ZsmUHpSTFIzbdjysCacghg3d6XRCgAj8XF0v9DRHOVM_Mel8nQoPXlgeS89dK5qoXbu7JmLia0PK6oK60bX4DaIQnGSi80P5KoOuLm2DyVAgQNZjatXR6Z_KjPGsXl7QSbvPrWVIDZJVjvGuIeXeUHf96Nfy6mZBuu8HxVhJKXLgnwG6XUJ6bF-rSTLaWsWcvLOyI9uVaL68ajLtVUju_09wpmwJR5nrLBm1bzum-hylrypqqYQs52RyoWSVyyu7wsvvoDjQ8CGReQU27SZKHvQEDMewP3UFHuB3ahSAbUcz3tkQsgv1pZ2g_-U0VVqBY1O25k9xRavU15_UIhaYqxMPRO1a2kEdJA8nS2ix_Y8sXwdXvbxSixga9jwh834NH_RqKGlW362b3aDzmTbskQuxFrop6RRJr-0Y4kliBzSg14E3btlNizavgF8mtdK0yH2_6vMcV5L6pbZKeMGw45CGhL0TgOx6fkqcq1MmuiyRtujBCZY5J13YA-iR-5rURzo1iK5mfXsUkgsn-9p3x53J80YaHhqGWhQnPeaQGpboyo1dIbaYlTfH30hj9rWDHzIWIvks2BFmsMPTh6dUZ_dMevRjWuAvKWmhNnjqQg2cT9ouDFQZ1ejQngyLOwYzQMuGiSCF-6g"
```

```javascript
const url = new URL(
    "http://127.0.0.1:8888/api/v2/vendors/general/perferendis"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "Authorization": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImRmYWQxNjQ0NGJlYzUyMDIyNDQ0M2RkZTBlOGYyMTdmNDg5YTU1MjViM2I3Nzc2ZDgyMzc1NjQ2NTRhY2JjZjJjYmRmZjdjMzJkMmQ2OTIzIn0.eyJhdWQiOiIxIiwianRpIjoiZGZhZDE2NDQ0YmVjNTIwMjI0NDQzZGRlMGU4ZjIxN2Y0ODlhNTUyNWIzYjc3NzZkODIzNzU2NDY1NGFjYmNmMmNiZGZmN2MzMmQyZDY5MjMiLCJpYXQiOjE1ODA5MzQ4MTYsIm5iZiI6MTU4MDkzNDgxNiwiZXhwIjoxNjEyNTU3MjE2LCJzdWIiOiIxMiIsInNjb3BlcyI6W119.M6CvzgSDLmrHajrD7pK66RyoxEsKpIb0ZsmUHpSTFIzbdjysCacghg3d6XRCgAj8XF0v9DRHOVM_Mel8nQoPXlgeS89dK5qoXbu7JmLia0PK6oK60bX4DaIQnGSi80P5KoOuLm2DyVAgQNZjatXR6Z_KjPGsXl7QSbvPrWVIDZJVjvGuIeXeUHf96Nfy6mZBuu8HxVhJKXLgnwG6XUJ6bF-rSTLaWsWcvLOyI9uVaL68ajLtVUju_09wpmwJR5nrLBm1bzum-hylrypqqYQs52RyoWSVyyu7wsvvoDjQ8CGReQU27SZKHvQEDMewP3UFHuB3ahSAbUcz3tkQsgv1pZ2g_-U0VVqBY1O25k9xRavU15_UIhaYqxMPRO1a2kEdJA8nS2ix_Y8sXwdXvbxSixga9jwh834NH_RqKGlW362b3aDzmTbskQuxFrop6RRJr-0Y4kliBzSg14E3btlNizavgF8mtdK0yH2_6vMcV5L6pbZKeMGw45CGhL0TgOx6fkqcq1MmuiyRtujBCZY5J13YA-iR-5rURzo1iK5mfXsUkgsn-9p3x53J80YaHhqGWhQnPeaQGpboyo1dIbaYlTfH30hj9rWDHzIWIvks2BFmsMPTh6dUZ_dMevRjWuAvKWmhNnjqQg2cT9ouDFQZ1ejQngyLOwYzQMuGiSCF-6g",
};

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```



### HTTP Request
`DELETE api/v2/vendors/general/{id}`

#### URL Parameters

Parameter | Status | Description
--------- | ------- | ------- | -------
    `id` |  required  | The ID of the destroy.

<!-- END_2d1b57a225ca42eaebe6151d17d2ba73 -->


