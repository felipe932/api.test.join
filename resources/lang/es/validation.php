<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages.
    |
    */

    'accepted'        => ':attribute debe ser aceptado.',
    'active_url'      => ':attribute no es una URL válida.',
    'after'           => ':attribute debe ser una fecha posterior a :date.',
    'after_or_equal'  => ':attribute debe ser una fecha posterior o igual a :date.',
    'alpha'           => ':attribute sólo debe contener letras.',
    'alpha_dash'      => ':attribute sólo debe contener letras, números y guiones.',
    'alpha_num'       => ':attribute sólo debe contener letras y números.',
    'array'           => ':attribute debe ser un conjunto.',
    'before'          => ':attribute debe ser una fecha anterior a :date.',
    'before_or_equal' => ':attribute debe ser una fecha anterior o igual a :date.',
    'between'         => [
        'numeric' => ':attribute tiene que estar entre :min - :max.',
        'file'    => ':attribute debe pesar entre :min - :max kilobytes.',
        'string'  => ':attribute tiene que tener entre :min - :max caracteres.',
        'array'   => ':attribute tiene que tener entre :min - :max ítems.',
    ],
    'boolean'        => 'El campo :attribute debe tener un valor verdadero o falso.',
    'confirmed'      => 'La confirmación de :attribute no coincide.',
    'date'           => ':attribute no es una fecha válida.',
    'date_equals'    => ':attribute debe ser una fecha igual a :date.',
    'date_format'    => ':attribute no corresponde al formato :format.',
    'different'      => ':attribute y :other deben ser diferentes.',
    'digits'         => ':attribute debe tener :digits dígitos.',
    'digits_between' => ':attribute debe tener entre :min y :max dígitos.',
    'dimensions'     => 'Las dimensiones de la imagen :attribute no son válidas.',
    'distinct'       => 'El campo :attribute contiene un valor duplicado.',
    'email'          => ':attribute no es un correo válido',
    'ends_with'      => 'El campo :attribute debe finalizar con uno de los siguientes valores: :values',
    'exists'         => ':attribute es inválido.',
    'file'           => 'El campo :attribute debe ser un archivo.',
    'filled'         => 'El campo :attribute es obligatorio.',
    'gt'             => [
        'numeric' => 'El campo :attribute debe ser mayor que :value.',
        'file'    => 'El campo :attribute debe tener más de :value kilobytes.',
        'string'  => 'El campo :attribute debe tener más de :value caracteres.',
        'array'   => 'El campo :attribute debe tener más de :value elementos.',
    ],
    'gte' => [
        'numeric' => 'El campo :attribute debe ser como mínimo :value.',
        'file'    => 'El campo :attribute debe tener como mínimo :value kilobytes.',
        'string'  => 'El campo :attribute debe tener como mínimo :value caracteres.',
        'array'   => 'El campo :attribute debe tener como mínimo :value elementos.',
    ],
    'image'    => ':attribute debe ser una imagen.',
    'in'       => ':attribute es inválido.',
    'in_array' => 'El campo :attribute no existe en :other.',
    'integer'  => ':attribute debe ser un número entero.',
    'ip'       => ':attribute debe ser una dirección IP válida.',
    'ipv4'     => ':attribute debe ser un dirección IPv4 válida',
    'ipv6'     => ':attribute debe ser un dirección IPv6 válida.',
    'json'     => 'El campo :attribute debe tener una cadena JSON válida.',
    'lt'       => [
        'numeric' => 'El campo :attribute debe ser menor que :value.',
        'file'    => 'El campo :attribute debe tener menos de :value kilobytes.',
        'string'  => 'El campo :attribute debe tener menos de :value caracteres.',
        'array'   => 'El campo :attribute debe tener menos de :value elementos.',
    ],
    'lte' => [
        'numeric' => 'El campo :attribute debe ser como máximo :value.',
        'file'    => 'El campo :attribute debe tener como máximo :value kilobytes.',
        'string'  => 'El campo :attribute debe tener como máximo :value caracteres.',
        'array'   => 'El campo :attribute debe tener como máximo :value elementos.',
    ],
    'max' => [
        'numeric' => ':attribute no debe ser mayor a :max.',
        'file'    => ':attribute no debe ser mayor que :max kilobytes.',
        'string'  => ':attribute no debe ser mayor que :max caracteres.',
        'array'   => ':attribute no debe tener más de :max elementos.',
    ],
    'mimes'     => ':attribute debe ser un archivo con formato: :values.',
    'mimetypes' => ':attribute debe ser un archivo con formato: :values.',
    'min'       => [
        'numeric' => 'El tamaño de :attribute debe ser de al menos :min.',
        'file'    => 'El tamaño de :attribute debe ser de al menos :min kilobytes.',
        'string'  => ':attribute debe contener al menos :min caracteres.',
        'array'   => ':attribute debe tener al menos :min elementos.',
    ],
    'not_in'               => ':attribute es inválido.',
    'not_regex'            => 'El formato del campo :attribute no es válido.',
    'numeric'              => ':attribute debe ser numérico.',
    'password'             => 'La contraseña es incorrecta.',
    'present'              => 'El campo :attribute debe estar presente.',
    'regex'                => 'El formato de :attribute es inválido.',
    'required'             => 'El campo :attribute es obligatorio.',
    'required_if'          => 'El campo :attribute es obligatorio cuando :other es :value.',
    'required_unless'      => 'El campo :attribute es obligatorio a menos que :other esté en :values.',
    'required_with'        => 'El campo :attribute es obligatorio cuando :values está presente.',
    'required_with_all'    => 'El campo :attribute es obligatorio cuando :values está presente.',
    'required_without'     => 'El campo :attribute es obligatorio cuando :values no está presente.',
    'required_without_all' => 'El campo :attribute es obligatorio cuando ninguno de :values estén presentes.',
    'same'                 => ':attribute y :other deben coincidir.',
    'size'                 => [
        'numeric' => 'El tamaño de :attribute debe ser :size.',
        'file'    => 'El tamaño de :attribute debe ser :size kilobytes.',
        'string'  => ':attribute debe contener :size caracteres.',
        'array'   => ':attribute debe contener :size elementos.',
    ],
    'starts_with' => 'El campo :attribute debe comenzar con uno de los siguientes valores: :values',
    'string'      => 'El campo :attribute debe ser una cadena de caracteres.',
    'timezone'    => 'El :attribute debe ser una zona válida.',
    'unique'      => 'El campo :attribute ya ha sido registrado.',
    'uploaded'    => 'Subir :attribute ha fallado.',
    'url'         => 'El formato :attribute es inválido.',
    'uuid'        => 'El campo :attribute debe ser un UUID válido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'password' => [
            'min' => 'La :attribute debe contener más de :min caracteres',
        ],
        'email' => [
            'unique' => 'El :attribute ya ha sido registrado.',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'address'               => 'dirección',
        'age'                   => 'edad',
        'body'                  => 'contenido',
        'city'                  => 'ciudad',
        'content'               => 'contenido',
        'country'               => 'país',
        'date'                  => 'fecha',
        'day'                   => 'día',
        'description'           => 'descripción',
        'email'                 => 'correo electrónico',
        'excerpt'               => 'extracto',
        'first_name'            => 'nombre',
        'gender'                => 'género',
        'hour'                  => 'hora',
        'last_name'             => 'apellido',
        'message'               => 'mensaje',
        'minute'                => 'minuto',
        'mobile'                => 'móvil',
        'month'                 => 'mes',
        'name'                  => 'nombre',
        'password'              => 'contraseña',
        'password_confirmation' => 'confirmación de la contraseña',
        'phone'                 => 'teléfono',
        'price'                 => 'precio',
        'second'                => 'segundo',
        'sex'                   => 'sexo',
        'subject'               => 'asunto',
        'terms'                 => 'términos',
        'time'                  => 'hora',
        'title'                 => 'título',
        'username'              => 'usuario',
        'year'                  => 'año',
    ],

    'api' => [
        'T25'	=>  "El campo {{nome-campo}} es obligatorio.",
        'T26'	=>  "El campo {{nome-campo}} se llenó con valores no válidos.",
        'T27'	=>  "El valor ingresado en el campo {{fieldname}} ya existe en el sistema y no se puede duplicar.",
        'T28'	=>  "El valor ingresado en el campo {{nome-campo}} ha excedido el tamaño permitido.",
        'T29'	=>  "El registro ya existe en la base de datos - no se pudo volver a crear.",
        'T30'	=>  "El fabricante de la jerarquía superior no existe - debe registrarlo antes de conectarlo.",
        'T31'	=>  "El campo {{nome-campo}} tiene un valor que no está activo en el sistema.",
        'T32'	=>  "El Peso Neto no puede ser mayor que el Peso Bruto.",
        'T33'	=>  "La Unidad de Medida ingresada en el campo {{fieldname}} debe estar relacionada con Peso.",
        'T34'	=>  "El Código del Artículo {{codigo-item}} no existe en la pestaña Datos Generales.",
        'T35'	=>  "El fabricante informado no existe en el sistema.",
        'T36'	=>  "Este número de pieza ya está registrado para este fabricante.",
        'T37'	=>  "El número de pieza ingresado no existe en el sistema.",
        'T38'	=>  "Este número de parte se ha establecido en 'Viejo' para algún otro número de parte y debe estar en estado Inactivo.",
        'T39'	=>  "La unidad de medida completa debe estar en estado activo.",
        'T40'	=>  "El código de barras ya está registrado en el sistema.",
        'T41'	=>  "El campo {{nome-campo}} se debe completar debido a las relaciones con otros campos.",
        'T42'	=>  "La inclusión de más de una alternativa adimensional no está permitida.",
        'T43'	=>  "La UM Base debe ser la misma que la Básica u otra Alternativa.",
        'T44'	=>  "La Marca informada no existe en el sistema.",
        'T45'	=>  "La Marca informada tiene un estado de Inactivo.",
        'T46'   =>  "La etiqueta {{nome-campo}} se repite.",
        'T47'   =>  "Se ingresaron Valores repetidos. ATENCIÓN: incluso con diferentes unidades de medida, nuestro algoritmo identifica que hay valores repetidos cuando se convierten. Ej. 1 metro = 1,000 milímetros.",
        'T48'   =>  "Este correo electrónico ya tiene otro Tiral activo - no está permitido crear un nuevo Trial.",
        'T49'   =>  "¡Confirma tu registro en mub!",
        'T50'   =>  "Registro completado con éxito. ¡Bienvenido al mub!",
        'T51'   =>  "Invitación a ser parte de mub!",
        'T52'   =>  "Este árbol de categorías no existe.",
        'T53'   =>  "Este nodo del árbol de categorías no existe.",
        'T54'   =>  "Está eliminando un Atributo de una Categoría con PD. Revise el PD para no generar inconsistencias en la formación de la descripción.",
        'T55'   =>  "El :code informado ya existe en la base.",
        'T56'   =>  "Línea",
        'T57'   =>  "Campo",
        'T58'   =>  "Descripción",
        'T59'   =>  "Línea importada con éxito.",
        'T60'	=>  "Para crear un nuevo registro por lo menos 2 o más Términos Equivalentes o 1 Término Equivalente y 1 Prohibido.",
        'T61'	=>  "La combinación insertada de Términos Equivalentes y Prohibidos ya existe. No se puede duplicar.",
        'T62'   =>  "Error",
        'T63'   =>  "Éxito",
        'T64'   =>  "Archivo no encontrado",
        'T65'   =>  "El registro ha sido actualizado con éxito.",
        'T66'   =>  "El Fabricante seleccionado para la jerarquía superior tiene que ser diferente al que se está editando."
    ],
];
