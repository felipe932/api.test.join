<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function()
{
    Route::post('oauth/authorize', '\Laravel\Passport\Http\Controllers\ApproveAuthorizationController@approve');
    Route::get('oauth/authorize', '\Laravel\Passport\Http\Controllers\AuthorizationController@authorize');
    Route::delete('oauth/authorize', '\Laravel\Passport\Http\Controllers\DenyAuthorizationController@deny');
    Route::get('oauth/clients', '\Laravel\Passport\Http\Controllers\ClientController@forUser');
    Route::post('oauth/clients', '\Laravel\Passport\Http\Controllers\ClientController@store');
    Route::delete('oauth/clients/{client_id}', '\Laravel\Passport\Http\Controllers\ClientController@destroy');
    Route::patch('oauth/clients/{client_id}', '\Laravel\Passport\Http\Controllers\ClientController@update');
    Route::get('oauth/personal-access-tokens', '\Laravel\Passport\Http\Controllers\PersonalAccessTokenController@forUser');
    Route::post('oauth/personal-access-tokens', '\Laravel\Passport\Http\Controllers\PersonalAccessTokenController@store');
    Route::delete('oauth/personal-access-tokens/{token_id}', '\Laravel\Passport\Http\Controllers\PersonalAccessTokenController@destroy');
    Route::get('oauth/scopes', '\Laravel\Passport\Http\Controllers\ScopeController@all');
    Route::post('oauth/token', 'Auth\AccessTokenController@issueToken');

    Route::post('oauth/token/refresh', '\Laravel\Passport\Http\Controllers\TransientTokenController@refresh');
    Route::get('oauth/tokens', '\Laravel\Passport\Http\Controllers\AuthorizedAccessTokenController@forUser');
    Route::delete('oauth/tokens/{token_id}', '\Laravel\Passport\Http\Controllers\AuthorizedAccessTokenController@destroy');

    Route::group(['middleware' => 'auth:api'], function () {

        Route::get('users', 'UsersController@index');
        Route::get('users/{id}', 'UsersController@show');
        Route::patch('users/{id}', 'UsersController@update');
        Route::delete('users/{id}', 'UsersController@destroy');
        Route::post('users/reset_password', 'UsersController@resetPassword');

        Route::get('categories','CategoriesController@index');
        Route::post('categories', 'CategoriesController@store');
        Route::get('categories/{id}', 'CategoriesController@show');
        Route::patch('categories/{id}', 'CategoriesController@update');
        Route::delete('categories/{id}', 'CategoriesController@destroy'); 

        Route::get('products','ProductsController@index');
        Route::post('products', 'ProductsController@store');
        Route::get('products/{id}', 'ProductsController@show');
        Route::patch('products/{id}', 'ProductsController@update');
        Route::delete('products/{id}', 'ProductsController@destroy');
        
    });
});